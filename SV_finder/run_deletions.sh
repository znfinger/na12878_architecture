#!/bin/bash

>&2 echo "DELETIONS: STEP 1"
#1.# Split by chromosome:
mkdir hmm
perl split_by_chr.pl example.m5.hmm.F example.m5.hmm.F

mkdir blasr
gawk '$1 == $4 && $1 ~ /^chr[0-9X]*$/ {
  fn="./blasr/"$4".example.m4.bp.F"
   print($0) > fn}' example.m4.bp.F

>&2 echo "DELETIONS: STEP 2"
#2.# Filter:
for file in ./hmm/*
do
 perl final_hmm_filter.pl $file | awk -F ' ' '$8 == "D" {left=$5-$3; right=$4-$6; if ( left > 200 && right > 200 ) { r=$10/$9; if (r < 0.10) {print($0); }} }' > ${file}.C
 done

for file in ./blasr/*
do
     awk -F '\t' '{
        if ($1 == $4) {
        if ( ($3 - $2) > 200 && ($6 - $5) > 200 ) {
            print($0);
        }
    }
 }' $file > ${file}.C
     done

>&2 echo "DELETIONS: STEP 3"
#3. # Merge:
(for file in ./blasr/*.C
    do
     chr=${file##*/}
      chr=`echo $chr | sed 's/.*\(chr[0-9X]*\).*/\1/'`

       hmm_file=./hmm/${chr}.example.m5.hmm.F
        blasr_file=./blasr/${chr}.example.m4.bp.F

	 perl merge_raw_coordinates.pl $chr $blasr_file $hmm_file
	 done) > raw_coords.txt

mkdir raw_coords

gawk '{
	 fn="./raw_coords/"$1"_raw_coords.txt"
	  print($0) > fn
}' raw_coords.txt

>&2 echo "DELETIONS: STEP 4"
#4.# Write and submit job files:
# Attention: Add the name of the queue to the below script.
# Also, R must be in the path along with the necessary R packages whose identity is indicated elsewhere

mkdir jobs.deletions logs.deletions output.deletions
working_dir=`pwd`

for file in ./raw_coords/*.txt
do
 chr=${file##*/}
  chr=`echo $chr | sed 's/.*\(chr[0-9X]*\).*/\1/'`

   job=./jobs.deletions/${chr}.job
   
   # NOTE, IF WORKINGO ON A CLUSTER USE THIS PART INSTEAD
    #echo "#BSUB -L /bin/bash
    #BSUB -n 1
    #BSUB -J ${chr}
    #BSUB -oo ../logs.deletions/${chr}.log
    #BSUB -eo ../logs.deletions/${chr}.err
    #BSUB -q <name of the queue goes here>
    #BSUB -W 2:00
   echo "cd $working_dir; R --no-save --args $file < call_deletions.R" > $job
   chmod u+x $job
done

#cd jobs.deletions
chmod u+x jobs.deletions/*.job
ls jobs.deletions/*.job > ./jobs.deletions/cmds.txt
parallel -j 40 < jobs.deletions/cmds.txt
#cd ..

>&2 echo "DELETIONS: STEP 5"
#5. # Filter:
perl filter_deletions.pl | awk '$4 >= 3 && $4 < 100 && $7 < 0.15 && $9 != "TRUE" {
 if ($5 < 100) {
  # Require more supporting reads for small deletions
  if ($4 >= 10) {
   print($0)
  }
 }
 else if ($5 >= 100 && $5 < 200) {
  # Require more supporting reads for small deletions
  if ($4 >= 5) {
   print($0)
  }
 }
 else {
   print($0)
 }
}' > deletions.txt
