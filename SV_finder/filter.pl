#!/usr/bin/perl

use strict 'vars';

my @files = <./output.insertions_gap_based/*.complete.out>;

foreach my $file (@files) {

	my $file2 = $file;
	$file2 =~ s/complete/concise/;

	my %list;

	open(fh, $file);

	while (my $line = <fh>) {
		chomp($line);
		next if ($line =~ /^ins_id\s/);

		$line =~ /^(\S+) (\S+) /;
		my ($cluster,$read_id) = ($1,$2);

		$list{$cluster} .= ",$read_id";
	}

	close(fh);

	open(fh, $file2);

	while (my $line = <fh>) {
		chomp($line);

		if ($line =~ /^chr\t/) {
			#print("$line\n");
		}
		else {
			$line =~ /^(\S+)\t(\S+)\t(.+)/;
			my ($chr, $id, $rest) = ($1,$2,$3);

			my $reads = $list{$id};
			$reads =~ s/^,//;

			print("$chr\t$rest\t$reads\n");
		}
	}

	close(fh);
}
