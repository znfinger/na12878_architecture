#!/usr/bin/perl

use strict 'vars';

my %list;

open(fh, @ARGV[0]) or die("Cannot open @ARGV[0]");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+) /;
	my $id = $1;

	push(@$id, $line);

	$list{$id} = 1;
}

close(fh);

foreach my $id (keys(%list)) {
	my @ref = @$id;

	if (@ref == 1) {
		print("@ref\n");
	}
	else {
		my %types;

		foreach my $item (@ref) {
			$item =~ /\s(D|I) \S+ \S+ \S+ \S+$/;
			$types{$1} = 1;
		}

		if (keys(%types) == 1) {
			foreach my $item (@ref) {
				print("$item\n");
			}
		}
		else {
			my $largest_ins = 0;
			my @all_del;

			foreach my $item (@ref) {
				$item =~ /\s(D|I) (\S+) \S+ \S+ \S+$/;
				my ($type, $size) = ($1,$2);

				if ($size > $largest_ins && $type eq "I") {
					$largest_ins = $size;
				}
				elsif ($type eq "D") {
					push(@all_del, $size);
				}
			}

			my $ignore = 0;

			foreach my $ins (@all_del) {
				my $ratio = $ins / $largest_ins;

				if ($ratio < 2) {
					$ignore = 1;
					last;					
				}
			}

			next if ($ignore == 1);

			foreach my $item (@ref) {
				print("$item\n");
			}
		}
	}
}