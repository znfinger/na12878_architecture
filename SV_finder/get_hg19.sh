#!/bin/bash 
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/bigZips/chromFa.tar.gz
mkdir hg19
tar -zxvf chromFa.tar.gz -C hg19
cat ./hg19/* > ./hg19.fa
samtools faidx hg19.fa
rm -rfv hg19
rm -v chromFa.tar.gz

wget -qO- http://hgdownload.cse.ucsc.edu/goldenPath/hg19/database/gap.txt.gz
gunzip gap.txt.gz
mv gap.txt hg19_gaps.txt