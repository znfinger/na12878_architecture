#!/usr/bin/perl

use strict 'vars';

die ("Usage: <chr> <m4 alignments>\n") if (@ARGV != 2);

use Statistics::Basic;

my $chromosome_length;
my %matrix_left;
my %matrix_right;
my %temp_left;
my %temp_right;
my $num;
my $prog;

my $chr_target = @ARGV[0]; # chromosome
my $input_file = @ARGV[1]; # raw alignments, see http://127.0.0.1/?q=svhuman/node/580

open(fh, $input_file) or die("Cannot open $input_file");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+) (\S+) \S+ \S+ \S+ (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+)/;
	my ($query, $chr, $query_start, $query_stop, $query_length, $target_strand, $target_start, $target_stop, $target_length) = ($1,$2,$3,$4,$5,$6,$7,$8,$9);

	# Save the chromosome length
	$chromosome_length = $target_length;

	# Make coordinates 1-indexed
	$query_start ++ ;
	$target_start ++ ;

	if ($target_strand == 0) { # 0 = Plus strand
		# Get left and right overhang
		my $left_overhang_len = $query_start;
		my $right_overhang_len = $query_length - $query_stop;

		if ($left_overhang_len > 100) {
			$matrix_left{$target_start} ++ ;
			$temp_left{$target_start} .= "$query:$target_start,";
		}
		
		if ($right_overhang_len > 100) {
			$matrix_right{$target_stop} ++ ;
			$temp_right{$target_stop} .= "$query:$target_stop,";
		}
	}
	else {
		my $correct_start = $target_length - $target_stop;
		my $correct_stop = $target_length - $target_start;

		my $left_overhang_len = $query_length - $query_stop;
		my $right_overhang_len = $query_start;

		if ($left_overhang_len > 100) {
			$matrix_left{$correct_start} ++ ;
			$temp_left{$correct_start} .= "$query:$correct_start,";
		}
		
		if ($right_overhang_len > 100) {
			$matrix_right{$correct_stop} ++ ;
			$temp_right{$correct_stop} .= "$query:$correct_stop,";
		}
	}

	$prog ++ ;

	if ( ($prog % 10000) == 0) {
		print(STDERR "indexing $prog\n");
	}
}

close(fh);

$prog=0;

# Iterate the chromosome in windows
for (my $i=1; $i<=$chromosome_length;) {
	my $left_overhangers = 0;
	my $right_overhangers = 0;

	my $foobar_left;
	my $foobar_right;

	for (my $c=$i; $c<($i+10); $c++) {
		$left_overhangers += $matrix_left{$c};
		$right_overhangers += $matrix_right{$c};

		my $ids = $temp_left{$c};

		if ($ids ne "") {
			$foobar_left .= $ids;
		}

		my $ids = $temp_right{$c};

		if ($ids ne "") {
			$foobar_right .= $ids;
		}
	}

	if ($left_overhangers > 1 && $right_overhangers > 1) {
		my @positions;
		my %qweqwe_left;
		my @qwe = split(/,/, $foobar_left);

		foreach my $item (@qwe) {
			$qweqwe_left{$item} = 1;

			$item =~ /^\S+:(\S+)/;
			my $t = $1;
			push(@positions, $t);
		}

		my %qweqwe_right;
		my @qwe = split(/,/, $foobar_right);

		foreach my $item (@qwe) {
			$qweqwe_right{$item} = 1;

			$item =~ /^\S+:(\S+)/;
			my $t = $1;
			push(@positions, $t);
		}

		my $min = 0;
		my $max = 0;

		foreach my $pos (@positions) {
			if ($min == 0 || $pos < $min) {
				$min = $pos;
			}

			if ($pos > $max) {
				$max = $pos;
			}
		}

		# The start coordinate is 0-indexed in the bed format
		my @aa_left = keys(%qweqwe_left);
		my $str_left = "@aa_left";

		my @aa_right = keys(%qweqwe_right);
		my $str_right = "@aa_right";

		print($chr_target . "\t" . $min . "\t" . $max . "\tins_cand_" , (++$num) . "\t" . $left_overhangers . "_" . $right_overhangers . "\t$str_left\t$str_right\n");
	}

	$i += 10;

	$prog ++ ;

	if ( ($prog % 10000) == 0) {
		print(STDERR "analyzing $prog\n");
	}
}
