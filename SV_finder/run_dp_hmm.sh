#!/bin/bash 

PYCODEPATH="/sc/orga/scratch/bashia02/for_ali2/gitrepos/hupac/scripts/"
#XXXMP - move things to the top that need to be specified by the user so they don't have 
#        to go on a treasure hunt.
# Adjust 40 to the number of CPUs you have available.
PROCS=40

#Call read-level breakpoints
#===========================
# python packages are needed here numpy, cython, etc

python $PYCODEPATH/getSVBreakpointsFromBlasr.py --pctIdentityCut 75 example.m4 example.m4.bp

#2.# Remove trailing characters:

awk 'BEGIN {OFS="\t"} {
if ($7 ~ /_[0-9]*_[0-9]*$/) {
 read_id=$7;
 sub(/_[0-9]*_[0-9]*$/,"",read_id);
 $7=read_id;
 print($0);
}
else {
 print($0);
}
}' example.m4.bp > example.m4.bp.F

#Call deletions and insertions from alignment gaps
#=================================================
#1.# Split example.m5 into smaller pieces.

total=`wc -l example.m5 | cut -d ' ' -f1`
lines_per_file=`echo "$total/100" | bc`
split -l ${lines_per_file} -d -a 4 example.m5 example.m5.
mkdir queries out
mv -v example.m5.* queries

(for file in ./queries/*
    do
     echo "python $PYCODEPATH/parseRm5ForDeletionsCython.py $file ./out/${file##*/}.hmm"
done) > cmds.txt



# GNU parallel should be in your path.
# $ parallel --version
# GNU parallel 20130222

parallel -j $PROCS < cmds.txt

# Merge back
cat ./out/* > example.m5.hmm

# Clean
rm -rf out queries cmds.txt

#2.# Remove events < 50 bp:
awk '$9 > 50' example.m5.hmm > example.m5.hmm.F
