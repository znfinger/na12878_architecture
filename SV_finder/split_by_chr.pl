#!/usr/bin/perl

use strict 'vars';

die("Usage: <file> <prefix>") if (@ARGV != 2);

my $file = @ARGV[0];
my $prefix = @ARGV[1];

my @filehandles;

# Initiate output files
for (my $i=1; $i<=22; $i++) {
	local *fh_foo;
	open(fh_foo, ">./hmm/chr" . $i . "." . $prefix);
	@filehandles[$i] = *fh_foo;
}

local *fh_foo;
open(fh_foo, ">./hmm/chrX." . $prefix);
@filehandles[23] = *fh_foo;

open(fh, $file) or die("Cannot open $file");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^\S+\schr(\S+)\s/;
	my $chr = $1;
	my $out;

	next if ($chr eq "Y");
	next if ($chr eq "M");
	next if ($chr =~ "_");

	if ($chr eq "X") {
		$out = @filehandles[23];
	}
	else {
		$out = @filehandles[$chr];
	}

	print($out "$line\n");
}

close(fh);
