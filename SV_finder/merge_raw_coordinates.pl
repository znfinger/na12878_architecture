#!/usr/bin/perl

use strict 'vars';

die ("Usage: <chr> <input1=[blasr]> <input2=[hmm]>\n") if (@ARGV != 3);

my $chr = @ARGV[0];
my $input_file1 = @ARGV[1];
my $input_file2 = @ARGV[2];
my $input_file3 = @ARGV[3];

my @lines;
my %used;
my $gmap_unique;
my $hmm_unique;

if ($chr !~ /^chr/) {
	die("Argument 1 is invalid.");
}

open(fh, $input_file1) or die("Cannot open $input_file1\n");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t\S+\t(\S+)\t(\S+)/;
	my ($chr1, $genome_start1, $genome_stop1, $chr2, $genome_start2, $genome_stop2, $read_id, $strand1, $strand2) = ($1,$2,$3,$4,$5,$6,$7,$8,$9);

	my @counts = ($read_id =~ m/\//g);

	if (@counts == 3) {
		$read_id =~ /^(\S+\/\S+)\/\S+/;
		$read_id = $1;
	}

	# chr22	16050551	16054609	chr14	19788399	19790360	m121207_175107_42141_c100461520030000001523059505101351_s1_p0/36419/4308_11574	1	-	+	1	1	0	7542	85.009200	7542	7815	86.102200	29	0	2106	1	2114

	next if ($strand1 ne $strand2);

	####################################################
	### This part deals with the query space. Because sometimes the coordinates can occur like this: 500,100 1,100
	$line =~ /\t(\S+)\t(\S+)\t(\S+)\t(\S+)$/;
	my ($query_space_breakpoint_start1,$query_space_breakpoint_stop1) = ($1,$2);
	my ($query_space_breakpoint_start2,$query_space_breakpoint_stop2) = ($3,$4);

	if ($query_space_breakpoint_start1 > $query_space_breakpoint_stop1) {
		my $a = $query_space_breakpoint_stop1;
		$query_space_breakpoint_stop1 = $query_space_breakpoint_start1;
		$query_space_breakpoint_start1 = $a;
	}

	if ($query_space_breakpoint_start2 > $query_space_breakpoint_stop2) {
		my $a = $query_space_breakpoint_stop2;
		$query_space_breakpoint_stop2 = $query_space_breakpoint_start2;
		$query_space_breakpoint_start2 = $a;
	}

	my $query_space_breakpoint_distance;

	if ($query_space_breakpoint_start1 > $query_space_breakpoint_start2) {
		$query_space_breakpoint_distance = abs($query_space_breakpoint_start1 - $query_space_breakpoint_stop2);
	}
	else {
		$query_space_breakpoint_distance = abs($query_space_breakpoint_stop1 - $query_space_breakpoint_start2);
	}

	# Ignore inter-chromosomal hits
	next if ($chr1 ne $chr2);

	# Ignore alignment overlaps
	next if ($genome_start1 <= $genome_stop2 && $genome_start2 <= $genome_stop1);

	# Put in logic order
	if ($genome_stop1 > $genome_start2) {
		my $a = $genome_start1;
		my $b = $genome_stop1;
		my $c = $strand1;

		$genome_start1 = $genome_start2;
		$genome_stop1 = $genome_stop2;
		$strand1 = $strand2;

		$genome_start2 = $a;
		$genome_stop2 = $b;
		$strand2 = $c;
	}

	# Further away than 1,000,000 bp? Ignore for now.
	my $dist = $genome_start2 - $genome_stop1;
	#next if ($dist > 10**6);

	# Ignore smaller than 50 bp
	next if ($dist < 50);

	next if ($query_space_breakpoint_distance > 300);

	push(@lines, { query_space_breakpoint_distance => $query_space_breakpoint_distance, start1 => $genome_start1, stop1 => $genome_stop1, start2 => $genome_start2, stop2 => $genome_stop2, cluster_assignment => -1, strand1 => $strand1, strand2 => $strand2, read_id => $read_id });

	$used{$read_id} = 1;
}

close(fh);

# hmm input
open(fh, $input_file2) or die("Cannot open $input_file2\n");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s/;
	my ($read_id, $chr1, $genome_full_alignment_start, $genome_full_alignment_stop, $genome_event_start, $genome_event_stop) = ($1,$2,$3,$4,$5,$6);

	my @counts = ($read_id =~ m/\//g);

	if (@counts == 3) {
		$read_id =~ /^(\S+\/\S+)\/\S+/;
		$read_id = $1;
	}

	next if ($line =~ /\sI\s/);
	next if ($used{$read_id} ne "");

	$hmm_unique ++ ;

	my $genome_start1 = $genome_full_alignment_start;
	my $genome_stop1 = $genome_event_start;

	my $genome_start2 = $genome_event_stop;
	my $genome_stop2 = $genome_full_alignment_stop;

	push(@lines, { query_space_breakpoint_distance => "hmm", start1 => $genome_start1, stop1 => $genome_stop1, start2 => $genome_start2, stop2 => $genome_stop2, cluster_assignment => -1, read_id => $read_id });

	$used{$read_id} = 1;
}

close(fh);

# Sort input lines based on start
@lines = sort { $a->{start1} <=> $b->{start1} } @lines;

foreach my $item (@lines) {
	my $foo = sprintf("%s %s %s %s %s %s",$chr,$item->{start1},$item->{stop1},$item->{start2},$item->{stop2},$item->{read_id});
	print("$foo\n");
}
