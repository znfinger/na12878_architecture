#!/usr/bin/perl
# OF; Dec. 6, 2013

use Bio::Seq;
use strict 'vars';

die("Usage: <query file> <chr> <merged_ec_output.bam.txt.F or merged_new_chem.bam.txt_NA12878.pacbio.blasr_softclip.chemistry_1_2_and_3.S.bam.txt.F>") if (@ARGV != 3);

my $query_file = @ARGV[0];
my $chr_target = @ARGV[1];
my $f_file = @ARGV[2];
my $counter=0;

if (!-e "hg19.fa") {
	die("Cannot open hg19.fa\n");
}

if (!-e "hg19.fa.fai") {
	die("Cannot open hg19.fa.fai\n");
}

open(fh, $query_file) or die("Cannot open $query_file");

my %targets1;
my %targets2;

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+)/;
	my ($id, $chr, $strand_alignment_1, $strand_alignment_2, $alignment_1_start, $alignment_1_stop, $alignment_2_start, $alignment_2_stop) = ($1,$2,$3,$4,$5,$6,$7,$8);

	$targets1{$id."_".$chr."_".$strand_alignment_1."_".$alignment_1_start."_".$alignment_1_stop}{overlapping_aln_start} = $alignment_2_start;
	$targets1{$id."_".$chr."_".$strand_alignment_1."_".$alignment_1_start."_".$alignment_1_stop}{overlapping_aln_stop} = $alignment_2_stop;

	$targets2{$id."_".$chr."_".$strand_alignment_2."_".$alignment_2_start."_".$alignment_2_stop} = 1;
}

close(fh);

open(fh, $f_file) or die("Cannot open $f_file");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+)\t(\S+)\t(\S+)\t(\S+)\t\S+\t(\S+)\t\S+\t\S+\t\S+\t(\S+)\t/;
	my ($id, $bitflag, $chr, $ref_start, $cigar, $query) = ($1,$2,$3,$4,$5,$6);

	next if ($chr ne $chr_target);
	#next if ($id ne "m130208_221027_42156_c100470302550000001823071006131300_s1_p0/49329/3699_7300");

	my $strand = (($bitflag & 0x10) ? "-" : "+");

	# Parse CIGAR information
	my @operations = ($cigar =~ m/(\d+[MIDNSHP\=X])/g);
	my $ref_stop = $ref_start;

	foreach my $op (@operations) {
		if ($op =~ /(\d+)M/) {
			$ref_stop += $1;
		}
		elsif ($op =~ /(\d+)D/) {
			$ref_stop += $1;
		}
	}

	$ref_stop -- ;

	if ($targets1{$id."_".$chr."_".$strand."_".$ref_start."_".$ref_stop}{overlapping_aln_start} ne "") {
		# Extract sequence
		my $cmd1 = "samtools faidx hg19.fa $chr:$ref_start-$ref_stop | grep -v '>'";
		my $ref = `$cmd1`;
		$ref =~ s/\n//g;
		$ref = uc($ref);

		my $query_aln;
		my $ref_aln;

		# Reconstruct the pairwise alignment
		foreach my $op (@operations) {
			if ($op =~ /(\d+)M/) {
				my $len = $1;
				$ref =~ /^(\S{$len})(\S*)/;
				$ref_aln .= $1;
				$ref = $2;

				$query =~ /^(\S{$len})(\S*)/;
				$query_aln .= $1;
				$query = $2;
			}
			elsif ($op =~ /(\d+)I/) {
				my $len = $1;
				
				$query =~ /^(\S{$len})(\S*)/;
				$query_aln .= $1;
				$query = $2;

				my $gap;

				for (my $i=1; $i<=$len; $i++) {
					$gap .= "-";
				}

				$ref_aln .= $gap;
			}
			elsif ($op =~ /(\d+)D/) {
				my $len = $1;
				my $gap;

				for (my $i=1; $i<=$len; $i++) {
					$gap .= "-";
				}

				$query_aln .= $gap;

				$ref =~ /^(\S{$len})(\S*)/;
				$ref_aln .= $1;
				$ref = $2;
			}
			elsif ($op =~ /(\d+)S/) {
				my $len = $1;
				substr($query, 0, $len) = "";
			}
			else {
				die("1. Unsupported CIGAR operation: $op\n");
			}
		}

		# Locate the region that should be examined
		my $o_start = $targets1{$id."_".$chr."_".$strand."_".$ref_start."_".$ref_stop}{overlapping_aln_start};
		my $o_stop = $targets1{$id."_".$chr."_".$strand."_".$ref_start."_".$ref_stop}{overlapping_aln_stop};

		my $local_coord_start = $o_start - $ref_start + 1;
		my $local_coord_stop = $o_stop - $ref_start + 1;

		my $ref_pos_tracker = 0;
		my $cutting_start = 0;
		my $cutting_stop = 0;

		for (my $i=0; $i<length($ref_aln); $i++) {
			my $char = substr($ref_aln, $i, 1);

			if ($char ne "-") {
				$ref_pos_tracker ++ ;
			}

			if ($ref_pos_tracker == $local_coord_start) {
				$cutting_start = $i;
			}
			elsif ($ref_pos_tracker == $local_coord_stop) {
				$cutting_stop = $i;
			}
		}

		my $mismatch_count = 0;

		for (my $i=0; $i<length($query_aln); $i++) {
			my $char1 = substr($query_aln, $i, 1);
			my $char2 = substr($ref_aln, $i, 1);

			if ($char1 ne "-" && $char2 ne "-") {
				if ($char1 ne $char2) {
					$mismatch_count ++ ;
				}
			}
			elsif ($char1 eq "-" || $char2 eq "-") {
				$mismatch_count ++ ;
			}
		}

		my $entire_alignment_mismatch_perc_identity = 100-sprintf("%.2f",($mismatch_count/length($query_aln))*100);

		my $aln_obj1 = Bio::Seq->new(-seq => $query_aln);
		my $aln_obj2 = Bio::Seq->new(-seq => $ref_aln);

		my $flank1_query = $aln_obj1->subseq( ($cutting_start - 200) < 1 ? 1 : $cutting_start - 200, $cutting_start);
		my $flank1_ref = $aln_obj2->subseq( ($cutting_start - 200) < 1 ? 1 : $cutting_start - 200, $cutting_start);

		my $flank2_query = $aln_obj1->subseq($cutting_stop, ($cutting_stop+200) > length($ref_aln) ? length($ref_aln) : ($cutting_stop+200));
		my $flank2_ref = $aln_obj2->subseq($cutting_stop, ($cutting_stop+200) > length($ref_aln) ? length($ref_aln) : ($cutting_stop+200));

		$query_aln = substr($query_aln, $cutting_start, $cutting_stop - $cutting_start);
		$ref_aln = substr($ref_aln, $cutting_start, $cutting_stop - $cutting_start);

		my $mismatch_count = 0;

		for (my $i=0; $i<length($query_aln); $i++) {
			my $char1 = substr($query_aln, $i, 1);
			my $char2 = substr($ref_aln, $i, 1);

			if ($char1 ne "-" && $char2 ne "-") {
				if ($char1 ne $char2) {
					$mismatch_count ++ ;
				}
			}
			elsif ($char1 eq "-" || $char2 eq "-") {
				$mismatch_count ++ ;
			}
		}

		my $mismatch_perc_identity = 100-sprintf("%.2f",($mismatch_count/length($query_aln))*100);

		my $mismatch_count = 0;

		for (my $i=0; $i<length($flank1_query); $i++) {
			my $char1 = substr($flank1_query, $i, 1);
			my $char2 = substr($flank1_ref, $i, 1);

			if ($char1 ne "-" && $char2 ne "-") {
				if ($char1 ne $char2) {
					$mismatch_count ++ ;
				}
			}
			elsif ($char1 eq "-" || $char2 eq "-") {
				$mismatch_count ++ ;
			}
		}

		my $flank1_mismatch_perc_identity = 100-sprintf("%.2f",($mismatch_count/length($flank1_query))*100);

		my $mismatch_count = 0;

		for (my $i=0; $i<length($flank2_query); $i++) {
			my $char1 = substr($flank2_query, $i, 1);
			my $char2 = substr($flank2_ref, $i, 1);

			if ($char1 ne "-" && $char2 ne "-") {
				if ($char1 ne $char2) {
					$mismatch_count ++ ;
				}
			}
			elsif ($char1 eq "-" || $char2 eq "-") {
				$mismatch_count ++ ;
			}
		}

		my $flank2_mismatch_perc_identity = 100-sprintf("%.2f",($mismatch_count/length($flank2_query))*100);

		print("large $chr_target $id $strand $ref_start $ref_stop $entire_alignment_mismatch_perc_identity $mismatch_perc_identity $flank1_mismatch_perc_identity $flank2_mismatch_perc_identity\n");
	}
	elsif ($targets2{$id."_".$chr."_".$strand."_".$ref_start."_".$ref_stop} ne "") {
		# Extract sequence
		my $cmd1 = "samtools faidx hg19.fa $chr:$ref_start-$ref_stop | grep -v '>'";
		my $ref = `$cmd1`;
		$ref =~ s/\n//g;
		$ref = uc($ref);

		my $query_aln;
		my $ref_aln;

		# Reconstruct the pairwise alignment
		foreach my $op (@operations) {
			if ($op =~ /(\d+)M/) {
				my $len = $1;
				$ref =~ /^(\S{$len})(\S*)/;
				$ref_aln .= $1;
				$ref = $2;

				$query =~ /^(\S{$len})(\S*)/;
				$query_aln .= $1;
				$query = $2;
			}
			elsif ($op =~ /(\d+)I/) {
				my $len = $1;
				
				$query =~ /^(\S{$len})(\S*)/;
				$query_aln .= $1;
				$query = $2;

				my $gap;

				for (my $i=1; $i<=$len; $i++) {
					$gap .= "-";
				}

				$ref_aln .= $gap;
			}
			elsif ($op =~ /(\d+)D/) {
				my $len = $1;
				my $gap;

				for (my $i=1; $i<=$len; $i++) {
					$gap .= "-";
				}

				$query_aln .= $gap;

				$ref =~ /^(\S{$len})(\S*)/;
				$ref_aln .= $1;
				$ref = $2;
			}
			elsif ($op =~ /(\d+)S/) {
				my $len = $1;
				substr($query, 0, $len) = "";
			}
			else {
				die("2. Unsupported CIGAR operation: $op\n");
			}
		}

		my $mismatch_count = 0;

		for (my $i=0; $i<length($query_aln); $i++) {
			my $char1 = substr($query_aln, $i, 1);
			my $char2 = substr($ref_aln, $i, 1);

			if ($char1 ne "-" && $char2 ne "-") {
				if ($char1 ne $char2) {
					$mismatch_count ++ ;
				}
			}
			elsif ($char1 eq "-" || $char2 eq "-") {
				$mismatch_count ++ ;
			}
		}

		my $mismatch_perc_identity = 100-sprintf("%.2f",($mismatch_count/length($query_aln))*100);
		print("small $chr_target $id $strand $ref_start $ref_stop $mismatch_perc_identity\n");
	}
}

close(fh);
