#!/bin/bash 
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz
mkdir hg38
tar -zxvf chromFa.tar.gz -C hg38
cat ./hg38/* > ./hg38.fa
samtools faidx hg38.fa
rm -rfv hg38
rm -v chromFa.tar.gz

wget -qO- http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/gap.txt.gz
gunzip gap.txt.gz
mv gap.txt hg38_gaps.txt
