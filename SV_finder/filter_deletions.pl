#!/usr/bin/perl

use strict 'vars';

my @files = <./output.deletions/*.full.out>;

foreach my $file (@files) {

	my $file2 = $file;
	$file2 =~ s/full/concise/;

	my %list;

	open(fh, $file);

	while (my $line = <fh>) {
		chomp($line);
		next if ($line =~ /^cluster\s/);

		$line =~ /^(\S+) /;
		my $cluster = $1;

		$line =~ /\s(\S+)$/;
		my $read_id = $1;

		$list{$cluster} .= ",$read_id";
	}

	close(fh);

	open(fh, $file2);

	while (my $line = <fh>) {
		chomp($line);

		if ($line =~ /^cluster/) {
			#print("$line\n");
		}
		else {
			$line =~ /^(\S+) (.+)/;
			my ($id, $rest) = ($1,$2);

			my $reads = $list{$id};
			$reads =~ s/^,//;

			print("$rest $reads\n");
		}
	}

	close(fh);
}
