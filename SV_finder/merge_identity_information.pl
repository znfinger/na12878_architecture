#!/usr/bin/perl

use strict 'vars';

die("Usage: <arg1> <arg2>\n") if (@ARGV != 2);

#my @files = <./IO/chemistry1.sorted.bed.overlaps.out.*.out>;
my @files = <@ARGV[0]>;

my %list;

foreach my $file (@files) {
	print(STDERR "$file\n");

	open(fh, $file);

	while (my $line = <fh>) {
		chomp($line);

		if ($line =~ /^large/) {
			$line =~ /^(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)/;
			my ($type, $chr, $id, $strand, $start, $stop, $identity1, $identity2, $identity3, $identity4) = ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);

			$list{$type}{$chr}{$id}{$strand}{$start}{$stop}{identity1} = $identity1;
			$list{$type}{$chr}{$id}{$strand}{$start}{$stop}{identity2} = $identity2;

			$list{$type}{$chr}{$id}{$strand}{$start}{$stop}{identity3} = $identity3;
			$list{$type}{$chr}{$id}{$strand}{$start}{$stop}{identity4} = $identity4;
		}
		else {
			$line =~ /^(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)/;
			my ($type, $chr, $id, $strand, $start, $stop, $identity) = ($1,$2,$3,$4,$5,$6,$7);
			$list{$type}{$chr}{$id}{$strand}{$start}{$stop}{identity} = $identity;
		}
	}

	close(fh);
}

open(fh, @ARGV[1]) or die("Cannot open @ARGV[1]");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+)/;
	my ($id, $chr, $strand1, $strand2, $a_start, $a_stop, $b_start, $b_stop) = ($1,$2,$3,$4,$5,$6,$7,$8);

	my $identity1 = $list{large}{$chr}{$id}{$strand1}{$a_start}{$a_stop}{identity1};
	my $identity2 = $list{large}{$chr}{$id}{$strand1}{$a_start}{$a_stop}{identity2};
	my $identity3 = $list{large}{$chr}{$id}{$strand1}{$a_start}{$a_stop}{identity3};
	my $identity4 = $list{large}{$chr}{$id}{$strand1}{$a_start}{$a_stop}{identity4};
	my $identity5 = $list{small}{$chr}{$id}{$strand2}{$b_start}{$b_stop}{identity};

	print("$id $chr $strand1 $strand2 $a_start $a_stop $b_start $b_stop $identity1 $identity2 $identity3 $identity4 $identity5\n");
}

close(fh);
