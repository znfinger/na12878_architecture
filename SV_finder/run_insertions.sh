#!/bin/bash
#Overhang based insertions
#=========================
#See insertion_overhang.png.

>&2 echo "INSERTIONS - OVERHANG: STEP 1"
#1.# Only use long alignments (>1000 bp):
awk '{
    len=$7-$6;

    if (len > 1000) {
print($0)
}
}' example.m4 > example.m4.long

>&2 echo "INSERTIONS - OVERHANG: STEP 2"
#2.# Remove non-unique hits:
awk '{lines[$1]++; lines2[$1]=$0;} END {for (i in lines) {if (lines[i] == 1) { print(lines2[i]) }}}' example.m4.long > example.m4.long.u

>&2 echo "INSERTIONS - OVERHANG: STEP 3"
#3.# Remove alignments without overhang:
awk '{
   length_right_flank=$8-$7;
   length_left_flank=$6;

   ignore=0;

   if (length_right_flank < 100 && length_left_flank < 100) {
      ignore=1;
   }

   if (ignore == 0) {
       print($0);
   }
 }' example.m4.long.u > example.m4.long.u.oh

>&2 echo "INSERTIONS - OVERHANG: STEP 4"
#4.# Create dir in case it doesn't exist
mkdir -p raw_coords

# Clean it
rm -v ./raw_coords/*

# Split by chromosome
gawk '$2 ~ /^chr[0-9X]*$/ {
     fn="./raw_coords/"$2".example.m4.long.u.oh"
      print($0) > fn
      }' example.m4.long.u.oh

>&2 echo "INSERTIONS - OVERHANG: STEP 5"
#5.# Write jobs:

mkdir -p jobs.ins_overhang logs.ins_overhang output.ins_overhang

working_dir=`pwd`

for file in ./raw_coords/chr*example.m4.long.u.oh
do
     chr=`echo $file | sed 's/.*\(chr[0-9X]*\).*/\1/g'`
          job=./jobs.ins_overhang/${chr}.job
     name=$chr
     output=./output.ins_overhang/${chr}.ins.out
     
     # if you want ot run on a cluster replace this!
     #echo "#BSUB -L /bin/bash
    #BSUB -n 1
#BSUB -J ${chr}
#BSUB -oo ../logs.ins_overhang/${chr}.log
#BSUB -eo ../logs.ins_overhang/${chr}.err
#BSUB -q <name of the queue goes here>
#BSUB -W 2:00
     echo "cd $working_dir; perl of_sv-caller.insertions_partial_overhang.pl $chr $file > $output" > $job

done

#for file in ./*.job; do bsub < $file; done
#cd jobs.ins_overhang
chmod u+x ./jobs.ins_overhang/*.job
ls ./jobs.ins_overhang/*.job > ./jobs.ins_overhang/cmds.txt
parallel -j 40 < ./jobs.ins_overhang/cmds.txt
#cd ..

>&2 echo "INSERTIONS - OVERHANG: STEP 6"
#6.# Filter:
perl filter_oh.pl | sort -k 1,1V -k 2,2n > insertions.overhang_based.txt

#Alignment gap-based insertions
#==============================
#See insertions_alignment_gap.png

>&2 echo "INSERTIONS - GAP: STEP 1"
#1.# Create it in case it doesn't exist
mkdir -p raw_coords

# Clean it
rm -v ./raw_coords/*

# Split by chromosome:
gawk '$2 ~ /^chr[0-9X]*$/ {
 fn="./raw_coords/"$2".example.m5.hmm.F"
 print($0) > fn
}' example.m5.hmm.F

gawk '$1 == $4 && $1 ~ /^chr[0-9X]*$/ {
  fn="./raw_coords/"$4".example.m4.bp.F"
   print($0) > fn}' example.m4.bp.F

>&2 echo "INSERTIONS - GAP: STEP 2"
#2.# Filter:
for file in ./raw_coords/chr*.example.m5.hmm.F
do
 perl final_hmm_filter2.pl $file | awk -F ' ' '$8 == "I" {left=$5-$3; right=$4-$6; if ( left > 200 && right > 200 ) { r=($6-$5)/$9; if (r < 0.10) {print($0); }} }' > ${file}.C
 done

for file in ./raw_coords/chr*.example.m4.bp.F
do
    awk -F '\t' '{
        if ($1 == $4) {
        if ( ($3 - $2) > 200 && ($6 - $5) > 200 ) {
            print($0);
        }
    }
 }' $file > ${file}.C
done

>&2 echo "INSERTIONS - GAP: STEP 3"
#3. # Merge coordinates:
for hmm_file in ./raw_coords/chr*.example.m5.hmm.F.C
do
     chr=${hmm_file##*/}
     chr=`echo $chr | sed 's/.*\(chr[0-9X]*\).*/\1/'`
     dp_file=./raw_coords/${chr}.example.m4.bp.F.C
     touch $dp_file
     out=./raw_coords/${chr}.final
     
     perl merge_raw_coordinates2.pl ${chr} ${hmm_file} ${dp_file} > $out
done

>&2 echo "INSERTIONS - GAP: STEP 4"
#4.# Write job scripts:
mkdir -p output.insertions_gap_based jobs.insertions_gap_based logs.insertions_gap_based

working_dir=`pwd`

for chr in chr{1..22} chrX chrY
do
     job=./jobs.insertions_gap_based/${chr}.job
     
     # NOTE, IF WORKING ON A CLUSTER USE THIS INSTEAD (OPTIONAL)
     #echo "#BSUB -L /bin/bash
#BSUB -n 1
#BSUB -J ${chr}
#BSUB -oo ../logs.insertions_gap_based/${chr}.log
#BSUB -eo ../logs.insertions_gap_based/${chr}.err
#BSUB -q <name of the queue goes here>
#BSUB -W 2:00
     #touch ./raw_coords/${chr}.out
     #echo "cd $working_dir; >&2 echo ./raw_coords/${chr}.final; R --no-save --args $working_dir ${chr} ./raw_coords/${chr}.final < insertions_type_1.R" > $job
     if [ -f ./raw_coords/${chr}.final ]
     then
	 #>&2 echo "Are you the file that is causing trouble?"
	 #>&2 echo ./raw_coords/${chr}.final 
	 #R --no-save --args $working_dir ${chr} ./raw_coords/${chr}.final < insertions_type_1.R
	 echo "cd $working_dir; >&2 echo ./raw_coords/${chr}.final; R --no-save --args $working_dir ${chr} ./raw_coords/${chr}.final < insertions_type_1.R" > $job
     fi
     done

#cd jobs.insertions_gap_based
chmod u+x ./jobs.insertions_gap_based/*.job
ls ./jobs.insertions_gap_based/*.job > ./jobs.insertions_gap_based/cmds.txt
parallel -j 40 < ./jobs.insertions_gap_based/cmds.txt
#for file in ./*.job; do  < $file; done
#cd ..

>&2 echo "INSERTIONS - GAP: STEP 5"
#5.# Filter:
perl filter.pl | awk -F '\t' '$5 > 50 && $4 >= 3 && $4 < 100 && $8 < 0.15 {
	 if ($5 < 100) {
		   # Require more supporting reads for small insertions
		   if ($4 >= 10) {
			      print($0)
			        }
		        }
	      else if ($5 >= 100 && $5 < 200) {
		       # Require more supporting reads for small insertions
		       if ($4 >= 5) {
			          print($0)
				    }
			    }
		  else {
		    print($0)
		     }
	     }' > insertions_gap_based.txt
