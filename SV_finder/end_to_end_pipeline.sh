#!/bin/bash

# get data (m5, m4, bam) before doing this
# also pull down reference and gaps (use bash script)
sh run_dp_hmm.sh > dp_hmm_log.txt 2> dp_hmm_log.err
sh run_deletions.sh > del_log.txt 2> del_log.err
sh run_insertions.sh > ins_log.txt 2> ins_log.err
sh run_inversions.sh > inv_log.txt 2> inv_log.err
