#!/usr/bin/perl

# ./merge_raw_coordinates.pl chr1 ./IO/chr1.NA12878.PB.m5.hmm_analyzed.chem_1_2_3.C.F ./IO/chr1.NA12878.iter.dp.F

use strict 'vars';

die ("Usage: <chr> <input1=[blasr]> <input2=[hmm]>\n") if (@ARGV != 3);

my $chr = @ARGV[0];
my $input_file1 = @ARGV[1];
my $input_file2 = @ARGV[2];

my @lines;
my %used;

if ($chr !~ /^chr/) {
	die("Argument 1 is invalid.");
}

# hmm input
open(fh, $input_file1) or die("Cannot open $input_file1\n");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s/;
	my ($read_id, $chr1, $genome_full_alignment_start, $genome_full_alignment_stop, $genome_event_start, $genome_event_stop) = ($1,$2,$3,$4,$5,$6);

	my @counts = ($read_id =~ m/\//g);

	if (@counts == 3) {
		$read_id =~ /^(\S+\/\S+)\/\S+/;
		$read_id = $1;
	}

	next if ($line =~ /\sD\s/);

	$line =~ /\s(\S+)\s\S+\s(\S+)\s(\S+)$/;
	my $query_distance = $1;
	my $query_start = $2;
	my $query_stop = $3;

	my $genome_start1 = $genome_full_alignment_start;
	my $genome_stop1 = $genome_event_start;

	my $genome_start2 = $genome_event_stop;
	my $genome_stop2 = $genome_full_alignment_stop;

	if ($query_start > $query_stop) {
		my $a = $query_start;
		$query_start = $query_stop;
		$query_stop = $a;
	}

	push(@lines, { read_id => $read_id,
		           ref_ins_pos_start => $genome_stop1,
		           ref_ins_pos_stop => $genome_start2,
		           query_distance => $query_distance,
		           query_start => $query_start,
		           query_stop => $query_stop });

	$used{$read_id} = 1;
}

close(fh);

# dp
open(fh, $input_file2) or die("Cannot open $input_file2\n");

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t\S+\t(\S+)\t(\S+)/;
	my ($chr1, $genome_start1, $genome_stop1, $chr2, $genome_start2, $genome_stop2, $read_id, $strand1, $strand2) = ($1,$2,$3,$4,$5,$6,$7,$8,$9);

	my @counts = ($read_id =~ m/\//g);

	if (@counts == 3) {
		$read_id =~ /^(\S+\/\S+)\/\S+/;
		$read_id = $1;
	}

	next if ($used{$read_id} ne "");

	# chr22	16050551	16054609	chr14	19788399	19790360	m121207_175107_42141_c100461520030000001523059505101351_s1_p0/36419/4308_11574	1	-	+	1	1	0	7542	85.009200	7542	7815	86.102200	29	0	2106	1	2114

	next if ($strand1 ne $strand2);

	####################################################
	### This part deals with the query space. Because sometimes the coordinates can occur like this: 500,100 1,100
	$line =~ /\t(\S+)\t(\S+)\t(\S+)\t(\S+)$/;
	my ($query_space_breakpoint_start1,$query_space_breakpoint_stop1) = ($1,$2);
	my ($query_space_breakpoint_start2,$query_space_breakpoint_stop2) = ($3,$4);

	if ($query_space_breakpoint_start1 > $query_space_breakpoint_stop1) {
		my $a = $query_space_breakpoint_stop1;
		$query_space_breakpoint_stop1 = $query_space_breakpoint_start1;
		$query_space_breakpoint_start1 = $a;
	}

	if ($query_space_breakpoint_start2 > $query_space_breakpoint_stop2) {
		my $a = $query_space_breakpoint_stop2;
		$query_space_breakpoint_stop2 = $query_space_breakpoint_start2;
		$query_space_breakpoint_start2 = $a;
	}

	my $query_space_breakpoint_distance;

	if ($query_space_breakpoint_start1 > $query_space_breakpoint_start2) {
		$query_space_breakpoint_distance = abs($query_space_breakpoint_start1 - $query_space_breakpoint_stop2);
	}
	else {
		$query_space_breakpoint_distance = abs($query_space_breakpoint_stop1 - $query_space_breakpoint_start2);
	}

	# Ignore if overlapping in query space
	next if ($query_space_breakpoint_start1 <= $query_space_breakpoint_stop2 && $query_space_breakpoint_start2 <= $query_space_breakpoint_stop1);

	# Ignore inter-chromosomal hits
	next if ($chr1 ne $chr2);

	# Put in logic order
	if ($genome_start1 > $genome_start2) {
		my $a = $genome_start1;
		my $b = $genome_stop1;

		$genome_start1 = $genome_start2;
		$genome_stop1 = $genome_stop2;

		$genome_start2 = $a;
		$genome_stop2 = $b;
	}

	my $dist_genome_space = abs($genome_start2 - $genome_stop1);
	my $ratio = $dist_genome_space/$query_space_breakpoint_distance;

	next if ($ratio > 0.1);
	next if ($query_space_breakpoint_distance < 50);

	push(@lines, { read_id => $read_id,
		           ref_ins_pos_start => (($genome_stop1 < $genome_start2) ? $genome_stop1 : $genome_start2),
		           ref_ins_pos_stop => (($genome_stop1 < $genome_start2) ? $genome_start2 : $genome_stop1),
		           query_distance => $query_space_breakpoint_distance,
		           query_start => $query_space_breakpoint_stop1,
		           query_stop => $query_space_breakpoint_start2 });
}

close(fh);

# Sort input lines based on start
@lines = sort { $a->{start1} <=> $b->{start1} } @lines;

print("chr read_id ref_ins_pos_start ref_ins_pos_stop query_distance query_start query_stop\n");

foreach my $item (@lines) {
	my $foo = sprintf("%s %s %s %s %s %s %s",$chr,$item->{read_id},$item->{ref_ins_pos_start},$item->{ref_ins_pos_stop},$item->{query_distance},$item->{query_start},$item->{query_stop});

	print("$foo\n");
}
