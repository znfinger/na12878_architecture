# OF <p.oscar.franzen@gmail.com>; Dec. 12, 2013

suppressMessages(library(IRanges))
suppressMessages(library(hash))

rm(list=ls())

args <- commandArgs(trailingOnly = TRUE)

arg.target_identity_file<-args[1]
arg.output_dir<-args[2]

# setwd("/hpc/users/franzo01/scratch/HUMAN-SV/PacBio/Inversions/Type_Ia")

#f=read.table("./IO/chemistry_1_2_3.overlaps.identities",sep=" ",header=F)
f<-read.table(arg.target_identity_file,sep=" ",header=F)

colnames(f)=c("id","chr","strand1","strand2","start1","stop1","start2","stop2","identity.aln1.full","identity.aln1.gap","identity.aln1.left_flank","identity.aln1.right_flank","identity.aln2.gap")
chrs=unique(f$chr)

for (chr_foo in chrs) {
  print(chr_foo)

  f.ss=subset(f, chr == chr_foo &
                 identity.aln1.gap <= 75 &
                 identity.aln1.full >= 50 &
                 (identity.aln1.full-identity.aln1.gap) > 5 &
                 identity.aln2.gap >= 70 &
                 identity.aln2.gap > identity.aln1.gap &
                 (((stop2 - start2) < 200 & identity.aln2.gap > 90) | (stop2 - start2) > 200)
                 )
  
  chr_target=unique(f.ss$chr)
  r=with(f.ss, IRanges(start=start2, end=stop2))
  
  f.ss$cluster=subjectHits(findOverlaps(r,reduce(r)))

  # Replace read ids with unique numbers instead (R slowness fix)
  counter=0
  reads=unique(f.ss$id)

  h=hash()

  for (item in reads) {
    counter=counter+1
    h[item]=counter
  }

  ids=c()

  for (item in f.ss[,1]) {
    ids=c(ids,h[[item]])
  }

  f.ss=data.frame(f.ss,read_id_as_number=ids)

  count_unique_reads=function(x) {
    length(unique(as.data.frame(x)$x))
  }

  if (nrow(f.ss) > 0) {
    read_counts=with(f.ss,aggregate(read_id_as_number, by=list(cluster), FUN=count_unique_reads)$x)

    cluster_start = with(f.ss, aggregate(start2, by=list(cluster), FUN=mean))
    cluster_stop  = as.integer(with(f.ss, aggregate(stop2, by=list(cluster), FUN=mean))$x)
    #read_counts   = as.integer(with(f.ss, aggregate(start2, by=list(cluster), FUN=length))$x)
    size.mean     = as.integer(with(f.ss, aggregate(stop2-start2, by=list(cluster), FUN=mean))$x)
    size.sd       = as.integer(with(f.ss, aggregate(stop2-start2, by=list(cluster), FUN=sd))$x)
    normalized_sd = sprintf("%.2f",(size.sd/size.mean))

    cluster_start.sd = as.integer(with(f.ss, aggregate(start2, by=list(cluster), FUN=sd))$x)
    cluster_stop.sd  = as.integer(with(f.ss, aggregate(stop2,  by=list(cluster), FUN=sd))$x)

    cluster_start.sd.norm = sprintf("%.2f",(cluster_start.sd/size.mean))
    cluster_stop.sd.norm  = sprintf("%.2f",(cluster_stop.sd/size.mean))

    df=data.frame(chr=chr_target,
                  cluster=cluster_start[,1],
                  inversion_start=as.integer(cluster_start$x),
                  inversion_stop=cluster_stop,
                  cluster_start.sd=cluster_start.sd,
                  cluster_stop.sd=cluster_stop.sd,
                  cluster_start.sd.norm=cluster_start.sd.norm,
                  cluster_stop.sd.norm=cluster_stop.sd.norm,
                  read_counts=read_counts,
                  size.mean=size.mean,
                  size.sd=size.sd,
                  size.norm.sd=normalized_sd)

    write.table(df, row.names=F, quote=F, file=paste(arg.output_dir,chr_target,".concise.out",sep=""),sep="\t")
    write.table(f.ss[1:14], row.names=F, quote=F, file=paste(arg.output_dir,chr_target,".complete.out",sep=""),sep="\t")
  }
}
