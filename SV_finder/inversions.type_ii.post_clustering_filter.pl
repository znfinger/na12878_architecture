#!/usr/bin/perl

use strict 'vars';

die("Usage: <input dir> <cut off reads> <normalized_sd cut off>") if (@ARGV != 3);

my $cluster_id = 1;
my %reads;

my @files = <@ARGV[0]/*complete*>;

die("No files 1") if (@files == 0);

foreach my $file (@files) {
	open(fh, $file);

	$file =~ /(plus|minus)/;
	my $foobar = $1;

	$file =~ /(chr\S+?)\./;
	my $chr = $1;
	
	while (my $line = <fh>) {
		chomp($line);
		next if ($line =~ /^cluster\s/);

		$line =~ /^(\S+)\s/;
		my $id = $1;

		$line =~ /\s(\S+)$/;
		my $read = $1;

		$reads{$chr}{$id} .= "$read,";
	}
}

my @files = <@ARGV[0]/*concise*>;
die("No files 2") if (@files == 0);

foreach my $file (@files) {
	open(fh, $file);

	$file =~ /(plus|minus)/;
	my $foobar = $1;
	
	while (my $line = <fh>) {
		chomp($line);
		next if ($line =~ /^cluster /);

		$line =~ /^(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)/;

		my ($cluster, $chr, $start, $stop, $read_count, $size_mean, $size_sd, $norm_sd, $gap) = ($1,$2,$3,$4,$5,$6,$7,$8,$9);

		next if ($gap eq "TRUE");

		if ($read_count >= @ARGV[1] && $norm_sd <= @ARGV[2]) {
			my $r = $reads{$chr}{$cluster};
			chop($r);

			print("$chr $start $stop $read_count $size_mean $size_sd $norm_sd $r\n");
		}
	}

	close(fh);
}