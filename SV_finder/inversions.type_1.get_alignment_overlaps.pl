#!/usr/bin/perl

use strict 'vars';

my %ids;

open(fh, @ARGV[0]) or die("Cannot open @ARGV[0]");

@ARGV[0] =~ /(chr[0-9X]+)/;
my $chr = $1;

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+)\t(\S+)\t(\S+)\t(\S+)\t\S+\t(\S+)/;
	my ($chr, $start, $stop, $id, $strand) = ($1,$2,$3,$4,$5);

	$start ++ ;

	push(@$id, { strand => $strand, start => $start, stop => $stop });
	$ids{$id}=1;
}

close(fh);

foreach my $id (keys(%ids)) {
	my @ref = @$id;
	next if (@ref == 1);

	# Split into pos and neg
	my @pos;
	my @neg;

	foreach my $item (@ref) {
		if ($item->{strand} eq "+") {
			push(@pos, $item);
		}
		elsif ($item->{strand} eq "-") {
			push(@neg, $item);
		}
	}

	next if (@pos == 0);
	next if (@neg == 0);

	# Check any overlaps
	foreach my $item1 (@pos) {
		foreach my $item2 (@neg) {
			if ($item2->{start} > $item1->{start} && $item2->{stop} < $item1->{stop}) {
				print("$id $chr + - " . $item1->{start} . " " . $item1->{stop} . " " . $item2->{start} . " " . $item2->{stop} . "\n");
			}
		}
	}

	foreach my $item1 (@neg) {
		foreach my $item2 (@pos) {
			if ($item2->{start} > $item1->{start} && $item2->{stop} < $item1->{stop}) {
				print("$id $chr - + " . $item1->{start} . " " . $item1->{stop} . " " . $item2->{start} . " " . $item2->{stop} . "\n");
			}
		}
	}
}
