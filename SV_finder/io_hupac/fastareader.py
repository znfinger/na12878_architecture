#!/usr/bin/env python

import sys
import os

def readFastaToDict(fastaFile):
    seqDict = {}
    try:
       fastaIn = open(fastaFile,'r')
    except:
       print fastaFile, " not Found!"
    fastaLine = fastaIn.readline()
    while (fastaLine):
          qidFin0 = fastaLine.rstrip("\n").split(">")[1]
          seqline = fastaIn.readline()
          seqL = []
          while(seqline.find(">") == -1):
               seqL.append(seqline.rstrip("\n"))
               seqline = fastaIn.readline()
               if (seqline == ''):
                  break
          seq = ''.join(seqL)
          seqDict[qidFin0] = seq
          fastaLine = seqline
    fastaIn.close()
    return seqDict

def readFastaToDictPOA(fastaFile):
    seqDict = {}
    try:
       fastaIn = open(fastaFile,'r')
    except:
       print fastaFile, " not Found!"
    fastaLine = fastaIn.readline()
    while (fastaLine):
          qidFin0 = fastaLine.rstrip("\n").split(">")[1]
          qidFin1 = qidFin0.split()[0]
          seqline = fastaIn.readline()
          seqL = []
          while(seqline.find(">") == -1):
               seqL.append(seqline.rstrip("\n"))
               seqline = fastaIn.readline()
               if (seqline == ''):
                  break
          seq = ''.join(seqL)
          seqDict[qidFin1] = seq
          fastaLine = seqline
    fastaIn.close()
    return seqDict
