#!/usr/bin/perl

use strict 'vars';

die("Usage: <dir> <cut off reads> <normalized_sd cut off>") if (@ARGV != 3);

#print(fh_out "id\tchr\tstrand1\tstrand2\tstart1\tstop1\tstart2\tstop2\tidentity.aln1.full\tidentity.aln1.gap\tidentity.aln1.left_flank\tidentity.aln1.right_flank\tidentity.aln2.gap\tcluster\n");

my @files = <./@ARGV[0]/*complete*>;
die("No files") if (@files == 0);

my %reads;

foreach my $file (@files) {
	open(fh, $file);
	
	while (my $line = <fh>) {
		chomp($line);
		next if ($line =~ /^id\t/);

		$line =~ /^(\S+)\t(\S+)\t/;
		my ($read, $chr) = ($1,$2);

		$line =~ /\t(\S+)$/;
		my $id = $1;

		$reads{$chr}{$id} .= $read . ",";
	}
}

my @files = <./@ARGV[0]/*concise*>;
die("No files") if (@files == 0);

foreach my $file (@files) {
	open(fh, $file);
	
	while (my $line = <fh>) {
		chomp($line);
		next if ($line =~ /^chr\t/);

		$line =~ /^(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\S+)/;
		my ($chr, $old_cluster_id, $start, $stop, $cluster_start_sd, $cluster_stop_sd, $cluster_start_sd_norm, $cluster_stop_sd_norm, $read_count, $size_mean, $size_sd, $size_norm_sd) = ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);

		#if ($read_count >= @ARGV[0] && $cluster_start_sd_norm <= @ARGV[1] && $cluster_stop_sd_norm <= @ARGV[1]) {
		if ($read_count >= @ARGV[1] && $size_norm_sd <= @ARGV[2]) {

			my $str = $reads{$chr}{$old_cluster_id};
			chop($str);
			
			print("$chr\t$start\t$stop\t$cluster_start_sd\t$cluster_stop_sd\t$cluster_start_sd_norm\t$cluster_stop_sd_norm\t$read_count\t$size_mean\t$size_sd\t$size_norm_sd\t$str\n");
		}
	}

	close(fh);
}