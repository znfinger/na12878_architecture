#!/usr/bin/perl

use strict 'vars';

die("Usage: <file>") if (@ARGV != 1);

# open(fh, "/Users/rand/Bioinformatik/Proj/HumanSV/Pacbio_custom_calling/Inversions/Type_Ia/NA12878.inversions.type_1.complete.txt") or die("Cannot open /Users/rand/Bioinformatik/Proj/HumanSV/Pacbio_custom_calling/Inversions/Type_Ia/NA12878.inversions.type_1.complete.txt");

# my %ignore;
my $signature_count = 0;

# while (my $line = <fh>) {
# 	chomp($line);
# 	next if ($line =~ /^id\t/);

# 	$line =~ /^(\S+)\t/;
# 	$ignore{$1} = 1;
# }

# close(fh);

my %ids;

open(fh, @ARGV[0]) or die("Cannot open @ARGV[0]");

@ARGV[0] =~ /(chr[0-9X]+)/;
my $chr = $1;

while (my $line = <fh>) {
	chomp($line);

	$line =~ /^(\S+) \S+ \S+ (\S+) \S+ (\S+) (\S+) \S+ (\S+) (\S+) (\S+) (\S+) /;
	my ($id, $identity, $query_start, $query_stop, $strand, $ref_start, $ref_stop, $chr_length) = ($1,$2,$3,$4,$5,$6,$7,$8);

	#next if ($ignore{$id} ne "");

	if ($strand == 1) { # negative strand
		push(@$id, { l => $line, strand => "-", start => ($chr_length - $ref_stop), stop => ($chr_length - $ref_start), q_start => $query_start, q_stop => $query_stop } );
	}
	else {
		push(@$id, { l => $line, strand => "+", start => $ref_start, stop => $ref_stop, q_start => $query_start, q_stop => $query_stop } );
	}

	$ids{$id}=1;
}

close(fh);

foreach my $id (keys(%ids)) {
	my @ref = @$id;
	next if (@ref < 3);

	# If all alignments have the same direction it cannot be an inversion
	my %directionality;

	foreach my $item (@ref) {
		$directionality{$item->{strand}} = 1 ;
	}

	next if (keys(%directionality) == 1);

	# Is there a flip?
	# +
	# -
	# +
	#
	# or
	# -
	# +
	# -

	@ref = sort { $a->{start} <=> $b->{start} } @ref;

	for (my $i=0; $i < @ref; $i++) {
		if ( ($i + 2) < @ref) {
			if ( (@ref[$i]->{strand} eq "+" && @ref[$i+1]->{strand} eq "-" && @ref[$i+2]->{strand} eq "+") || (
				  @ref[$i]->{strand} eq "-" && @ref[$i+1]->{strand} eq "+" && @ref[$i+2]->{strand} eq "-") ) {

				# Check overlaps
				if ( (@ref[$i]->{stop} - @ref[i+1]->{start}) < 150)  {
					if ( (@ref[$i+1]->{stop} - @ref[i+2]->{start}) < 150)  {
						my $a_start = @ref[$i]->{start};
						my $a_stop  = @ref[$i]->{stop};

						my $b_start = @ref[$i+2]->{start};
						my $b_stop  = @ref[$i+2]->{stop};

						# No overlap between first and third alignment in reference-coordinate space
						if (! (( $a_start <= $b_stop ) && ( $a_stop >= $b_start )) ) {

							if ( (@ref[$i]->{q_stop} - @ref[i+1]->{q_start}) < 150)  {
								if ( (@ref[$i+1]->{q_stop} - @ref[i+2]->{q_start}) < 150)  {
									# No overlap between first and third alignment in query-coordinate space
									my $a_start = @ref[$i]->{q_start};
									my $a_stop  = @ref[$i]->{q_stop};

									my $b_start = @ref[$i+2]->{q_start};
									my $b_stop  = @ref[$i+2]->{q_stop};

									if (! (( $a_start <= $b_stop ) && ( $a_stop >= $b_start )) ) {
										# Is the query coverage approximately the same as the span?
										
										# Not acceptable:
										# ++++++++++++++++++++++++++++           -------               ++++++++++++++++++++++++++++
										
										# Acceptable:
										# ++++++++++++++++++++++++++++---------------------------------++++++++++++++++++++++++++++

										my $distal_pos1 = @ref[$i]->{start};
										my $distal_pos2 = @ref[$i+2]->{stop};
										my $distal_length = $distal_pos2 - $distal_pos1;

										my $query_length_sum = (@ref[$i]->{stop} - @ref[$i]->{start}) +
															   (@ref[$i+1]->{stop} - @ref[$i+1]->{start}) +
															   (@ref[$i+2]->{stop} - @ref[$i+2]->{start});

										my $r = ($query_length_sum / $distal_length);

										next if ( $r < 0.10);

										$signature_count ++ ;

										print("$chr $signature_count " . @ref[$i]->{l} . "\n");
										print("$chr $signature_count " . @ref[$i+1]->{l} . "\n");
										print("$chr $signature_count " . @ref[$i+2]->{l} . "\n");
									}
								}
							}
						}
					}
				}
			}
		}
	}
}