__doc__="""Module for parsing simple readmatcher output.  Usage: python parseRm5ForDeletionsCython.py <input.m5> <output>""" 

import sys
import os
import numpy as np
import insdelhmm
import pysam

rcMap = dict(zip("ACTGNactgn-", "TGACNtgacn-"))

# transition probabilities
t_prob = np.array([[ 0.99,
                     0.000000000000000000000000000000001, 
                     0.01], 
                   [ 0.000000000000000000000000000000001, 
                     0.99,
                     0.01], 
                   [ 0.00000000005, 
                     0.00000000005, 
                     0.9999999999]]) 


# emission probabilities = ((insertion (gap), match, deletion (gap)), ...)
# the order is insertion, deletion, normal
e_prob = np.array([[.9, .05, .05], [.05, .05, .9],[.1, .8, .1]])

# t_prob = np.array([[ 0.999999999999999999999999999999999, 
#                      0.000000000000000000000000000000000000000000000000000000000001, 
#                      0.000000000000000000000000000000001], 
#                    [ 0.000000000000000000000000000000000000000000000000000000000001, 
#                      0.999999999999999999999999999999999, 
#                      0.000000000000000000000000000000001], 
#                    [ 0.000000000000000000005, 
#                      0.000000000000000000005, 
#                      0.99999999999999999999]]) 
# e_prob = np.array([[.9, .05, .05], [.05, .05, .9],[.01, .98, .01]])

t_log = np.array(np.log(t_prob), dtype='f8')
#print t_log

e_log = np.array(np.log(e_prob), dtype='f8')
#print e_prob
#print e_log
def rcSeq (seq):
    return "".join(map(lambda x: rcMap[x], seq)[::-1])


def parseRm5():
    infn , outfn = sys.argv[1], sys.argv[2]
    out = open(outfn, 'w')
    counter = 0
    currid = ""

    for line in open(infn, buffering=100000):
        values = line.rstrip("\n").split(" ")
        query_id, query_length, query_start, query_end, query_strand = values[0], int(values[1]), int(values[2]), int(values[3]), values[4]
        if currid == query_id:
            counter += 1
            continue

        currid = query_id
        target_id, target_length, target_start, target_end, target_strand = values[6], int(values[7]), int(values[8]), int(values[9]), values[10]
        alignedQuery = values[17]
        alignedTarget = values[19]
        aligned = values[18]

        score = int(values[11])
        
        #query_id = "/".join(query_id.split("/")[0:3])
       
        if target_strand == "-":
            #print "negative", target_id, query_id
            #print alignedQuery
            #print alignedTarget

            alignedQuery = alignedQuery[::-1]
            alignedTarget = alignedTarget[::-1]
            aligned = aligned[::-1]

        #print "".join(map(lambda x: rcMap[x], alignedQuery))
        #print "".join(map(lambda x: rcMap[x], alignedTarget))
        aq = alignedQuery
        at = alignedTarget
        states = insdelhmm.outputDelInsCython (aq, at, t_log, e_log)
 
        
        
        #if 1 in states or 0 in states:
        if len(states) > 1:
            #printSummary(query_id, target_id, target_start, target_end, target_strand, states, alignedQuery, alignedTarget, out)
            insdelhmm.printSummaryFast(query_id, target_id, target_start, target_end, target_strand, states, alignedQuery, alignedTarget, out)
        counter += 1


def buildRefDict (refFile):
    refDict = {}
    seqList = []
    seqname = None
    f = open(refFile)
    for line in f.xreadlines():
        if line[0] == ">":
            #seqname = line.strip()[1:]
            print seqname
            if len(seqList) > 0:
                refDict[seqname] = "".join(seqList)
            seqname = line.strip()[1:]
            seqList = []
        else:
            seqList.append(line.strip())
    refDict[seqname] = "".join(seqList)
    return refDict
                       
def parseSam(refDict):
    infn , outfn = sys.argv[1], sys.argv[2]
    bamfile =  pysam.Samfile( infn, "rb" )
    out = open(outfn, 'w')
    counter = 0
    currid = ""

    tidDict = {}
    for ar in bamfile.fetch():
        query_id = ar.qname
        #query_id = "/".join(query_id.split("/")[0:3])
        if ar.tid in tidDict:
            target_id = tidDict[ar.tid]
        else:
            target_id  = bamfile.getrname(ar.tid)
            tidDict[ar.tid] = target_id
        if ar.is_secondary:
            counter += 1
            continue
        #print "target_id", target_id
        target_start = ar.aend-ar.alen
        target_end = ar.aend
        target_strand = "+"
        if ar.is_reverse:
            target_strand = "-"

        if currid == query_id:
            counter += 1
            continue
        currid = query_id
        alignedpositions = ar.aligned_pairs
        #print alignedpositions
        
        #print refDict
        alignedQuery = buildAlignmentSequence(ar.seq, alignedpositions, 0)
        alignedTarget = buildAlignmentSequence(refDict[target_id], alignedpositions, 1)
         
       
        #if target_strand == "-":
        #    print "negative", alignedpositions[0], alignedpositions[-1], target_id, query_id
            #alignedQuery = alignedQuery[::-1]
            #alignedTarget = alignedTarget[::-1]
            
        #else:
        #    print "positive", alignedpositions[0], alignedpositions[-1]
        #print alignedQuery
        #print alignedTarget

        aq = alignedQuery
        at = alignedTarget
        states = insdelhmm.outputDelInsCython (aq, at, t_log, e_log)
                       
        #if 1 in states or 0 in states:
        if len(states) > 1:
            #printSummary(query_id, target_id, target_start, target_end, target_strand, states, alignedQuery, alignedTarget, out)
            insdelhmm.printSummaryFast(query_id, target_id, target_start, target_end, target_strand, states, alignedQuery, alignedTarget, out)
        counter += 1
        

def buildAlignmentSequence (seq, aligned_pairs, index):
    charList = []
    prevposition = -1
    for aligned_pair in aligned_pairs:
        #print aligned_pair
        aln_pos = aligned_pair[index]
        if aln_pos:
            charList.append(seq[aln_pos])
        else:
            charList.append("-")
    return "".join(charList)
        

def printSummary (qid, tid, t_s, t_e, strand, states, alignedQuery, alignedTarget, out):
    n = len(states)
    normal = True
    event_start = -1
    evEnt_pos = -1
    event = -1
    pos = t_s
    event_size = 0
    qpos = 0 if strand == "+" else (n-1)
    qstart = 0
    for i in range (n):
        if states[i] != 2:
            if normal:
                normal = False
                event_start = pos
                qstart = qpos
                event_pos = pos
                eventsize = 1
                event = "D" if states[i] else "I"
            else:
                event_pos = pos
                event_size += 1
        else:
            if not(normal):
                # flip the query_start so that is less than the query_end
                qdist = abs(qpos-qstart)
                out.write("%s %s %i %i %i %i %s %s %i %i\n" %( qid, tid, t_s, t_e, event_start, event_pos+1, strand, event, event_size, qdist))
            event_size = 0
            normal = True
        if alignedTarget[i] != "-":
            pos += 1
        if alignedQuery[i] != "-":
            if strand == "-":
                qpos += -1
            else:
                qpos += 1

def outputDelIns (alignedQuery, alignedTarget, aligned):
    n = len(alignedQuery)
    v = np.zeros((3,n+2))
    p = np.zeros((3,n+2), dtype=int)
    # set all pointers to point to normal case in second row
    v[2][0] = 0.0
    v[0][0] = -1000.0
    v[1][0] = -1000.0

    for i in range(n):
        x = 1
        if alignedQuery[i] == "-":
            x = 2
        if alignedTarget[i] == "-":
            x = 0
        temp =  v[:,i] + t_log
        v[:,i+1] = e_log[:,x]+temp.max(1)
        p[:,i+1] = temp.argmax(1)

    currState = 2
    states = np.zeros(n, dtype=int)
    states[n-1] = 2
    for i in range (n, 1, -1):
        prevState = p[currState][i]
        states[i-2] = prevState
        currState = prevState
    return states


def loop1 (v, p, n, alignedQuery, alignedTarget):
    for i in range(n):
        x = 1
        if alignedQuery[i] == "-":
            x = 2
        if alignedTarget[i] == "-":
            x = 0
        subloop1b(v,p,x, i)

def subloop1b (v, p, x, i):
    temp = subadd(v,i)
    v[:,i+1] = subadd2 (temp, x)
    p[:,i+1] = temp.argmax(1)

def subadd (v,i):
    return v[:,i] + t_log

def subadd2 (temp, x):
    return e_log[:,x]+temp.max(1)

def subloop1 (v, p, x, i):    
    for s in range (3):
        prev_v = v[:,i]+t_log[:,s]
        v[:,i+1]
        maxprev = max(prev_v)
        v[s][i+1] =  e_log[s][x] + maxprev
        #p[s][i+1] = prev_v.index(maxprev)
        p[s][i+1] = np.argmax(prev_v) 

def outputDelInsSub (alignedQuery, alignedTarget, aligned):
    n = len(alignedQuery)
    v = np.zeros((3,n+2))
    p = np.zeros((3,n+2), dtype=int)
    # set all pointers to point to normal case in second row
    v[2][0] = 0.0
    v[0][0] = -1000.0
    v[1][0] = -1000.0

    loop1(v,p,n,alignedQuery, alignedTarget)

    
    # Originally taking max to find curr state but I KNOW it has
    # to start in an aligned position so just setting currState to 2
    #currState = np.argmax([v[0][n+1], [v[1][n+1], [v[2][n+1])
    currState = 2
    states = np.zeros(n, dtype=int)
    states[n-1] = 2
    for i in range (n, 1, -1):
        prevState = p[currState][i]
        states[i-2] = prevState
        currState = prevState
    return states

#import cProfile
#inFile, outFile = sys.argv[1], sys.argv[2]
#print inFile, outFile
#print 'parseRm5(%s, %s)' %(inFile, outFile)
#cProfile.run('parseRm5()')
if len(sys.argv) > 3:
    refFile = sys.argv[3]
    refDict = buildRefDict(refFile)
    parseSam(refDict)
else:
    parseRm5()

