__doc__="""Module for parsing simple readmatcher output into sv breakpoints""" 
import sys
import numpy as np
from itertools import *
import optparse, logging
from io_hupac.ReadMatcherIO import parseRm4, parseRm5

def printBreakpoints (hits, final_path, out):
    for i in range(len(final_path)-1):
        left = hits[final_path[i]]
        right = hits[final_path[i+1]]

        pairname = "_".join(map(str, [left.query_id, final_path[i], final_path[i+1]]))
        # Handle overlapping reads by trimming the left side
        raw_gap = right.query_start-left.query_end
        # scale gap by distance in target-coordinates
        gap = raw_gap * (left.target_end-left.target_start)/float(left.query_end-left.query_start)
        
        ladjust, radjust = left.score, right.score
        # keep track of the query coordinates of the event boundaries
        left_qend = left.query_end
        right_qstart = right.query_start
        
        if gap < 0:
            # for now don't do anyting to the left score, only adjust the right
            ladjust = left.score
            raln = abs(right.query_end-right.query_start)
            # if the boundaries overlap, make them non-overlapping in the query
            left_qend = right_qstart-1
            radjust = (float(raln+gap)/raln) * right.score 
            # trim either the end or start depending on strand
            if left.target_strand == "+":
                # ------------>  left
                #        ----------->  right
                #        | -g |
                #  reivsed target positions = l.target_start, l.target_end+g
                # as a last precaution make sure the target_end can't go below the left target_start
                left_target_end = max( left.target_start, left.target_end+gap )
                out.write("%s\t%i\t%i\t%s\t%i\t%i\t%s\t%i\t%s\t%s\t%i\t%i\t%i" %(left.target_id, left.target_start, left_target_end, right.target_id, right.target_start, right.target_end, pairname, 1, left.target_strand, right.target_strand, 1, 1, 0))
            else:
                # <------------  left
                #        -----------  right
                #        | -g |
                #  revised target positions = l.target_end+g,l.target_end
                # you are not changing the target_end position but shifting the target_start relative to target end
                # as a last precaution make sure the target_start can't go below zero after correction
                left_target_start = max( 0, left.target_end+gap )
                out.write("%s\t%i\t%i\t%s\t%i\t%i\t%s\t%i\t%s\t%s\t%i\t%i\t%i" %(left.target_id, left_target_start, left.target_end, right.target_id, right.target_start, right.target_end, pairname, 1, left.target_strand, right.target_strand, 1, 1, 0))
        else:
            out.write("%s\t%i\t%i\t%s\t%i\t%i\t%s\t%i\t%s\t%s\t%i\t%i\t%i" %(left.target_id, left.target_start, left.target_end, right.target_id, right.target_start, right.target_end, pairname, 1, left.target_strand, right.target_strand, 1, 1, gap))
        
        out.write("\t%i\t%f\t%i\t%i\t%f\t%i\t%i\t%i\t%i\t%i\n" %(left.score, left.pctidentity, ladjust, right.score, right.pctidentity, radjust, left.query_start, left.query_end, right.query_start, right.query_end))

def writeESPFile (espout, esplist):
    ''' Write ESPList to esp out file'''
    for esp in esplist:
        #espout.write(str(esp))
        espout.write(esp.prStr())
        espout.write("\n")
    
 
class ReadBreakpoints:
    def __init__( self ):
        self.__parseArgs( )
        self.__initLog( )

    def __parseArgs( self ):
        """Handle command line argument parsing"""
        
        usage = "%prog [--help] [options] input_filename output_filename"
        parser = optparse.OptionParser( usage=usage, description=__doc__ )

        parser.add_option( "-l", "--logFile", help="Specify a file to log to. Defaults to stderr." )
        parser.add_option( "-d", "--debug", action="store_true", help="Increases verbosity of logging" )
        parser.add_option( "-i", "--info", action="store_true", help="Display informative log entries" )
        parser.add_option( "-p", "--profile", action="store_true", help="Profile this script, dumping to <scriptname>.profile" )
        parser.add_option( "-t", "--inputType", help="blasr output format (m5, m4)")
        parser.add_option( "-e", "--espOutputFile", help="esp output file to write otherwise no ouptut")
        parser.add_option( "--pctIdentityCut", help="cutoff for pctidentity to accept hit")

        parser.set_defaults( logFile=None, debug=False, info=False, profile=False, inputType="m4", espOutputFile=None, pctIdentityCut=0)
        
        self.opts, args = parser.parse_args( )

        if len(args) != 2:
            parser.error( "Expected 2 arguments." )

        self.infn, self.outfn = args[0], args[1]
        self.score_cutoff = 500
        self.inputType = self.opts.inputType
        self.espFlag = False
        if self.opts.espOutputFile != None:
            self.espFlag = True
            self.espOut = open(self.opts.espOutputFile, 'w')
        self.pctIdentityCut = float( self.opts.pctIdentityCut )


    def __initLog( self ):
        """Sets up logging based on command line arguments. Allows for three levels of logging:
        logging.error( ): always emitted
        logging.debug( ): emitted with --info or --debug
        logging.debug( ): only with --debug"""

        logLevel = logging.DEBUG if self.opts.debug else logging.INFO if self.opts.info else logging.ERROR
        logFormat = "%(asctime)s [%(levelname)s] %(message)s"
        if self.opts.logFile != None:
            logging.basicConfig( filename=self.opts.logFile, level=logLevel, format=logFormat )
        else:
            logging.basicConfig( stream=sys.stderr, level=logLevel, format=logFormat )
                                                                 
    def run( self ):
        """Executes the body of the script."""
    
        logging.info("Log level set to INFO")
        logging.debug("Log Level set to DEBUG")

        self.buildBPInput ()
        # close esp out file
        if self.espFlag:
            self.espOut.close()
        return 0


    def buildBPInput( self ):
        '''Read through blasr output group by read.  
           Build Graph for each read and write output'''
        out = open(self.outfn, 'w')
        if self.inputType == "m5":
            hits = parseRm5(self.infn)
        else:
            hits = parseRm4(self.infn)
        counter = 0
        for k, g in groupby(hits, lambda x: x.query_id):
            counter += 1
            if counter % 100000 == 0:
                logging.info("processed %i reads" %(counter))
            qhits = filter(lambda x: x.score > self.score_cutoff and x.pctidentity > self.pctIdentityCut, list(g))
            if len(qhits) == 0:
                continue
            # to speed up analysis don't build graphs on alignments that are clearly top alignments
            qhits.sort(key=lambda x: x.score, reverse=True)
            maxIden = max(map(lambda x: x.pctidentity, qhits))
            maxHit = qhits[0]
            minStart = min(map(lambda x: x.query_start, qhits))
            maxEnd = max(map(lambda x: x.query_end, qhits))
            # if the top alignment has the highest accuracy and is within 100 bps of minimal observed
            # query start adn query end alignments then simply accept it and move on to the next alignment
            if maxHit.query_start <= minStart + 100 and maxHit.query_end >= maxEnd - 100 and maxIden == maxHit.pctidentity:
                #print minStart, maxEnd, maxIden
                #logging.info(str(maxHit))
                continue
            if len(qhits) > 1:
                self.getBreakpointsFromHits (qhits, out)


    def buildGraphFromHits (self, hits, scoreCutoff=500):
        ''' Build a graph from a list of hits for the query.
            Each node corresponds to an alignemnt each edge 
            corresponds to a pair of alignments that are sufficiently
            non-overlapping to allow pairs to both exist. '''
        n = len(hits)
        hits.sort(lambda x,y: cmp(x.query_start, y.query_start))
        # for now keep a matrix of all events (note, this is not efficient!)
        scoreMatrix = np.zeros((n, n))-1000
        # keep a list of ESPs
        ESPList = []

        if self.espFlag:
            # items needed for ESP
            qname = hits[0].query_id
            # iterate over hits to make standard ESPAlignemnts
            ESPAligns = []
            for i, hit in enumerate(hits):
                #if hit.target_strand == "+":
                #    print "found pos alignment!"
                ESPAligns.append(ESPAlignment(hit.query_start, hit.query_end, hit.target_id, hit.target_start, hit.target_end, hit.pctidentity, hit.target_strand))

        # iterate over each pair of hits            
        for i,hit in enumerate(hits):
            logging.debug(str(hit))
            scoreMatrix[i][i] = hit.score
            hit_query_align_len = hit.query_end-hit.query_start
            for j in range (i, n):
                hitj = hits[j]
                logging.debug("--" + str(hitj))
                if hitj.query_start >= hit.query_end:
                    logging.debug("    j start greater than i end")
                    scoreMatrix[i][j] = hitj.score
                    # Add in standard ESP
                    if self.espFlag:
                        q_dist = hitj.query_start-hit.query_end
                        espij = ESP (qname, ESPAligns[i], ESPAligns[j], q_dist)
                        ESPList.append(espij)
                else:

                    if hit.query_end < hitj.query_end:
                        logging.debug("    j start less than i end, but j end > i end")
                        # determine the overlap fraction
                        # hiti querystart---------------------queryend
                        # hitj              querystart-------------------queryend
                        # overlap = hiti.query_end-hitj.query_start
                        alignLen = hitj.query_end-hitj.query_start
                        overlapLen = hit.query_end-hitj.query_start

                        # make sure the remaining alignments aren't of trivial length
                        if hit_query_align_len - overlapLen < 101 or alignLen - overlapLen < 101:
                            continue
                        # put in an artificial penalty to select an additiona alignment
                        # this is achieved by increasing th overlaplen
                        overlapFrac = (float(overlapLen+100)/alignLen)
                        newScore = hitj.score-hitj.score*overlapFrac
                        logging.debug("       overlap = %i, overlapFrac = %f" %(overlapLen, overlapFrac))
                        logging.debug("       original score = %i, new score = %f" %(hitj.score, newScore))
                        if newScore > scoreCutoff:
                            scoreMatrix[i][j] = newScore
                            if self.espFlag:
                                # correct ESP alignments in overlap region                          
                                newEnd1, newStart2 = hitj.query_start, hit.query_end
                                trimAmount = hit.query_end-hitj.query_start
                                target_start1, target_end1 = self.trimAlignments(hit, trimAmount, True )
                                target_start2, target_end2 = self.trimAlignments(hitj, trimAmount, False )

                                
                                espaligni = ESPAlignment(hit.query_start, newEnd1, 
                                                         hit.target_id, target_start1, target_end1,
                                                         hit.pctidentity, hit.target_strand)
                                espalignj = ESPAlignment(newStart2, hitj.query_end, 
                                                         hitj.target_id, target_start2, target_end2,
                                                         hitj.pctidentity, hitj.target_strand)                            
                                espij = ESP (qname, espaligni, espalignj, 1)
                                ESPList.append(espij)
        return scoreMatrix, ESPList

    def trimAlignments (self, hit, trimAmount, trimEnd = False):
        ''' Return target coordinates of trimmed alignments'''
        targetQueryRatio = float(hit.target_end-hit.target_start)/(hit.query_end-hit.query_start)
        targetTrimAmount = int(trimAmount*targetQueryRatio)+1
        target_start = hit.target_start
        target_end = hit.target_end
        if hit.target_strand == "+":
            if trimEnd:
                target_end = max(hit.target_start, hit.target_end-targetTrimAmount)
            else:
                target_start = min(hit.target_end, hit.target_start+targetTrimAmount)
        else:
            if trimEnd:
                target_start = min(hit.target_end, hit.target_start+targetTrimAmount)
            else:
                target_end = max(hit.target_start, hit.target_end-targetTrimAmount)
        return target_start, target_end
            
    def getBreakpointsFromHits (self, hits, out):
        ''' For each set of hits build an alignment
            graph findthe optimal path through the alignment
            graph'''
        n = len(hits)
        s, esplist = self.buildGraphFromHits (hits)
        bestScore = np.zeros(n)
        bestPath = np.zeros(n, dtype=int)
        logging.debug("DP Matrix:\n %s" %("\n".join(map(lambda x: "\t".join(map(lambda y: str(int(y)), x)), s))))
        # initialize at i = 0
        sameScore = np.zeros(n)
        bestScore[0] = s[0][0]
        bestPath[0] = -1
        
        # fill in the rest of the array using a dp solution
        for i in range (1,n):
            logging.debug("DP Traversal i: %i" %(i))
            bestScore[i] = s[i][i]
            sameScore[i] = 1
            bestPath[i] = -1
            for j in range (0,i):
                logging.debug("-- DP Traversal j: %i" %(j))
                # check to see if I should even consider this hit
                if s[j][i] <= 0:
                    continue
                newScore = bestScore[j]+s[j][i]
                if newScore > bestScore[i]:
                    logging.debug("  New Best Score: %s" %(newScore))
                    bestScore[i] = newScore
                    bestPath[i] = j
                    sameScore[i] = 1
                elif newScore == bestScore[i]:
                    sameScore[i] += 1

        logging.debug("bestScore: " + str(bestScore))
        # Traverse path
        end = np.argmax(bestScore)
        curr = end
        final_path = [] 
        while (curr != -1):
            final_path.append(curr)
            if sameScore[curr] > 1:
                final_path = []
                break
            curr = bestPath[curr]
        
        final_path = final_path[::-1]
        
        logging.debug("DP Best Path: " + str(final_path))

        # Only print out the final path if its greater than a single alignment
        if (len(final_path) > 1):
            printBreakpoints (hits, final_path, out)
            if self.espFlag:
                writeESPFile (self.espOut, esplist)

class ESP: 
    ''' Object to represent GASV ESPs '''
    def __init__( self, qname, esp1, esp2, query_dist):
        self.qname   = qname
        self.esp1 = esp1
        self.esp2 = esp2
        self.query_dist = query_dist
        
    def __str__ ( self ):
        newqname = "%s:%s:%s" %(self.qname, self.esp1.alignName(), self.esp2.alignName())
        return "%s\t%s\t%s\t%i" %(newqname, str(self.esp1), str(self.esp2), self.query_dist)

    def prStr ( self ):
        #newqname = "%s:%s:%s" %(self.qname, self.esp1.alignName(), self.esp2.alignName())
        #return "%s\t%s\t%s\t%i" %(newqname, str(self.esp1), str(self.esp2), self.query_dist)
        tid1     = self.esp1.tid
        tid2     = self.esp2.tid
        
        if self.esp1.strand == "+":
            strand1 = "+"
            start1  = self.esp1.start
        else:
            strand1 = "-"
            start1  = self.esp1.end
        if self.esp2.strand == "+":
            strand2 = "-"
            end2 = self.esp2.end
        else:
            strand2 = "+"
            end2 = self.esp2.start
        dist = (self.esp2.qend - self.esp1.qstart)
        #if start1 < end1:
        newqstr    = "%s %i %i %s %s %i %i %s %i" %(tid1, start1, start1, strand1, tid2, end2, end2, strand2, dist)
        #else:

        return newqstr

        
class ESPAlignment:
    ''' Object to represent one of alignment in an ESP Alignment Pair '''    
    def __init__( self, qstart, qend, tid, start, end, pctid, strand):
        self.qstart = qstart
        self.qend  = qend
        self.tid    = tid
        self.start  = start
        self.end    = end
        self.pctid = pctid
        self.strand = strand
        if qstart < 0 or self.qend < 0:
            print >>sys.stderr, self.alignName(), str(self)
        
    def alignName ( self ):
        return "%i_%i_%s_%i_%i" %(self.qstart, self.qend, self.tid, self.start, self.end)
    
#    def prStr ( self ):
#        return "%s\t%i\t%i\t%f" %(self.tid, self.start, self.end, self.pctid)    

    def __str__ ( self ):
        return "%s\t%i\t%i\t%f" %(self.tid, self.start, self.end, self.pctid)    

if __name__ == "__main__":
    app = ReadBreakpoints()
    if app.opts.profile:
        import cProfile
        cProfile.run( 'app.run()', '%s.profile' % sys.argv[0] )
    sys.exit( app.run() )


