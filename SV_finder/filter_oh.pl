#!/usr/bin/perl

use strict 'vars';

my @files = <./output.ins_overhang/*>;

foreach my $file (@files) {
	open(fh, $file);

	while (my $line = <fh>) {
		chomp($line);

		$line =~ /^\S+\t\S+\t\S+\t\S+\t(\S+)\t/;

		my $foo = $1;
		$foo =~ /(\d+)_(\d+)/;
		my ($left, $right) = ($1,$2);

		if ($left > 1 && $right > 1) {
			print("$line\n");
		}
	}

	close(fh);
}
