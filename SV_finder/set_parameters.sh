#!/usr/bin/sh
#This file contains the many pointers required for running this without
#having to have several TB of copied files. 

#It also outputs a single "parameters.log" file that can be referenced
# and parsed by any script in the package for finding references, reads
# etc.

M4=$1
M5=$2
SAM=$3
REF=$4

echo "M4 "$M4 > parameters.log
echo "M5 "$M5 >> parameters.log
echo "SAM "$SAM >> parameters.log

#folder containing the reference to be used. That folder should contain:
#  a fasta                         (e.g., hg38.fa)
#  an faidx index of that fasta    (e.g., hg38.fa.fai)
#   that matches file names with 
#   the above fasta
#  a blasr suffix array            (hg38.fa.sa)
# ...from REF=$4

if [ $REF = "" -o $REF = "hg38" ]; then
    ./sh get_hg38.sh
    echo "REF "$PWD"/hg38.fa" >> parameters.log
    echo "SA "$PWD"/hg38.fa.sa" >> parameters.log
    echo "FAIDX "$PWD"/hg38.fa.fai" >> parameters.log

elif [ $REF = "hg19" ]; then
    ./sh get_hg19.sh
    echo "REF "$PWD"/hg19.fa" >> parameters.log
    echo "SA "$PWD"/hg19.fa.sa" >> parameters.log
    echo "FAIDX "$PWD"/hg19.fa.fai" >> parameters.log
else
    ln -s $REF .
    ln -s $REF.fai .
    ln -s $REF.sa .
    echo "REF " $REF >> parameters.log
    echo "SA "$REF".sa" >> parameters.log
    echo "FAIDX "$REF".fai" >> parameters.log

fi

# a sam file, which will be simlinked to the working directory
# if $SAM has extension .fofn, each file will be converted to bam,
# sorted and indexed, then merged into example.bam
# ...from SAM=$3
if [$SAM == *".fofn"]; then
    module load samtools
    while read r; do samtools view $r | samtools sort ${r//.sam/.sorted}; done < $SAM
    for file in *.sorted.bam; do samtools index $file; done
    samtools merge example.bam *sorted.bam
else
    ln -s $SAM example.sam
    samtools view example.sam | samtools sort example.bam
    samtools index example.bam
fi

# a fofn of .m5 blasr output files (with any bestn output) to be linked to the cwd
# this negates the need for a splitting step in run_dp_hmm.sh since most alignment
# zeroeth steps are done in a split fashion.
#NOTE: this will be deprecated with the migration to pure sam format input files.
# ...from M5=$2

while read r; ln -s $r . ; done < $M5

# ...from M4=$1

ln -s $M4 example.m4
