#!/bin/bash

#ADD CLEANERS HERE!


#Inversions type 1a
#==================
#See inversions.png
>&2 echo "INVERSIONS TYPE 1A"

>&2 echo "INVERSIONS TYPE 1A: STEP 1"
#1.# Bam to bed:
#bamToBed -i example.bam | awk '$1 !~ /chr[YM]/ && $1 !~ /_/' > example.bed

>&2 echo "INVERSIONS TYPE 1A: STEP 2"
#2.# Bam to sam:
#samtools view example.bam | awk '$3 !~ /chr[YM]/ && $3 !~ /_/' > example.sam

>&2 echo "INVERSIONS TYPE 1A: STEP 3"
#3.# Create it in case it doesn't exist
mkdir raw_coords

# Clean it
rm -v ./raw_coords/*

# Split by chromosome:
gawk '{fn="./raw_coords/example.bed."$1; print $0 > fn}' example.bed

>&2 echo "INVERSIONS TYPE 1A: STEP 4"
#4.# Sort:
for file in ./raw_coords/example.bed.*
do
    echo $file
    sort -T . -S 10G -k 4,4V $file > ${file}.S
done

>&2 echo "INVERSIONS TYPE 1A: STEP 5"
#5.# Get alignment signatures:
mkdir -p output.inversions_t1 jobs.inversions_t1 logs.inversions_t1
rm -v output.inversions_t1/*
rm -v jobs.inversions_t1/*
rm -v logs.inversions_t1/*

working_dir=`pwd`

for file in ./raw_coords/*.S
do
 prefix=${file##*/}
 job=./jobs.inversions_t1/${prefix}.job
 out=./output.inversions_t1/${prefix}.overlaps.out

# echo "#BSUB -L /bin/bash
#BSUB -n 1
#BSUB -J ${prefix}
#BSUB -oo ../logs.inversions_t1/${prefix}.log
#BSUB -eo ../logs.inversions_t1/${prefix}.err
#BSUB -q <name of the queue goes here>
#BSUB -W 03:00
 echo "cd $working_dir; perl inversions.type_1.get_alignment_overlaps.pl $file > $out" > $job
done

#cd jobs.inversions_t1
chmod u+x ./jobs.inversions_t1/*.job
ls ./jobs.inversions_t1/*.job > ./jobs.inversions_t1/cmds.txt
parallel -j 40 < ./jobs.inversions_t1/cmds.txt
#cd ..

>&2 echo "INVERSIONS TYPE 1A: STEP 6"
#6.# Merge:
cat ./output.inversions_t1/*.S.overlaps.out | sort -u | sort -k 1,1V -k 5,5n > ./output.inversions_t1/merged.bed.S.overlaps.out

>&2 echo "INVERSIONS TYPE 1A: STEP 7"
#7.# Filter alignment signatures:
awk '{print $1}' ./output.inversions_t1/merged.bed.S.overlaps.out | sort -u > ./output.inversions_t1/merged.bed.S.overlaps.out.reads.txt

awk '{
      if (FILENAME ~ /merged.bed.S.overlaps.out.reads.txt/) {
	          targets[$1] = 1;
		    }
	    else {
	          if (targets[$1] != "") {
			        print($0);
				    }
		        }
	  }' ./output.inversions_t1/merged.bed.S.overlaps.out.reads.txt example.sam > example.sam.F

>&2 echo "INVERSIONS TYPE 1A: STEP 8"
#8.# Split by chromosome:
gawk '{
     fn="./output.inversions_t1/merged.bed.S.overlaps.out."$2
      print $0 > fn
      }' ./output.inversions_t1/merged.bed.S.overlaps.out

# And split again:
for file in `ls ./output.inversions_t1/merged.bed.S.overlaps.out.* | grep -v reads`
do
    lines=`wc -l $file | awk '{print $1}'`
    echo $lines
    lines_per_query=`echo "1+$lines/20" | bc`
    >&2 echo $lines_per_query
    >&2 echo "Am I causing an issue with split?"
    >&2 echo "My name is $file and I have $lines lines and $lines_per_query lines per query."
    split -d -l $lines_per_query $file ${file}.
done

>&2 echo "INVERSIONS TYPE 1A: STEP 9"
#9.# Check identity in the gap region:
(for file in `ls ./output.inversions_t1/merged.bed.S.overlaps.out.*.* | grep -v reads`
do
chr=`echo $file | sed -E 's/.*\.(chr[0-9X]*)\..*/\1/'`
prefix=${file##*/}
output=./raw_coords/${prefix}.out

echo "perl alignment_signature_percent_sequence_identity.pl $file $chr example.sam.F > ${output}"
done) > cmds.txt

# GNU parallel should be in your path.
# $ parallel --version
# GNU parallel 20130222
# Adjust 40 to the number of CPUs you have available.
parallel -j 40 < cmds.txt

>&2 echo "INVERSIONS TYPE 1A: STEP 10"
#10.# Merge output:
>&2 echo "Contents of raw_coords/"
>&2 ls ./raw_coords/*

for file in ./raw_coords/*.*.*.out
do
 awk '{
  fn="./raw_coords/merged."$2
  print($0) >> fn
   }' $file
 done

>&2 echo "INVERSIONS TYPE 1A: STEP 11"
#11.# Table:
perl merge_identity_information.pl "./raw_coords/merged.chr*" ./output.inversions_t1/merged.bed.S.overlaps.out > ./output.inversions_t1/merged.bed.S.overlaps.out.identities

>&2 echo "INVERSIONS TYPE 1A: STEP 12"
#12.# Cluster alignments into inversion calls:
R --no-save --args ./output.inversions_t1/merged.bed.S.overlaps.out.identities ./output.inversions_t1/ < inversions.type_1.cluster.R

>&2 echo "INVERSIONS TYPE 1A: STEP 13"
#13.# Post-processing:
(echo -e "chr\tinversion_start\tinversion_stop\tcluster_start_sd\tcluster_stop.sd\tcluster_start.sd.norm\tcluster_stop.sd.norm\tread_counts\tsize.mean\tsize.sd\tsize.norm_sd\treads"; (perl inversions.type_i.post_clustering_filter.pl output.inversions_t1 3 0.3 | sort -k 1,1V -k 2,2n)) > inversions.type_1.txt

>&2 echo "INVERSIONS TYPE 1A: STEP 14"
#14. # To bed:
grep -v -P '^chr\t' inversions.type_1.txt | awk 'BEGIN {OFS="\t"} {print $1,($3-1),$4,$2}' > inversions.type_1.bed


#Inversions type 1b
#==================
#See inversions.png

>&2 echo "INVERSIONS TYPE 1b: STEP 1"
#1.# Create it in case it doesn't exist
mkdir -p raw_coords

# Clean it
rm -v ./raw_coords/*

gawk '$2 ~ /^chr[0-9X]*$/ {
 fn="./raw_coords/"$2".example.m4"
 print($0) > fn
}' example.m4

>&2 echo "INVERSIONS TYPE 1b: STEP 1"
#2. # Get signatures:

mkdir -p jobs.inversions_t1b logs.inversions_t1b output.inversions_t1b
rm jobs.inversions_t1b/*
rm logs.inversions_t1b/*
rm output.inversions_t1b/*

working_dir=`pwd`

for file in ./raw_coords/chr*
do
     prefix=${file##*/}
      prefix=${prefix%.*}
       prefix=${prefix%.*}

        job=./jobs.inversions_t1b/${prefix}.job
	 out=./output.inversions_t1b/inv_t1b_all.${prefix}.txt

#	 echo "#BSUB -L /bin/bash
#BSUB -n 1
#BSUB -J ${prefix}
#BSUB -oo ../logs.inversions_t1b/${prefix}.log
#BSUB -eo ../logs.inversions_t1b/${prefix}.err
#BSUB -q <name of the queue goes here>
#BSUB -W 12:00
	 echo "cd $working_dir; perl get_inv_type_1b_signatures.pl $file > $out" > $job

done

#cd jobs.inversions_t1b
chmod u+x ./jobs.inversions_t1b/*.job
ls ./jobs.inversions_t1b/*.job > ./jobs.inversions_t1b/cmds.txt
parallel -j 40 < ./jobs.inversions_t1b/cmds.txt
#cd ..


#Inversions type 2
#=================
#See inversions2.png

>&2 echo "INVERSIONS TYPE 2: STEP 1"
#1.# Filtering:
awk -F '\t' '$1 !~ /_/ && $1 != "chrY" && $1 != "chrM" {
if ($1 == $4 && (($9 == "+" && $10 == "-") || ($9 == "-" && $10 == "+")) ) {
a_start=$20;
a_stop=$21;

b_start=$22;
b_stop=$23;

# (StartA <= EndB) and (EndA >= StartB)
overlap=((a_start <= b_stop) && (a_stop >= b_start))

# No overlap
if (overlap == 0) {
  print($0)
}
else {
  foo=a_stop-b_start;

  if (foo < 0) {
   foo = foo * -1;
  }

  if (foo < 100) {
    print($0)
  }
}
}
}' example.m4.bp.F | awk -F '\t' '{
a_start=$2;
a_stop=$3;

b_start=$5;
b_stop=$6;

overlap=((a_start <= b_stop) && (a_stop >= b_start))

# No overlap
if (overlap == 0) {
  print($0)
}
else {
  foo=a_stop-b_start;

  if (foo < 0) {
   foo = foo * -1;
  }

  if (foo < 100) {
    print($0)
  }
}
}' | awk '($5-$3) < 1000000' > example.m4.bp.F.F

>&2 echo "INVERSIONS TYPE 2: STEP 2"
#2.# Cluster:
mkdir -p output.inversions_t2
rm output.inversions_t2/*

R --no-save --args `pwd` example.m4.bp.F.F hg19_gaps.txt ./output.inversions_t2/ < inversions.type_ii.cluster.R

>&2 echo "INVERSIONS TYPE 2: STEP 3"
#3.# Filter:

(echo "chr inversion_start inversion_stop read_counts size.mean size.sd norm_sd reads"; (perl inversions.type_ii.post_clustering_filter.pl ./output.inversions_t2 3 0.3 | sort -k 1,1V -k 2,2n)) > inversions.type_ii.dp.txt