# -------------------------------------
#  GRID CONFIGURATION
# -------------------------------------
#
#  By default, do NOT use SGE; use manual override with command-line options.
#
useGrid                = 0
scriptOnGrid           = 0
frgCorrOnGrid          = 0
ovlCorrOnGrid          = 0

#
# -------------------------------------
#  ERROR RATES
# -------------------------------------
#
#  Expected rate of sequencing error. Allow pairwise alignments up to this rate.
#  
utgErrorRate        = 0.03  # previously {0.10,0.03,0.025,0.015 and 0.030 = default}
utgErrorLimit       = 4.5  # previously {4.5,2.5 and default = 2.5} 
ovlErrorRate        = 0.03  # Larger than utg to allow for correction.
cnsErrorRate        = 0.06  # previously 0.25. Larger than utg to avoid occasional consensus failures
cgwErrorRate        = 0.1 # previously 0.25 and this has to be >= cnsErrorRate. Larger than utg to allow contig merges across high-error ends
#
# -------------------------------------
# STAGE CONFIGURATION
# -------------------------------------
#
frgMinLen           = 4500
ovlMinLen           = 40    # default

# MERYL
merylMemory         = 128000
merylThreads        = 32
# 
# OVERLAPPER
merSize             = 22    # default=22; use lower to combine across heterozygosity, higher to separate near-identical repeat copies 
merThreshold        = 250   # ???
overlapper          = ovl   # the mer overlapper for 454-like data is insensitive to homopolymer problems but requires more RAM and disk
#ovlMemory           = 8GB --hashload 0.8 --hashstrings 400000 --hashdatalen 400000000
#ovlHashBlockSize    = 8000000
#ovlRefBlockSize     = 32000000
ovlConcurrency      = 32          # Not needed when using grid

ovlHashBits         = 24
ovlThreads          = 32
ovlHashBlockLength  = 800000000
ovlRefBlockSize     =  100000000
#
# OVERLAP STORE
ovlStoreMemory      = 128000      # Must be in Mbp; keep it a bit below the max available memory to avoid malloc errors
# 
# ERROR CORRECTION
frgCorrThreads      = 32
frgCorrBatchSize    = 48000000   # We want this to be as big as possible. A batch of 50M reads will use around 100 Gb of memory.
frgCorrConcurrency  = 1          # not needed when using grid
ovlCorrBatchSize    = 2500000    # 2.5M reads can run on a single GPC node (4M should work too); submitting 80 at a time works nicely
ovlCorrConcurrency  = 32         # not needed when using grid
#
# UNITIGGER
unitigger           = bog       # Use bog when dealing with Illumina reads
utgBubblePopping    = 1
# utgGenomeSize     =
#
# CONSENSUS
cnsConcurrency      = 32
cnsPartitions       = 175 
cnsMinFrags         = 50000 
#
# SCAFFOLDING
doExtendClearRanges = 0          # Let's skip extended clear ranges during scaffolding for now
#
# -------------------------------------
