NA12878 Architecture README
==============================================================================

This repository is for code used in the manuscript:

Pendleton M, Sebra R, Pang AW, Ummat A, Franzen O, Rausch T, Stütz AM, Stedman W, Anantharaman T, Hastie A, Dai H, Fritz MH, Cao H, Cohain A, Deikus G, Durrett RE, Blanchard SC, Altman R, Chin CS, Guo Y, Paxinos EE, Korbel JO, Darnell RB, McCombie WR, Kwok PY, Mason CE, Schadt EE, Bashir A. Assembly and diploid architecture of an individual human genome via single-molecule technologies. Nat Methods. 2015 Aug;12(8):780-6. doi: 10.1038/nmeth.3454 [[Online Link]](http://www.nature.com/nmeth/journal/v12/n8/full/nmeth.3454.html)

This README enumerates setup details for code, in addition to what is indicated in each package's respective
README files. 

Raw data files used in the manuscript can be found at: https://bashia02.u.hpc.mssm.edu/NA12878_architecture/.

1. Error-correction and Assembly
=================================
FALCON
-------
Falcon is an experimental PacBio diploid assembler.  This version of the package used in our study has been linked recursively in the git repository.

To run falcon you can use the provided parameter file "falcon.cfg" as well as
a "fofn" (file of filenames) pointing to the initial filtered subreads file for your data.  

Celera Assember
------------------------
The most current version of this may be reached at the project's homepage:

  http://wgs-assembler.sourceforge.net/wiki/index.php?title=Main_Page

Version used is at this link:
http://sourceforge.net/p/wgs-assembler/svn/4502/tree/

The parameter file we used for our assembly is "celera_assembler.spec".

2. BioNano-PacBio Mapping tool
=======================
This tool, as written currently requires the input data to be locally organized
in a folder ./input_data/. Because including the data explicitly as part of this 
repository would balloon its size tremendously, it will be available from a 
website that is soon to be set up with toy data sets and full data sets for 
testing all the various algorithms involved.
In the meantime, the bionano package with data is available for direct download
at the following website:

https://pendlm02.u.hpc.mssm.edu/downloads/script_release_bionano.tar.bz2

The included raw data includes the two incrementally applied PacBio assemblies as fasta files and the BioNano cmap file. These files are included in the 
folder named "input_data" as:

* consensus_diploid_for_andy_forncbi_filtered2b.fa (first PB assembly, generated using in-house assembly pipeline)

* primary_tigs_c_copy.fa (second, more aggressive PB assembly, generated using Falcon)

* EXP_REFINEFINAL1.cmap (BioNano assembly)

Before running this code, some minimal setup is required. The number of 
processors available, the available memory and the path to RefAligner must be
specified in a pair of xml parameter files contained in the ./xml/ folder, 
under the fields:

* refaligner path:
This is specified in the flag attribute "refaligner". Change the quoted path
at val0 to the installation directory for refaligner.

* memory:
This is specified in the flag attribute "maxmem. Change the quoted value
at val0 to the amount of available RAM in GB.

* cores:
This is specified in the flag attribute "maxthreads". Change the quoted value
at val0 to the number of cores available for hybrid assembly.

Once configured, the pipeline is run by executing the perl script in the base
directory. Further instructions may be found in the package's own README.

2b. Additional BioNano related tools
====================================

Contig Swap
-----------------
This set of scripts can be used to swap a new contig set into a pre-constructed hybrid map
 for cases when a set of contigs are error corrected or otherwise improved and the user would like to 
swap the improved contigs into the hybrid assembly. The precondition for doing this is that it cannot 
be used on completely newly assembled contigs; they must be the same contigs with the same names.


Build contigs.fa from hybrid map
--------------------------------------------

These scripts are used to generate a fasta from hybrid optical contigs. Alternatively, it is possible to do
so in Windows using Bionano's own IrysView suite.

Dependencies:
*[Nucmer/3.23](http://mummer.sourceforge.net/)
*[pbcore](https://github.com/PacificBiosciences/pbcore)

Generate AGP from hybrid map
-------------------------------------------

3. HapCut
=========

This package has been linked recursively to its git repository.


4. PBHoney - A Structural Variation finder
==========================================

Since the method described in the paper consisted of generating a consensus 
between our own in-house SV calls and PBHoney, the PBhoney version that was used
can be found at this link:

http://sourceforge.net/projects/pb-jelly/files/PBSuite_14.5.13.tgz

5. In-house SV_finder
==============================

Software requirements
=====================
* linux
* wget
* samtools
* python
* python (for building)
* numpy, scipy
* pbcore (from the pacbio github repository)
* perl
* awk
* gawk
* GNU parallel
* R and the following libraries: IRanges, intervals, hash
* BioPerl
* perlmodules: Statistics/Basic.pm
* perlmodules: Number/Format.pm
* bedtools  (BEDTools 2.21.0)
* pysam (pysam 0.7.6)

Dependency command on Mount Sinai's Minerva:
```
module load python; 
module load py_packages; 
module load samtools; 
module load gcc; 
module load bedtools; 
module load bioperl; 
module load perlmodules/Number-Format; 
module load perlmodules/Statistics-Basic; 
module load R; 
module load parallel;
```

Step 0:
Download hg_19 reference fasta using the provided script while connected to the internet. 
```
#!bash
bash get_hg19.sh
```

Copy the reference gap table ./NA12878_architecture/examples/hg19_gap.txt to the $PWD as well.

Step 1:
From fasta file of reads, create blasr alignment files of type .m4, .m5 and .sam:

Provided that the reads are kept in a file named "reads.fasta", the following commands can be used. Change PROC so that it is equal to the number of processors available for mapping:

```
#!bash

export REF=/path/to/reference.fasta
PROC=32
blasr reads.fasta $REF -noSplitSubreads -clipping soft -m 4 -out example.m4 -nproc $PROC
blasr reads.fasta $REF -noSplitSubreads -clipping soft -m 5 -out example.m5 -nproc $PROC
blasr reads.fasta $REF -noSplitSubreads  -clipping soft -sam -out example.sam -nproc $PROC
samtools view -bS example.sam | samtools sort - example.sorted
mv example.sorted.bam example.bam
samtools index example.bam
```
Step 2:
With mapped reads in the current directory along with all associated .pl and .sh scripts,
The four controller scripts can then be invoked in the following order. 
Note for inversion finding: There are two lengthy steps at the beginning of the inversion
finding script that are commented out. One generates a file example.bed from the indexed bam
file. The other generates a sam equivalent from the indexed bam file. If you have your original 
sam file, the sam file creation step may be skipped by symbolically linking the sam file to 
the file name example.sam.
(Alternatively, adding the SV_finder folder to the environment $PATH variable would probably work, though we haven't tested it). These specific commands also provide some minimal logging in case something goes wrong:


```
#!bash

ln -s $REF ./hg19.fa
ln -s $REF.fa ./hg19.fa.fai
sh run_dp_hmm.sh > dp_hmm_log.txt 2> dp_hmm_log.err
sh run_deletions.sh > del_log.txt 2> del_log.err
sh run_insertions.sh > ins_log.txt 2> ins_log.err
sh run_inversions.sh > inv_log.txt 2> inv_log.err

```
Step 3: Tabulation of results
Each of these scripts outputs results in a .bed-like format that additionally includes the ZMW_id of any reads that support the event in question. 

6. PACMonSTR - A tandem repeat assessor
=======================================

This code is from [Ummat, et al. 2014](http://www.ncbi.nlm.nih.gov/pubmed/25028725)

Briefly, it takes in a blasr .m5 format alignment of reads to a reference genome, a binned_anchor 
file and a table of known tandem repeats. It assesses the TRs for copy number in all reads, then
clusters the results of all single read results to identify zygosity.

A small test set of data has been included, and can be run by directly copying the commands below, 
noting that setting path to the working directory for PACMonSTR must be done first.

A. For running the pipeline, please run the following:
python $path/pacMonStr_V1.py $path/test.m5 $path/test.binned_anchors $flank $numCore $runDir

1. path="The location of PacmonSTR directory"

2. $path/test.m5 = The input alignment between the long reads data and the reference. Here .m5 format which is an output of 'Blasr' tool is assumed.

3. $path/test.binned_anchors = The long reads (based on their alignement start and end positions with respect to the reference) that span the TR event (defined for each chromosome by th
eir start position, end position, repeat element, repeat period, repeat multiplicity, among other attributes) are written in this file with their flanking distance upstream and downstre
am of the TR event.

4. $flank = The flanking distance upstream and downstream of the TR event. They are assumed to be same.

5. $numCore = Number of processors to be used. The pipeline uses multiprocessing library (which is in standard python) for spawning processes via pool.

6. $runDir = The directory where the output files should be generated. Please note that using multiple processors results in writing of many out files and this can be controlled by modi
fying the value of 'num' variable in the source code. Right now it uses a factor of 1.0/0.05. This parameter is used to partition the dictionary queryAligns, and passes that partition t
o a processor via pool. Higher the number lower the number of partitions created.

B. For running the clustering part of the pipeline, please run:
source runClustering.sh $pathDirInput $pathDirOutput

1. pathDirInput="location of directory where the *out* from previous step is located"

2. pathDirOutput="location of directory where the clustering output files are written"

This script processes the output from the previous step and uses that as an input for the gmmCluster/clusteringGMM_AIC_cSep.py. The input parameters for clustering are specified in the 
script. The script writes the output file by creating a directory 'clusterRun' in the user specified location for clustering output files.

7. Per-Read SV visualization
============================

For visualization, a simple dot plotting tool was constructed
that is designed to tolerate the high indel rate of PacBio raw CLR
sequencing data. In order to generate these dotplots (including various
dimensional projections for identification of SVs) there are two steps.
The first step is to grab from a full assembly.bam file all reads that
traverse an SV of interest. In order to do so, a .bed file of regions of
interest are passed to the first grabRegionReadsAndRefs.py. A file
structure is constructed that can then be traversed by the second 
script which generates plotter slaves (or, alternatively, submit jobs
to a queue).