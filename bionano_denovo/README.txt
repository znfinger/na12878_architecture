This section describes an overview of the BioNano Genomics de novo assembly and structural variation detection pipeline. The core algorithms are the Assembler and RefAligner binaries, and they are wrapped in a set of Python scripts. 
Here are the minimal system requirements:
�	Ubuntu Linux Server v12.04 or CentOs6
�	Intel AVX or AMD Opteron 3.0GHz processors capable of running at least 32 threads
�	128GB DDR3 RDIMM 1600MHzor better, ECC
�	SATA RAID or non-RAID, 3.0Gb/s Hard Drive Controller
�	4 TB SATA 3.0Gb/s, 7200 RPM, 32MB Cache or Distributed File System Hard Drive
Scheduler requirements:
�	Sun grid engine (SGE) 6.2u5
�	Python-DRMAA library installed
�	Grid engine allowing multi-threaded jobs on the cluster with at least 32 threads per job (larger memory host needs more threads)
�	Grid engine configured to support "openmp" physical environment
Hardware requirements:
�	AVX supporting processors (recommended)
�	One host with 256 GB of memory or larger (at least 64 threads) and 4 other hosts with 128 GB of memory (at least 32 threads)

To run the pipeline, please use the following command on a Linux system meeting the requirements specified above (the same command is also shown in the run_assembly.sh file)
python pipeline/pipelineCL.py -d -U -T 240 -N 6 -j 240 -i 5 -l output/ -t tools -C xmls/clusterArguments.xml -b molecules/input.bnx -r hg19/ref.cmap -a xmls/optArguments.xml

Here are the contents of the sub-directories, and they are required as inputs to the assembly and structural variation detection pipeline.
hg19/ contains the a file representing the in-silico digested hg19 public reference
molecules/ contains the input molecules from the NA12878 genome, and these molecules were used to build the BioNano genome map assembly
pipeline/ contains Python pipeline scripts
tools/ contains the binaries such as Assembler and RefAligner
xmls/ contains the parameters used to run the pipeline

The output directories contain multiple sub-directories, and the following are the relevant directories/files containing final results:
contigs/
	exp_refineFinal1/
		EXP_REFINEFINAL1.cmap - the final refined BioNano genome map assembly
		alignref/ - contains the alignment of the genome maps to the hg19 public reference
	exp_refineFinal1_sv/merged_smaps/
		exp_refineFinal1_merged.smap - contains structural variants detected when comparing the genome maps against the public reference

Note that the files here are the input to the BioNano Genomics' pipeline. A copy of the output files can be found in (http://bnxinstall.com/publicdatasets/).