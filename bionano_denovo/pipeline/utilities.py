
#utility functions for pipeline (mostly just in CharacterizeModule)

import os #needed for file utilities
import re
import math
import string
import atexit
import socket

"""@package utilities General methods for analysis

"""


#SVN utilties

global version
version=[]

#store argument in global version
def setVersion(v):
    global version
    version.append(v)

#parse version strings in global version; return ele which corresponds to max version
#this is only called once, in Pipeline.py
def getMaxVersion():
    global version
    maxi = maxv = 0
    for i,vstr in enumerate(version) :
        try :
            ver = int(vstr.split()[2])
        except :
            continue
        if ver > maxv :
            maxv = ver
            maxi = i
    return version[maxi]


setVersion("$Id: utilities.py 2499 2014-02-15 01:08:30Z vdergachev $") #use above for this file


#general utilities

def uniquifyContigName(oname):
    iname = oname.upper()
    if iname != oname:
        return iname
    iname = oname.lower()
    if iname != oname:
        return iname
    return 'tempMerge'

#because the even len case returns a float, do the same for odd len case
def getMedian(inlist):
    insort = sorted(inlist) #copies list
    inlen = len(insort)
    if not inlen%2 : #even number of eles
        return (insort[inlen/2-1] + insort[inlen/2]) / 2.
    return float(insort[inlen/2]) #odd number of eles


#These two moved here from mapClasses.py
def getRMSsquared(inlist):
    if len(inlist) == 0 : #otherwise it's an exception
        return 0
    sumsquared = sum( map(lambda x: x**2, inlist) )
    return sumsquared/len(inlist)

def getRMS(inlist):
    return math.sqrt( getRMSsquared(inlist) )


#n50 algo from here:
#http://code.google.com/p/biopyscripts/source/browse/trunk/seq/N50.py
#Stuglik is apparently the guy's name
def getn50stuglik(inlist) :
    teoN50 = sum(inlist) / 2.0    
    #inlist.sort() #this is in getn50
    inlist.reverse() 
   
    testSum = 0; n50 = 0
    for leni in inlist:
        testSum += leni
        if teoN50 < testSum:
            n50 = leni
            break
    return n50


#Sorts list, calls getn50stuglik (was getn50sorted)
# Input list must be positive ints or floats only
def getn50(inlist) :
    if type(inlist) != list :
        print "Error in getn50: argument must be a list:", type(inlist)
        return 0
    inlist.sort()
    return getn50stuglik( inlist ) #much faster...
    #return getn50sorted( inlist ) #my algo sucks, performance wise


#getn50sorted :
# get the n50 for the input list
#  n50 is the value such that the sum of the elements < that value is equal to the sum >.
# for this, input list must be sorted--use getn50 above for unsorted

#Note on n50 algo for case of floats:
# It is very rare that sumL == sumR for floats. Therefore, for the rare case in which casting to an int would result in sumL == sumR, we'll end up choosing the lastL or lastR instead of the average of the two.
# This is a rare enough case that I'm going to ignore it. It only matters when there's a large difference between neighboring elements near the median of the expanded list.

#misc notes
# generalize to nX? ie, n50 is x = 50--another arg???

def getn50sorted(inlist) :
    if type(inlist) != list :
        print "Error in getn50sorted: argument must be a list:", type(inlist)
        return 0
    if len(inlist) == 0 :
        return 0 #nothing to do
    if len(inlist) == 1 :
        return inlist[0] #only one ele means return it
    printdebug = False
    sumL = inlist[0]
    sumR = inlist[-1]
    lastL = 1 #index on L--_next_ ele to use
    lastR = -1 #index on R (ele already used)--this will always count backwards
    try :
        sumall = sum(inlist) #I think these may save multiple calls to these guys
    except TypeError :
        print "Error in getn50sorted: Bad types in input list", inlist
        return 0
    lenall = len(inlist) #I think this can't fail for any list
    while True :
    #for ele in inlist[1:-1] : #exclude first, last since already added
        #try the outter loop a while true, and the inner a for, break
        #while sumL <= sumR :
        for idx, ele in enumerate(inlist[lastL:lastR]) :
            sumL += ele
            newlastL = idx #don't want to overwrite bc used in for loop
            if printdebug :
                print "sumL =", sumL, "lastL =", newlastL+lastL+1 #debug
            #require > bc makes R easier to handle--also need to update lastL if end of loop
            # len of this loop is lenall - lastL - abs(lastR)
            #  And -abs(lastR) = lastR since lastR < 0, and -1 bc index vs len
            if sumL > sumR or idx == lenall - lastL + lastR - 1 : 
                lastL += newlastL + 1 #n old + n new (need additional +1?)
                break
        #end loop to increase sumL, lastL
        neleused = lastL + abs(lastR) #for L, index of next ele is n ele used on L
        if printdebug :
            print "neleused =", neleused
        #error check--n eles
        if neleused > lenall or neleused < 0 :
            print "Error in getn50sorted: bad indicies", neleused
            return 0
        if neleused == lenall : #no eles left--recall lastR < 0
            if sumL == sumR :
                #avg of last two (recall, need to take away 1 from lastL)
                return (inlist[lastL-1] + inlist[lastR])/2 
            elif sumL > sumR :
                return inlist[lastL-1] #if exceeded, last ele is n50 (lastL is next, so -1)
            #sumL < sumR if middle happens to be low--nothing wrong with this
            elif sumL < sumR :
                return inlist[lastR] #if R exceeded, need lastR
        #more eles left--increase R
        lastR -= 1
        sumR += inlist[lastR]
        if printdebug :
            print "sumR =", sumR, "lastR =", lastR #debug
        #error check--bail if summed too much
        if sumR > sumall or sumL > sumall :
            print "Error in getn50: sum excess"
            return 0
        #error check--bail if sum < 0
        if sumR < 0 or sumL < 0 :
            print "Error in getn50: negative sum"
            return 0

#end def getn50sorted



#Given a list of pairs of ints/floats, return a new list of pairs which have no overlap.
# Pairs here means a list of two elements (if this isn't satisfied, skip it, but print an error).
# No overlap means that each pair specifies a range. So two ranges which overlap should be merged to a single range.
#
#Other notes:
# Allow modification of the input list--this will make implementation easier, and will also consume less memory. Of course, the downside is that after calling this function, you can't assume your list is the same.
#  Therefore, I will remove any items (pairs) which are invalid--see error handling at beginning of first loop
# I'm not doing any checking for negative numbers, so best not to input them
# For implementation notes, see inline comments

def uniqueRange(inlist) :
    printerr = True #this amounts to a verbose flag

    if type(inlist) != list :
        if printerr :
            print "Error in uniqueRange: input must be a list:", inlist
        return #return none is fine--list is merged in place

    #Need to check that all the pairs are sorted, otherwise, can't really trust the results of below--this is a bit time consuming, but I think it's necessary
    #store idxs to delete bc it's very very bad to delete from a list while you're looping over it
    badidxs = [] 
    for idx, ele in enumerate(inlist) :
        if type(ele) != list :
            if printerr :
                print "Warning in uniqueRange: skipping non-list ele:", ele
            badidxs.append( idx )
            continue
        if len(ele) != 2 :
            if printerr :
                print "Warning in uniqueRange: skipping list of improper size:", ele
            badidxs.append( idx )
            continue
        if not isIntorFloat(ele[0]) or not isIntorFloat(ele[1]) :
            if printerr :
                print "Warning in uniqueRange: skipping list ele of improper type:", ele[0], ele[1]
            badidxs.append( idx )
            continue
        #now that we're sure that all the pairs are good, sort them
        ele.sort()

    #delete bad eles--see first 'Other notes' above
    #I think if you reverse it, it's safe bc will change from end to beginning--assumes badidxs is sorted, but it must be because of how it's constructed, right?
    badidxs.reverse() 
    for idx in badidxs : del inlist[idx]

    #need to now sort the sublists within inlist (pretty cool, eh?)
    #and it doesn't matter if the second ele is not sorted
    inlist.sort(key=lambda x: x[0])

    #print inlist #start with this (sorted)--debug

    #now merge overlapping entries
    badidxs = [] #clear this list--this should be fine, I think
    for idx, ele in enumerate(inlist[:-1]) : #leave off last ele
        nextele = inlist[idx+1]
        #if next element starts before (or at) current one ends, merge the pairs
        #careful--need to cast to floats, otherwise comp won't work
        if float(nextele[0]) <= float(ele[1]) : 
            #print "merging", ele, "and", nextele, #debug/dev
            nextele[1] = max(float(nextele[1]), float(ele[1])) #next ends as far as it can
            #need to check if next ele starts after current--if so, it needs to start at current start
            if float(nextele[0]) >= float(ele[0]) :
                nextele[0] = ele[0]
            badidxs.append( idx ) #discard current in favor of next
            #print "to", nextele #debug/dev (new is next)

    #now remove the elements which were merged
    badidxs.reverse() 
    for idx in badidxs : del inlist[idx]

    #print inlist #this is here for development purposes--remove later
    #the input list was modified in place--no return

#end def uniqueRange(inlist)



#simple helper fn for above (not so simple...see below)
def isIntorFloat(ele) :
    #they say that checking type is a sign of bad polymorphism, but I never envision using that here
    #the new version doesn't check type, but it still has flaws: 12L fails both, but is a 'number'
    # I don't know how to do better...perhaps a cast?
    # for the case of "L" numbers, this works:
    #  import numbers
    #  isinstance(12L, numbers.Integral)
    return isinstance(ele, int) or isinstance(ele, float)
    #return type(ele) == int or type(ele) == float #old way--replace with above



#leave above, but make new version (int only, not float):
# isinstance is polymorphism safe and avoids these problems:
#  try: return ele == int(ele) #this doesn't work bc '1. == 1'; returns True
#  try: return newele = int(ele) #this doesn't throw an exception for ele is float; also returns True
def isInt( ele ) :
    return isinstance( ele, int )



#String Utilities


#check if object (string) has substring
def hasSubStr(instring, substr) :
    try :
        #find returns pos in string if found
        # ie, if found, get 1, which is not -1
        # if not found, get -1, and (-1 != -1) is False
        return ( instring.find(substr) != -1 )
    except :
        return False


#check if string represents an int using cast in try
# return the int if so; return None if not
# note that empty string will return None, not 0 (because it's not a valid int)
# also note that strings for floats will also return None because of how casting works in python
#  ie, int("1.0") raises ValueError
#  Do not put a "." in instring
def getIntFromString(instring) :
    try :
        inint = int(instring)
    except ValueError : #only catch invalid cast, which is a ValueError
        return None

    return inint #no exception
#end def getIntFromString


#get index of a file whose format is:
# 'path/filenameX.suff'
#  Note that 'path' is irrelevant--just the end of the string is used, beginning is ignored
#  and X (the index) is an integer up to (and including) 4 digits
#   If you want more than 4 digits, change the 'ndigits' local
# return the index as an int if valid, and return None if not
# depends on getIntFromString, which should return None for non-integers
# Note that multiple "." will use last, ignoring others
def getPathIndex(instring) :
    ndigits = 4 #if you want more digits, increase this

    #first, strip suff--if it doesn't exist, do nothing
    #usestr = os.path.split(instring)[1] #unnecessary
    dpos = instring.rfind(".")
    if dpos != -1 : #has a period
        usestr = instring[:dpos] #slice does not include second arg
    else :
        usestr = instring #copy so that above does not modify input (not sure if strings are passed by ref or value)

    #then, try each of 3, 2, 1 digits
    for idx in range(1,ndigits+1)[::-1] : #if ndigits is 3, do 3,2,1 (the -1 of slice goes backwards)
        useint = getIntFromString(usestr[-idx:])
        if useint != None : #this means getIntFromString succeeded
            break #since go from largest to smallest, break at first non-None

    #if getIntFromString never found an int, useint is None, but return it anyway
    return useint
#def getPathIndex(instring) :



#File Utilities



#check a file--filepath is full path, not just prefix
# if filesuff, check that filepath ends with this string
# if checkReadable, require user has read access
def checkFile(filepath, filesuff="", checkReadable=True) :
    try :
        valid = os.path.isfile(filepath)
        if filesuff != "" :
            valid = (valid and filepath.endswith(filesuff))
        if checkReadable :
            valid = (valid and os.access(filepath, os.R_OK))
    except :
        valid = False
    #print "checkFile:", filepath, valid #debug
    return valid
#end def checkFile



#check a cmap--use checkFile with filesuff == ".cmap"
#returns true if arg is a file that exists and ends in ".cmap"
def checkCmap(cmappath) :
    return checkFile(cmappath, ".cmap")
#end def checkCmap


#true if arg is file with suffix "cmap" OR "spots"
def checkCmapSpots(inpath) :
    return checkFile(inpath, ".cmap") or checkFile(inpath, ".spots")
#end def checkCmapSpots


#
# Convert string to C like representation
# to avoid printing control characters
#
def normalizeString(s):
	out=""
	for ch in s:
		if ch=="\"":
			out+="\\\""
			continue
		if ch=="\\":
			out+="\\\\"
			continue
		if ch=="\n":
			out+="\\n"
			continue
		if ch=="\r":
			out+="\\r"
			continue
		if ch=="\t":
			out+="\\t"
			continue
		if string.find(string.printable, ch)>=0:
			out+=ch
			continue
		out+="\\%03o" % tuple([ord(ch)])
	return out

def checkStdOut(filename, end_pattern="END of output\n"):
	try:
		f=open(filename, "rb")
		# seek relative to end of file
		f.seek(-len(end_pattern), 2)
		line=f.read()
		if line!=end_pattern:
			print("Missing end marker in \"%s\" (found \"%s\" while expecting \"%s\")\n" % (normalizeString(filename), normalizeString(line), normalizeString(end_pattern)))
			return 0
		f.close()
		return 1
	except:
		return 0


def InitStatus(filename):
	global status_log
	global status_log_filename
	status_log_filename=filename
	status_log=open(filename, "wb")
	
	LogStatus("pipeline", "pid", "%d" % os.getpid())
	LogStatus("pipeline", "hostname", socket.gethostname())
	LogStatus("pipeline", "status_file", filename)
	LogStatus("pipeline", "user_home", os.path.expanduser('~'))
	try:
		LogStatus("pipeline", "uid", os.getuid())
	except:
		pass

	
@atexit.register
def on_exit():
	try:
		LogStatus("pipeline", "exit", "%d" % os.getpid())
	except:
		pass
		
def LogStatus(category, field, value):
	global status_log
	status_log.write("<%s attr=\"%s\" val0=\"%s\"/>\n" % (category, field, value))
	status_log.flush()
		
global ErrorLog
ErrorLog=[]

def LogError(category, message):
	global ErrorLog
	ErrorLog.append((normalizeString(category), normalizeString(message)))

	if category=="warning":
		LogStatus("warning", "message", normalizeString(message))
	else:
		LogStatus("error", normalizeString(category), normalizeString(message))
		
def SummarizeErrors(varsP=None):
	global ErrorLog
	if len(ErrorLog)==0:
		print "No errors detected\n"
		if varsP!=None:
			varsP.updatePipeReport("No errors detected\n", printalso=False)
		return 0
		
	categories={}
	error_count=0
	for i in range(0, len(ErrorLog)):
            errstr = "%s : %s\n" % ErrorLog[i]
            print(errstr)
            if varsP!=None:
		varsP.updatePipeReport(errstr, printalso=False)                
            cat=ErrorLog[i][0]
            if not cat=="warning":
		    error_count+=1
            try:
                categories[cat]+=1
            except:
                categories[cat]=1
	print("\nError summary:\n")
	for cat in categories:
		print("\t%d %s error(s)\n" % (categories[cat], cat))

	if varsP!=None:
		varsP.updatePipeReport("\nError summary:\n", printalso=False)
		for cat in categories:
			varsP.updatePipeReport("\t%d %s error(s)\n" % (categories[cat], cat), printalso=False)
	return error_count
		
		
#return a list of full paths to cmaps in a dir (I have to do this all the time, may as well make a fn)
def getListOfCmapsFromDir(indir) :
    if not checkDir(indir, checkWritable=False, makeIfNotExist=False) :
        print "Error in getListOfCmapsFromDir: argument is not a dir:", indir
        return
    outlist = []
    for qfile in os.listdir(indir) :
        if not checkCmap( os.path.join(indir, qfile) ) :
            continue
        outlist.append( os.path.join(indir, qfile) )
    return outlist
#end def getListOfCmapsFromDir


#check if a file exists (checkFile) and is executable
def checkExecutable(filepath, filesuff=""):
    if not checkFile(filepath, filesuff):
        return False
    return os.access(filepath, os.X_OK)
#end def checkExecutable


#check if directory exists; make it if not
#optionally check writability--True by default
#make creation an option--True by default
#should probably make a vebose option
def checkDir(dirpath, checkWritable=True, makeIfNotExist=True) :

    #if it exists and it's not a directory, return False
    if os.path.exists(dirpath) and not os.path.isdir(dirpath) : 
        return False

    #this check was isdir, but actually, it should be existence--if it doesn't exist, make it
    elif not os.path.exists(dirpath) : #os.path.isdir(dirpath) : 
        if makeIfNotExist :
            try :
                os.makedirs(dirpath)
            except : 
                return False
            if not os.path.isdir(dirpath) : #too bad to handle
                #print "Error in utilities.checkDir: cannot create dir:", dirpath #silent
                return False
        else : #doesn't exist, and flag to make is false--return False
            return False

    #above is check of existence. Now check writable 
    if checkWritable and not os.access(dirpath, os.W_OK) : # W_OK is for writing, R_OK for reading, etc.
        return False

    return True #all checking passed
#def checkDir


#get all sub-directories (not files) in input dir
#returns empty list if no sub-dirs
def getSubDirs(indir) :
    if not checkDir(indir, makeIfNotExist=False) :
        print "Error in getSubDirs: input is not a dir"
        return None

    targetlist = []
    for qfile in os.listdir(indir) :
        targetdir = os.path.join(indir, qfile)
        if os.path.isdir(targetdir) :
            targetlist.append( targetdir )

    return targetlist
#end def getSubDirs(indir)


#strip the first dir and filename from a path
# ie, given 'first_dir/second_dir/.../n_th_dir/filename.suf'
#  you get 'second_dir/.../nth_dir'
# if no filename, get same
def stripFirstDir( inpath ) :

    #again, the 'isinstance' vs 'try' question
    if not isinstance(inpath, str) :
        print "Error in stripFirstSubDir: input not string:", inpath
        return None

    folders = getPathList( inpath ) 

    #only strip the last ele if it's a file
    #But I don't want to check file existence, just in case this is not a real path
    #so just discard if it has '.'
    if '.' in folders[-1] : #'in' operator for strings works just like it does for lists
        del folders[-1]

    #ok, now strip the first ele in the loop statement (always)
    outstr = ""
    for ele in folders[1:] : 
        outstr = os.path.join(outstr, ele)

    #print outstr #debug
    return outstr
    
#end def stripFirstSubDir( inpath ) :


#given a path, return a list of its elements
# ie, given 'first_dir/second_dir/.../n_th_dir/filename.suf',
#  you get ['first_dir', 'second_dir', ..., 'n_th_dir', 'filename.suf']
#from http://stackoverflow.com/questions/3167154/
# but slightly modified to handle case of trailing '/'
def getPathList( inpath ) :

    #again, the 'isinstance' vs 'try' question
    if not isinstance(inpath, str) :
        print "Error in stripFirstSubDir: input not string:", inpath
        return None
        
    folders=[]
    path = inpath #copy (?)
    while True :
        path, filename = os.path.split(path)

        if filename != "" :
            folders.append(filename)
        else:
            #only append if this is the last thing to append (ie, not first ending in /)
            if path != "" and os.path.split(path)[0] == '' : 
                folders.append(path)
            if os.path.split(path)[0] == '' :
                break
        #print path, filename, folders #debug

    folders.reverse()
    #print folders #debug
    return folders

#end def getPathList()


#get the number of files in a directory with a given suffix (ie, filename ends with substr)
def getNfilesDir(indir, suffix="") :
    #do not make dir, but check existence--leave checkWritable True
    assert checkDir(indir, makeIfNotExist=False) 

    nfile = 0
    #note on listdir: you get everything you get with "-a" *except* "." and ".."
    # this means you do get ".xyz" files and "blah.suf~" files
    # And that is what you get if suffix is not supplied, because endswith("") is apparently always True
    for filename in os.listdir(indir) :
        if filename.endswith(suffix) :
            #print filename #debug
            nfile += 1

    return nfile
#end def getNfilesDir



#if a dir exists, _remove_ all its contents. Be careful.
#dependency: shutil (this is python std lib, so should be fine)
def removeDirContents(indir) :

    #check if it exists, return if not--do not create
    if not checkDir(indir, makeIfNotExist=False) :
        return None

    import shutil

    for files in os.listdir(indir): 
        shutil.rmtree( os.path.join(indir, files) ) #don't call on indir bc only want to remove its contents

#end removeDirContents


#get number of newlines in file
def wc(infile) :
    if not checkFile(infile) :
        print "Error in wc: bad file:", infile
        return
    nlines = 0
    for line in open(infile) :
        nlines += 1
    return nlines
#end wc



#list/dict utilities



#Get the total length which corresponds to the sum of all the ranges in a list of ranges
#**No checks for any failures are done.**
# Garbage in, garbage out.
#Note: requires math
def totalLengthFromRanges(inlist) :
    printerr = True #this amounts to a verbose flag

    totlen = 0
    if type(inlist) != list : #this should probably be a try...
        if printerr :
            print "Error in totalLengthFromRanges: input must be a list:", inlist
        return 0 #return int/float

    for ele in inlist :
        elelen = 0
        try :
            elelen = math.fabs(ele[1] - ele[0])
        except :
            pass
        #print "adding", elelen #debug
        totlen += elelen

    return totlen
#end totalLengthFromRanges



#wrap the fn below to print
# assume keys can be casted to strs and values are ints
#Note: sorting ints by their string values results in errors like 10 before 2.
# but chromosomes are mixed ints and strings...
def printSortedDict(adict) :
    keys, values = sortedDict(adict)
    for i in range(len(keys)) :
        print "%3s  %3i" % (keys[i], values[i])



#return tuple of keys, values such that keys are sorted
#from http://code.activestate.com/recipes/52306-to-sort-a-dictionary/, slightly modified
#note that there is an OrderedDict class in the built-in collections module
def sortedDict(adict):
    keys = adict.keys()
    keys.sort()
    #map returns a list, applying the first arg (a fn) to the list of second arg
    #dict.get is same as [], but this is apparently faster than doing [adict[key] for key in keys]
    return keys, map(adict.get, keys)


#dict.update will clobber existing entries if the argument contains the same keys
#first check for the keys, and if found, raise an exception
# if you want a bool, wrap this in try
#if no exception, do the update (dicts are passed by ref (sort of), so modify existing)
def safeDictUpdate(indict, updict) :
    if any([ indict.has_key(upkey) for upkey in updict.keys() ]) :
        #print indict, updict #debug
        #KeyError is raised if key doesn't exist, and here's it's raised if it does
        raise KeyError, "key in update dict already in original dict" 

    #I think else isn't necessary becuase if raise, exit this fn, but it seems safer this way
    else : 
        indict.update( updict )


#take two dicts; add all values in second (adict) to first (sumdict) when keys match
#In order to prevent raising a KeyError exception, check key
# if addkeys, then put the key in sumdict
# if not, ignore any values whose keys are not in sumdict
#Also, no return because after calling, the sumdict will be changed (like a pass by reference)
def addDictToDict(sumdict, adict, addkeys=True) :
    if not adict : #do nothing for empty/None adict
        return
    for key,value in adict.iteritems() :
        if sumdict.has_key(key) :
            sumdict[key] += value
        elif addkeys :
            sumdict[key] = value

def contigWeight(fname):
	f=open(fname, "rb")
	f.seek(-512, 2)
	ctg=f.read().split("\n")
	f.close()
	
	i=len(ctg)
	while i>=1:
		i-=1
		if ctg[i]!="":
			break
	line=ctg[i].split()
	
	length1=float(line[1])
	nsites=float(line[2])
	density=nsites*1.0/length1
	if density< (1.0/10e3):
		density=1.0/10e3
	return density*density*nsites

def contigComputeWeights(file_list):
	L=[]
	sizesum=0
	for fname in file_list:
		#a=os.stat(fname).st_size
		a=contigWeight(fname)
		sizesum+=a;
		L.append(a)
	return sizesum, L



#bnxfile and molecule classes

class bnxfile:

    def __init__(self, sourcefile='', thresh=None): #sourcefile is .bnx
        #init data members
        self.moleculelist    = [] #list of molecule objects
        self.moleculeIdlist  = []
        self.moleculeLenlist = []
        self.molstats        = {}
        self.genomesizemb    = -1. #guess genome size from sourcefile
        self.header          = ""
        self.lengthThresholds = (thresh if thresh else [100, 150, 180, 200])
        if checkFile(sourcefile, ".bnx") : #must end in ".bnx" -- if migrate this out of utilities, need util.checkFile
            self.makeFromFile(sourcefile)
            self.calculateMolStats()
        elif sourcefile : #if argument supplied but the file is not found, print a warning
            print "Warning in bnxfile init: sourcefile not found:", sourcefile

    def makeFromFile(self, sourcefile) :
        self.moleculelist = []
        #try to guess genome size based on sourcefile
        if hasSubStr(sourcefile.lower(), "human") :
            self.genomesizemb = 3200
        elif hasSubStr(sourcefile.lower(), "tribolium") :
            self.genomesizemb = 200
        elif hasSubStr(sourcefile.lower(), "chicken") :
            self.genomesizemb = 1000 #rounding down from 1064 total in the fasta
        with open(sourcefile) as f1 :
            mollines = ""
            for line in f1 :
                if line[0] == "#" : #header lines
                    self.header += line
                    continue

                elif line[0] == "0" : #0h Label Channel	MapID	Length
                    if mollines : #at first molecule, this is empty
                        self.moleculelist.append( molecule(mollines) ) #store previous molecule
                    mollines = line #replace with current line

                else : #for any other line type, append to mollines
                    mollines += line

                #if len(self.moleculelist) > 3 : #test
                #    break
            #end for line in f1
            self.moleculelist.append( molecule(mollines) ) #need last molecule
        #end with open sourcefile

    #end makeFromFile


    #make a list of just the molecule ids from the molecule list
    def makeMolIdList(self) :
        if not self.moleculelist :
            self.moleculeIdlist = []
            return
        self.moleculeIdlist = [x.id for x in self.moleculelist]
    #end makeMolIdList(self) 


    #make a list of just the molecule ids from the molecule list
    def makeMolLenList(self) :
        if not self.moleculelist :
            self.moleculeLenlist = []
            return
        self.moleculeLenlist = [x.lenkb for x in self.moleculelist]
    #end makeMolLenList(self) 


    #inits data members for statistics of molecule data
    #for length thresholds specified in thresh local
    #use dictionary whose keys are the thresholds and values are the bnxstats class
    def calculateMolStats(self) :
        self.molstats = {}

        #these are no longer used
        #if not self.moleculeIdlist :
        #    self.makeMolIdList()
        #if not self.moleculeLenlist :
        #    self.makeMolLenList()

        for thresh in self.lengthThresholds :
            #default for printing len is Mb, change here for other (see bnxstats.__str__)
            stat = bnxstats(genomesizemb=self.genomesizemb) 
            #stat.makeFromLenList( [x for x in self.moleculeLenlist if x > thresh] ) #moleculeLenlist is assumed to be in kb
            stat.makeFromMolList( [x for x in self.moleculelist if x.lenkb >= thresh] )
            self.molstats[thresh] = stat
    #end calculateMolStats
    

    #print results of calculateMolStats
    def printMolStats(self) :
        if not self.molstats :
            self.calculateMolStats()
        #for thresh, stats in self.molstats.iteritems() : #not sorted
        for thresh in sorted(self.molstats.keys()) : #loop over sorted keys
            print "Len >= %i (kb)" % thresh
            print self.molstats[thresh] #stats
    #end printMolStats


    #check for duplicate molecule: if all lab poses are the same, it's a duplicate; print summary
    #warning: this is very slow for large bnxs
    def checkDuplicateMolecule(self) :
        ndupe = 0
        for idx1,mol1 in enumerate(self.moleculelist) :
            for mol2 in self.moleculelist[idx1+1:] : #first arg of slice is included, but don't want it
                if mol1.labposeskb == mol2.labposeskb and mol1.labposeskb == mol2.labposeskb :
                    ndupe += 1
                    print "Duplicate molecules:", mol1.id, mol2.id
        #end outer mol loop
        if not ndupe :
            print "No duplicates found"
    #end def checkDuplicateMolecule


    #compare molecule IDs in self to those in argument, print any matches
    def compareMoleculeIds(self, inbnx) :
        ndupe = 0
        inidlist = [x.id for x in inbnx.moleculelist]
        for mol in self.moleculelist :
            if mol.id in inidlist :
                ndupe += 1
                print "Molecule in both bnx:", mold.id
        if not ndupe :
            print "No duplicates found"
        else :
            print "N duplicates:", ndupe
    #end compareMoleculeIds

    #I'm currently only supporting bnx version 0.1
    #which means the header must reflect this--fix the header
    def setHeaderVersion(self, newversion="0.1"):
        verstr = "BNX File Version"
        headlines = self.header.split("\n")
        headlen = len(headlines)
        #to do this properly, you should loop over every line
        #for line in headlines :
        #    if line.find(verstr) != -1 :
        #to do it easily, assume the first line
        topline = headlines.pop(0) #removes 0th element
        if topline.find(verstr) == -1 : #if this isn't in first line, do nothing
            return
        topline = "# "+verstr+":\t0.1"
        headlines[0:0] = [topline] #insert new topline back into header
        self.header = "\n".join(headlines)
        #assert headlen == len(self.header.split("\n")) #check new vs orig len
    #end setHeaderVersion

    def writeToFile(self, filename) :
        self.setHeaderVersion() #this is only necessary for reading a version 1 bnx
        if not filename.endswith(".bnx"):
            print "Error in bnxfile.writeToFile: file to write must end in .bnx"
            return
        outfile = open(filename, "w+")
        outfile.write(self.header)
        for mol in self.moleculelist :
            outfile.write(mol.printBnx())
        outfile.close()
    #end writeToFile

#end class bnxfile


#class to store stats for bnx files

class bnxstats :

    #see __str__ for explanation of unitscale
    def __init__(self, nmol=0, totlen=0, n50=0, genomesizemb=-1, unitscale=1e3, totlab=0) :
        self.nmol         = nmol
        self.totlen       = totlen
        self.n50          = n50
        self.unitscale    = unitscale
        self.genomesizemb = genomesizemb
        self.totnlab      = totlab
        self.labdensity   = (totlab*1e2/totlen if totlen else 0) #in units of /100kb, assuming totlen is in units of kb
        self.coverage     = (self.totlen/1e3/self.genomesizemb if self.genomesizemb > 0 else 0) #totlen is in kb

    #inlist is list of mol lens _in kb_
    #because this is length only, no nlab or labdensity
    def makeFromLenList(self, inlist) :
        self.nmol   = len(inlist)
        self.totlen = sum(inlist)
        self.n50    = getn50(inlist)
        self.coverage = (self.totlen/1e3/self.genomesizemb if self.genomesizemb > 0 else 0) #totlen is in kb

    #inlist is list of molecule objects
    # redundant with makeFromLenList except for totnlab, which you can't get from len alone
    def makeFromMolList(self, inlist) :
        lenlist = [x.lenkb for x in inlist]
        self.nmol       = len(lenlist)
        self.totlen     = sum(lenlist) #assume kb
        self.n50        = getn50(lenlist)
        self.totnlab    = sum( [len(x.labposeskb) for x in inlist] )
        self.labdensity = (self.totnlab*1e2/self.totlen if self.totlen else 0) #in units of /100kb
        self.coverage   = (self.totlen/1e3/self.genomesizemb if self.genomesizemb > 0 else 0) #totlen is in kb

    def __str__(self) :
        ustr = "kb" #default is kb, so unitscale is relative to this
        if self.unitscale == 1e3 : #ie, 1e3 kb is Mb
            ustr = "Mb"
        out =  "n mols: %i\n" % self.nmol
        uselen = self.totlen/(self.unitscale if self.unitscale==1e3 else 1)
        #usen50 = self.n50   /(self.unitscale if self.unitscale==1e3 else 1) #keep n50 in kb
        out += ("total len ("+ustr+"): %10.3f\n") % uselen
        out += ("mol n50 ("+"kb"+")  : %10.3f\n") % self.n50 #usen50
        if self.labdensity > 0 : 
            out += "lab (/100kb)  : %10.3f \n" % self.labdensity
        if self.genomesizemb > 0 :
            #out += "coverage (x)  : %10.3f\n" % (self.totlen/1e3/self.genomesizemb) #totlen is in kb
            out += "coverage (x)  : %10.3f\n" % self.coverage
        #print "max, min len (kb):", max(lenlist), min(lenlist) #forget this
        return out

#end class bnxstats


#molecule class is for entries in a bnx file

class molecule:
    
    def __init__(self, linein=''): #linein is all lines from bnx which make molecule
        if linein :
            self.makeFromLine(linein)
        else : #init data members
            self.id    = 0
            self.lenkb = 0.
            self.labposeskb = []


    #the argument is the four (or two, or more) lines in a bnx which contain the molecule data
    def makeFromLine(self, linein) :
        for line in linein.split("\n") :
            if not line :
                continue
            #0 is just the id and the length
            if line[0] == "0" :
                items = line.split()
                #try here for cast?
                self.id    = int(items[1])
                self.lenkb = float(items[2])/1e3
            #1 is the label positions
            elif line[0] == "1" :
                poses = line.split()[1:] #first is just '1', discard
                self.labposeskb = [float(x)/1e3 for x in poses]


    #lab per 100kb is number of labels over length in 100kb
    def getLabPer100kb(self) :
        return len(self.labposeskb)*100/self.lenkb

    #scale length and all label positions by argument
    # ie, for length adjustment, give bpp_in/500 as argument
    def stretchAll(self, stretch) :
        self.lenkb *= stretch
        self.labposeskb = [x*stretch for x in self.labposeskb]

    #return bnx format 0.1 string: just id, length, and positions in bp, with trailing newline
    def printBnx(self) :
        retstr =  "0\t%i\t%.1f\n" % (self.id, self.lenkb*1e3)
        retstr += "1\t"+"\t".join(["%.1f"%(x*1e3) for x in self.labposeskb])+"\n"
        return retstr

    #str method is for quick printing, not bnx format
    def __str__(self) :
        strposes = ["%.3f"%x for x in self.labposeskb]
        return "%7i  %.3f" % (self.id, self.lenkb) + "\n" + " ".join(strposes)

#end class molecule

# See http://code.activestate.com/recipes/410692/
#
# C or Tcl-like switch statement to compensate for python shortcomings.
#
# This class provides the functionality we want. You only need to look at
# this if you want to know how this works. It only needs to be defined
# once, no need to muck around with its internals.
class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    
    def match(self, *args, **kwargs):
	regexp=kwargs.pop("regexp", False)
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True            
        elif not regexp and self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        elif regexp : # changed for v1.5, see below
            self.fall = re.match(args[0], self.value)!=None
            return self.fall
        else:
            return False

# Replace (if present) sequence of list values
# Currently only supports key-value pair
def argumentReplaceList(origList, updatedVals):
    if updatedVals.__len__() != 2:
        return origList
    targetVal = updatedVals[0]
    try:
        i = origList.index(targetVal)
    except:
        return origList + updatedVals

    newList = list(origList) #copy of origList
    for updatedVal in updatedVals:
        if i >= len(newList):
            newList.append(updatedVal)
        else:
            newList[i] = updatedVal
        i += 1
    return newList


