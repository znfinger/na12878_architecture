#import subprocess
#import time
import os
#import shutil
#import pdb

import Multithreading as mthread

"""@package RefinementModule Defines jobs for refinement, extension and merging 
operations


"""


import utilities
utilities.setVersion("$Id: RefinementModule.py 2253 2013-12-09 22:33:14Z vdergachev $")


#this class replaces the old RefineA and RefineB classes by merging them
#see comment above init
class Refine(mthread.jobWrapper):
    """refineStage is a string specifying what stage of refinement you're in
    must be 'refineA', 'refineB', 'refineNGS', or 'refineFinal' (later)
    """
    def __init__(self, refineStage, varsP):
        validstages = ['refineA', 'refineB', 'refineNGS', 'refineFinal']
        if not refineStage in validstages :
            varsP.error += 1
            varsP.message += '  Error: Refine stage name invalid: '+str(refineStage)+'\n'
            return
        self.refineStage = refineStage
        self.varsP = varsP
        #super is more pythonic than referring to the base class explicitly (only matters for multiple inheritance)
        super(Refine, self).__init__(varsP, refineStage, clusterArgs=varsP.getClusterArgs(refineStage))
        intermediateContigPrefix = self.varsP.expID + self.refineStage.replace("refine", "_r")
        self.varsP.prepareContigIO(intermediateContigPrefix)
        #modify results of varsP.prepareContigIO for special case of refineNGS
        if self.refineStage == 'refineNGS' :
            self.varsP.inputContigPrefix = self.varsP.ngsContigPrefix
            self.varsP.inputContigFolder = self.varsP.ngsInDir
        self.generateJobList()
        
    def runJobs(self):
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)
        
    def writeIDFile(self, nJobs):
        f1 = open(self.varsP.idFile, 'w')
        f1.write(str(nJobs))
        f1.close()

    def generateJobList(self):
        baseArgs1 = self.varsP.argsListed(self.refineStage)
        if self.refineStage != 'refineNGS' : #noise args are in refineNGS
            baseArgs1 += self.varsP.argsListed('noise0')
        contigFiles, contigIDs = self.varsP.findContigs(self.varsP.inputContigFolder, self.varsP.inputContigPrefix)
        #nJobs = len(contigFiles)
        if self.refineStage == 'refineA' : #refineA uses assembler, all others use refaligner
            r1args = [self.varsP.AssemblerBin, '-i', self.varsP.bnxFile] #need this before -contigs
            r1args += ['-contigs', os.path.join(self.varsP.inputContigFolder, self.varsP.inputContigPrefix) + '.contigs']
        else : #should be same for refineB/NGS/Final
            r1args = [self.varsP.RefAlignerBin, '-i', self.varsP.bnxFile]
            self.writeIDFile(len(contigFiles)) #nJobs) 
        output1String = os.path.join(self.varsP.outputContigFolder, self.varsP.outputContigPrefix)
        for contigID in contigIDs :
            expectedOutputString = self.varsP.outputContigPrefix + '_contig' + contigID
            expectedResultFile = os.path.join(self.varsP.outputContigFolder, expectedOutputString + '.cmap') #refineB
            jobName = self.refineStage + ' %5s' % contigID
            if self.refineStage == 'refineA' : 
                currentArgs = 2*[str(contigID)] #this must come after r1args because it's actually an argument to -contigs
            else : #should be same for refineB/NGS/Final
                r1_cmapFile = self.varsP.inputContigPrefix + '_contig' + str(contigID) + '.cmap'
                r1_cmapFile = os.path.join(self.varsP.inputContigFolder, r1_cmapFile)
                currentArgs = ['-maxthreads', str(self.varsP.maxthreads), '-ref', r1_cmapFile, '-id', contigID]
            currentArgs = r1args + currentArgs + baseArgs1 + ['-o', output1String]
            if self.varsP.stdoutlog :
                currentArgs.extend( ['-stdout', '-stderr'] )
            s1Job = mthread.singleJob(currentArgs, 
                                    jobName, 
                                    expectedResultFile, 
                                    expectedOutputString,
                                    maxThreads=self.varsP.maxthreads,
                                    clusterLogDir=self.varsP.clusterLogDir,
                                    )
            self.addJob(s1Job)
        self.logArguments()
            
    def checkResults(self):
        ''' #old logic below--clean up using new jobWrapper method
        contigs = []
        for sJob in self.jobList:
            sJob.CheckIfFileFound()
            contigFile = sJob.expectedResultFile
            if not(sJob.resultFound):
                self.warning += 1 #in multi.jobWrapper
                self.messages += '  '+self.refineStage+' Warning Missing Expected File: %s\n' % contigFile
            else:
                contigs.append(contigFile)
        if contigs.__len__() == 0:
            self.error += 1 #in multi.jobWrapper
            self.messages += '  Error: '+self.refineStage+' Missing All Contig Files\n' 
        self.varsP.error += self.error
        self.varsP.warning += self.warning
        self.varsP.message += self.messages
        self.varsP.stageComplete = self.refineStage
        self.varsP.mergeIntoSingleCmap()
        self.pipeReport += self.makeRunReport()
        self.pipeReport += self.makeParseReport()
        self.varsP.updatePipeReport(self.pipeReport)
        '''
        self.varsP.stageComplete = self.refineStage
        self.varsP.mergeIntoSingleCmap()
        self.doAllPipeReport() #see Multithreading.jobWrapper



class Extension(mthread.jobWrapper):
    """Generate jobs for extension phase of assembly
    """
    def __init__(self, varsP):
        self.varsP = varsP
        stageName = '  Extension'
        mthread.jobWrapper.__init__(self,varsP, stageName,clusterArgs=varsP.getClusterArgs('extension'))
        self.varsP.extensionCount += 1
        extContigPrefix = self.varsP.expID + '_ext%s' % self.varsP.extensionCount
        varsP.prepareContigIO(extContigPrefix)
        self.generateJobList()
        
    def runJobs(self):
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)
        
    def generateJobList(self):
        contigFiles, contigIDs = self.varsP.findContigs(self.varsP.inputContigFolder, self.varsP.inputContigPrefix)
        curargs = [self.varsP.RefAlignerBin, '-i', self.varsP.bnxFile]
        baseArgs = self.varsP.argsListed('noise0') + self.varsP.argsListed('extension')
        nJobs = contigFiles.__len__()    
        ct = 0
        logArguments = "" #just in case the following loop isn't entered
        for jobNum in range(1,nJobs + 1):
            contigID = contigIDs[jobNum - 1]
            #jobName = 'Extend ' + contigID + ', Job ' + str(jobNum) + ' of ' + str(nJobs)
            expContigString = self.varsP.outputContigPrefix + '_contig' + contigID
            outputString = os.path.join(self.varsP.outputContigFolder, self.varsP.outputContigPrefix)
            expectedResultFile = os.path.join(self.varsP.outputContigFolder, expContigString + '.cmap')# '_refined.cmap')
            jobName = 'Ext %s' % expContigString# + ', Job ' + str(jobNum) + ' of ' + str(nJobs)
            currentContig = contigFiles[jobNum - 1]
            currentArgs = curargs + baseArgs 
            currentArgs += ['-maxthreads', str(self.varsP.maxthreads), '-o', outputString, '-id', contigID, '-ref', currentContig]
            if self.varsP.stdoutlog :
                currentArgs.extend( ['-stdout', '-stderr'] )
            #if ct == 0: #this does nothing--replaced with self.logArguments below
            #    logArguments = " ".join(currentArgs) + 2 * '\n'
            s1Job = mthread.singleJob(currentArgs, 
                                        jobName, 
                                        expectedResultFile, 
                                        expContigString,
                                        maxThreads=self.varsP.maxthreads, 
                                        forceForward = currentContig, 
                                        clusterLogDir=self.varsP.clusterLogDir,
                                        )#, shell=True)
            self.addJob(s1Job)
            ct += 1
        self.logArguments()
    
    def checkResults(self):
        ''' #old logic below--clean up using new jobWrapper method
        contigs = []
        for sJob in self.jobList:
            sJob.CheckIfFileFound()
            contigFile = sJob.expectedResultFile
            if not(sJob.resultFound):
                self.warning += 1
                self.messages += '  Ext % 2d Warning Missing Expected File: %s\n' % (self.varsP.extensionCount, contigFile)
            else:
                contigs.append(contigFile)
        if contigs.__len__() == 0:
            self.error += 1
            self.messages += '  Error: Ext % 2d  Missing All Contig Files\n' % self.varsP.extensionCount
        self.varsP.error += self.error
        self.varsP.warning += self.warning
        self.varsP.message += self.messages
        self.varsP.stageComplete = 'Extension% 2d' % self.varsP.extensionCount
        self.varsP.mergeIntoSingleCmap()
        self.pipeReport += self.makeRunReport()
        self.pipeReport += self.makeParseReport()
        self.varsP.updatePipeReport(self.pipeReport)
        '''
        self.varsP.stageComplete = 'Extension% 2d' % self.varsP.extensionCount
        self.varsP.mergeIntoSingleCmap()
        self.doAllPipeReport() #see Multithreading.jobWrapper


        
class Merge(mthread.jobWrapper):
    """Generate jobs for merge step. 
    
    Contigs can only merge with one other contig at a time.
    External function will call execution of this class iteratively to exhaust
    all merging possibilities
    """
    def __init__(self, varsP):
        self.varsP = varsP
        stageName = '  Merge'
        mthread.jobWrapper.__init__(self,varsP, stageName,clusterArgs=varsP.getClusterArgs('merge'))
        self.alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        self.iterCount = 0
        contigTag = '_mrg%d' % self.varsP.extensionCount
        initialPrefix = self.varsP.expID + contigTag
        varsP.prepareContigIO(initialPrefix)
        self.prefixIter = iter([self.varsP.expID + contigTag + x for x in self.alphabet])
        self.prevPrefix = None
        self.curPrefix = None
                
    def runJobs(self):
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)
    
    def countContigs(self,targetFolder,prefix): 
        contigFiles, contigIDs = self.varsP.findContigs(targetFolder, prefix)
        return contigFiles.__len__()
        
    def generateJobList(self):
        """Defines job parameters for merge. Updates variables for subsequent
        completion test in mergeComplete()
        """
        self.clearJobs()
        self.prevPrefix = self.varsP.inputContigPrefix
        self.curPrefix = self.prefixIter.next()
        self.varsP.updatePipeReport('   PREV PREFIX %s, CUR PREFIX %s' % (self.prevPrefix, self.curPrefix))
        self.iterCount += 1
        self.groupName = '  Merge %s' % self.curPrefix
        currentArgs = [self.varsP.RefAlignerBin] + self.varsP.argsListed('merge') 
        currentArgs += ['-maxthreads', str(self.varsP.nThreads)]
        contigsTextFile = os.path.join(self.varsP.inputContigFolder, 'mergeContigs.txt')
        contigFiles, contigIDs = self.varsP.findContigs(self.varsP.inputContigFolder, self.prevPrefix, txtOutput=contigsTextFile)
        self.varsP.prefixUsed.append(self.curPrefix)
        outputString = os.path.join(self.varsP.outputContigFolder, self.curPrefix)
        fileArgs = ['-if', contigsTextFile, '-o', outputString]
        if self.varsP.stdoutlog :
            fileArgs.extend( ['-stdout', '-stderr'] )
        jobName = 'Mrg_' + self.curPrefix
        expoutput = outputString+".align" #don't know which contigs will disappear, but should always get an align file
        s1Job = mthread.singleJob(currentArgs + fileArgs, jobName, expoutput, jobName,maxThreads=self.varsP.nThreads, clusterLogDir=self.varsP.clusterLogDir, expectedStdoutFile = outputString + ".stdout")
        self.addJob(s1Job)
        self.logArguments() 
            
    def mergeComplete(self):
        """Test if merge possibilities are exhaused and increment names and 
        counters.
        
        """
        prevCount = self.countContigs(self.varsP.inputContigFolder, self.prevPrefix)
        curCount = self.countContigs(self.varsP.outputContigFolder, self.curPrefix)
        self.varsP.stageComplete = 'Merge% 2d' % self.varsP.extensionCount
        self.checkResults()
        contigCount = '  %s %d to %s %d' % (self.prevPrefix, prevCount, self.curPrefix, curCount)
        self.varsP.inputContigPrefix = self.curPrefix
        self.varsP.inputContigFolder = self.varsP.outputContigFolder
        self.varsP.outputContigPrefix = self.curPrefix
        if curCount <= 1 or curCount >= prevCount or self.iterCount >= self.alphabet.__len__()-1:
            # Terminate Merging
            contigCount += '  .. Terminate Merge ..'
            #print(contigCount)
            self.varsP.updatePipeReport(contigCount + '\n')
            if curCount == 0:
                self.varsP.outputContigPrefix = self.prevPrefix
            self.varsP.mergeIntoSingleCmap()
            return 1
        else:
            contigCount += '  .. Continue Merge ..'
            self.varsP.updatePipeReport(contigCount + '\n')
            #print(contigCount)
            return 0
    
    def checkResults(self):
        ''' #old logic below--clean up using new jobWrapper method
        for sJob in self.jobList:
            sJob.CheckIfFileFound()
        self.pipeReport += self.makeRunReport()
        self.pipeReport += self.makeParseReport()
        self.varsP.updatePipeReport(self.pipeReport)
        '''
        self.doAllPipeReport() #see Multithreading.jobWrapper
