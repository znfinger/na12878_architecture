import os
import pdb

import utilities as util
import Multithreading as mthread

"""@package SampleCharModule Defines jobs to map bnx to reference and parses
mapping results

Optional pre-processor for De Novo assembly to characterize the single molecule
noise of the input. Reference required. Can help to inform run time noise 
arguments.
"""


util.setVersion("$Id: SampleCharModule.py 2237 2013-12-04 19:45:23Z MRequa $")


class SampleChar(mthread.jobWrapper):
    """Generates jobs for distributed single molecule mapping
    """
    def __init__(self, varsP):
        self.varsP = varsP
        stageName = '  Sample Characterization, Noise Levels'
        super(SampleChar, self).__init__(self.varsP, stageName, clusterArgs=varsP.getClusterArgs('sampleChar'))
        self.generateJobList()
    
    def runJobs(self):
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)
    
    def generateJobList(self):
        curArgs = self.varsP.argsListed('noise0') + self.varsP.argsListed('sampleChar')
        if util.checkFile(self.varsP.bnxTarget) : #file exists only if image processing was run
            bnxFiles = parseExperimentFile(self.varsP.bnxTarget)
            if not bnxFiles : #check that you got at least one
                errstr = "ERROR in SampleChar.generateJobList: no bnx files found in: "+self.varsP.bnxTarget
                print errstr
                self.varsP.updatePipeReport(errstr+"\n\n")
                return
            basepath = "" #os.path.split(bnxFiles[0])[0] #don't use basepath for this case
        else : #otherwise, assume this is the only bnx file
            bnxFiles = [self.varsP.bnxFile]
            #here, make a dir for the results--should really check results of checkEmptyDir for errors
            basepath = os.path.join(self.varsP.localRoot, "sampleChar")
            if self.varsP.wipe :
                util.checkEmptyDir(basepath) #will make if not exist, but if it does, will remove and re-make
            else :
                util.checkDir(basepath) #will make if not exist, but won't remove anything
        nJobs = len(bnxFiles)
        #for i, bnxFile in enumerate(bnxFiles):
        for bnxFile in bnxFiles :
            #bnxGroupName = '%02d' % (i+1) #get this from the path, ie, bnxFiles
            cargs = [self.varsP.RefAlignerBin, '-i', bnxFile]
            bnxname = os.path.split(bnxFile)[1].replace(".bnx","")
            jobname = 'Sample_Char_' + bnxname
            #outputTarget = os.path.join(basepath, bnxGroupName)
            if basepath : #bnx input
                outputTarget = os.path.join(basepath, bnxname)
            else : #image processing
                outputTarget = bnxFile.replace(".bnx","") + "_sampleChar"
            expectedResultFile = outputTarget + '.err' #this is used in checkResults
            currentArgs = cargs + ['-ref', self.varsP.ref, '-o' , outputTarget, '-f']
            if self.varsP.stdoutlog :
                currentArgs.extend( ['-stdout', '-stderr'] )
            currentArgs += ['-maxthreads', str(self.varsP.maxthreads)] + curArgs
            sJob = mthread.singleJob(currentArgs, jobname, expectedResultFile, jobname, clusterLogDir=self.varsP.clusterLogDir) # peStr is deprecated in favor of clusterargs
            #sJob.expTag = bnxGroupName #removed from checkResults
            self.addJob(sJob)
        self.logArguments()

    
    def checkResults(self):
        self.doAllPipeReport()
        self.infoReport = '  Sample Characterization:\n'
        for i,sJob in enumerate(self.jobList):
            if not sJob.resultFound :
                continue
            errSum = parseErrFile(sJob.expectedResultFile)
            if i == 0:
                self.infoReport += '    ExpID  ' + errSum.makeHeader() + '\n'
            #self.infoReport += '    Exp ' + sJob.expTag + ' ' + errSum.makeReport() + '\n' #no longer use expTag
            self.infoReport += sJob.jobName + ' ' + errSum.makeReport() + '\n'
        self.varsP.updateInfoReport(self.infoReport)
        

    
def parseErrFile(targetFile):
    f1 = open(targetFile)
    errResults = []
    while(True):
        line = f1.readline()
        if line == '': 
            f1.close()
            break
        if line[0] == 'I' or line[0] == '#':
            continue
        else:
            errResult = errFileResult(line)
            errResults.append(errResult)
    errSum = errFileSum(errResults)
    return errSum

    
class errFileSum():
    
    def __init__(self, errFileResults):
        self.FP = errFileResults[-1].FP
        self.FN = errFileResults[-1].FN
        self.ssd = errFileResults[-1].ssd
        self.sd = errFileResults[-1].sd
        self.bpp = errFileResults[-1].bpp
        self.nMaps = errFileResults[0].nMaps
        self.mapRate = float(errFileResults[-2].nMapsMapped) / errFileResults[-2].nMaps
    
    def makeHeader(self):
        output = '% 8s% 8s% 8s% 8s% 8s% 8s% 8s' %('NumMaps', 'MapRate', 'FP', 'FN', 'bpp', 'sd', 'ssd')
        return output
        
    def makeReport(self):
        #output = '% 8d ' % self.nMaps 
        #output += '% 8.3f   ' % self.mapRate
        #output += '% 8.3f   ' % self.FP
        #output += '% 8.4f   ' % self.FN
        #output += '% 8.1f   ' % self.bpp
        #output += '% 8.4f   ' % self.sd
        #output += '% 8.4f   ' % self.ssd
        output = '% 8d% 8.3f% 8.3f% 8.4f% 8.1f% 8.4f% 8.4f' % (self.nMaps, self.mapRate, self.FP, self.FN, self.bpp, self.sd, self.ssd)
        
        return output
        
class errFileResult():

    def __init__(self, line):
        tokens = [float(x) for x in line.strip().split('\t')]
        self.FP = tokens[1]
        self.FN = tokens[2]
        self.ssd = tokens[3]
        self.sd = tokens[4]
        self.bpp = tokens[5]
        self.nMaps = int(tokens[7])
        self.nMapsMapped = int(tokens[9])    


def groupBnxByExperimentMod(bnxFiles):
    prevExp = ''
    allGroups = []
    allGroupNames = []
    curGroup = []
    for bnxPath in bnxFiles:
        dump, bnxFile = os.path.split(bnxPath)
        allGroupNames.append(bnxFile[:4])
        allGroups.append([bnxPath])
    return allGroups, allGroupNames
    
def groupBnxByExperiment(bnxFiles):
    prevExp = ''
    allGroups = []
    allGroupNames = []
    curGroup = []
    for bnxPath in bnxFiles:
        dump, bnxFile = os.path.split(bnxPath)
        curExp = bnxFile[:2]
        if curExp == prevExp:
            curGroup.append(bnxPath)
        else:
            if curGroup.__len__() > 0:
                allGroups.append(curGroup)
            curGroup = [bnxPath]
            allGroupNames.append(curExp)
            prevExp = curExp
    if curGroup.__len__() > 0:
        allGroups.append(curGroup)            
    return allGroups, allGroupNames    


#open file targetFile, read bnx paths from it
#targetFile is output of Pipeline.writeIntermediate
def parseExperimentFile(targetFile):
    targetLocations = []
    f1 = open(targetFile)
    while(True):
        line = f1.readline()
        if line == '':
            f1.close()
            break
        if line[0] == '#':
            continue
        targetLocations.append(line.strip())
    return targetLocations
