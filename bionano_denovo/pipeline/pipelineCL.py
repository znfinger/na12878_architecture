import sys

"""
@package pipelineCL 
Entry point of Pipeline for command line

Usage: python pipelineCL.py -h
"""

if __name__ == "__main__":
    import Pipeline
    varsP = Pipeline.varsPipeline()
    varsP.prerunChecks()
    
    print('  Prerun Tests:\n\t%d ERRORS\n\t%d WARNINGS\n' % (varsP.error, varsP.warning))
    if varsP.error or varsP.warning:
        print(varsP.message)
    if varsP.error:
        print('  EXITING: See errors') 
        sys.exit()
    
    dnpipeline = Pipeline.DNPipeline()
    dnpipeline.run(varsP)
    
