import os

import Multithreading as mthread

"""@package PairwiseModule Defines jobs for execution of pairwise comparison

Major operation modes:
Basic distributed pairwise jobs
Distributed pairwise jobs following hash result
Distributed pairwise jobs following distributed hash resuls
Distributed pairwise jobs using triangle input partitioning
"""

import math
import utilities as util
util.setVersion("$Id: PairwiseModule.py 2383 2014-01-23 18:29:17Z wandrews $")



class Pairwise(mthread.jobWrapper):
    """Populates Multithreading package for distributed pairwise jobs
    """
    def __init__(self, varsP):
        self.varsP = varsP
        stageName = '  Pairwise Comparison of Molecules'
        mthread.jobWrapper.__init__(self, varsP, stageName,clusterArgs=varsP.getClusterArgs('pairwise'))
        self.generateJobList()
    
    def runJobs(self):
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)
    
    #__This will not work with hashing__
    # becuase hash is not supported with partial
    #triangle mode is now always on, so no need to check this
    def generateJobListLinear(self):
        """Pairwise.generateJobListLinear: This method is the old way of doing pairwise
        comparison of all molecules. It uses the -partial option to RefAligner. This
        option is _incompatible_ with the various hashing options to RefAligner.
        """
        baseArgs = self.varsP.argsListed('noise0') + self.varsP.argsListed('pairwise') 
        ct = 0
        outputTarget = os.path.join(self.varsP.alignFolder, 'exp')

        cArgs = [self.varsP.RefAlignerBin, '-i', self.varsP.bnxFile]
        for jobNum in range(1,self.varsP.nPairwiseJobs + 1):
            jobName = 'Pairwise %d of %d' % (jobNum, self.varsP.nPairwiseJobs)
            outputString = 'pairwise%dof%d' % (jobNum,self.varsP.nPairwiseJobs)
            expectedResultFile = outputTarget + outputString + '.align'
            partialArgs = ['-partial', str(jobNum), str(self.varsP.nPairwiseJobs)]
            currentArgs = cArgs + baseArgs + ['-o' , outputTarget + outputString]
            if self.varsP.stdoutlog :
                currentArgs.extend( ['-stdout', '-stderr'] )
            if self.varsP.nPairwiseJobs > 1:
                currentArgs += partialArgs
            currentArgs += ['-maxthreads', str(self.varsP.maxthreads)]
	    if self.varsP.bnxStatsFile!=None:
		currentArgs += ['-XmapStatRead', self.varsP.bnxStatsFile]
            sJob = mthread.singleJob(currentArgs, 
                                     jobName, 
                                     expectedResultFile, 
                                     outputString,
                                     maxThreads=self.varsP.maxthreads,
                                     clusterLogDir=self.varsP.clusterLogDir, expectedStdoutFile=outputTarget + outputString+".stdout")
            ct += 1
            self.addJob(sJob)
        self.logArguments()
        

    def generateJobListTriangle(self):
		baseArgs = self.varsP.argsListed('noise0') + self.varsP.argsListed('pairwise')

                #class defined below--constructor will create and launch jobs both for sortBNX and splitBNX
		try:
                    sortBNX(self.varsP) #raise RuntimeError if either job fails
                except RuntimeError :
                    return

		cArgs = [self.varsP.RefAlignerBin, '-i', self.varsP.bnxFile]
		ct = 0
		outputTarget = os.path.join(self.varsP.alignFolder, 'exp')
		njobs=self.varsP.nPairwiseJobs*(self.varsP.nPairwiseJobs+1)/2
		BNX_list=[]
		for i in range(1,self.varsP.nPairwiseJobs + 1):
			file1=self.varsP.bnxFile.replace(".bnx", "_%s_of_%s.bnx" %(i, self.varsP.nPairwiseJobs))
			BNX_list.append(file1+"\n")
			for j in range(i,self.varsP.nPairwiseJobs + 1):
				file2=self.varsP.bnxFile.replace(".bnx", "_%s_of_%s.bnx" %(j, self.varsP.nPairwiseJobs))
				jobName = 'Pairwise %d of %d' % (ct+1, njobs)
				outputString = 'pairwise%dof%d' % (ct+1, njobs)
				expectedResultFile = outputTarget + outputString + '.align'
				if i==j :
					currentArgs = [self.varsP.RefAlignerBin, '-i', file1] + ['-o' , outputTarget + outputString] + baseArgs
				else :
					currentArgs = [self.varsP.RefAlignerBin, "-first", "-1", "-i", file1, "-i", file2] + ['-o' , outputTarget + outputString] + baseArgs
                                if self.varsP.stdoutlog :
                                    currentArgs.extend( ['-stdout', '-stderr'] )
				#if self.varsP.nPairwiseJobs > 1:
					#currentArgs += partialArgs
				currentArgs += ['-maxthreads', str(self.varsP.maxthreads)]
				if self.varsP.bnxStatsFile!=None:
					currentArgs += ['-XmapStatRead', self.varsP.bnxStatsFile]
				#if ct == 0: #redundant with logArguments below
				#	self.pipeReport += " ".join(currentArgs) + 2 * '\n'
				sJob = mthread.singleJob(currentArgs, 
							jobName, 
							expectedResultFile, 
							outputString,
							maxThreads=self.varsP.maxthreads,
							clusterLogDir=self.varsP.clusterLogDir,
							expectedStdoutFile=outputTarget + outputString+".stdout",
							)#, shell=True)
				ct += 1
				self.addJob(sJob)
		self.varsP.bnxFileList=self.varsP.bnxFile.replace(".bnx", ".list")
		f=open(self.varsP.bnxFileList, "w")
		f.writelines(BNX_list)
		f.close()
		self.logArguments()

    def generateJobList(self):
		if self.varsP.pairwiseTriangleMode :
			self.generateJobListTriangle()
		else :
			self.generateJobListLinear()

	
    def checkResults(self):
        if self.varsP.ngsBypass : #this means that pairwise is skipped completely, so do not check anything
            return #return None means success
        self.doAllPipeReport() #loops over self.jobList and calls CheckIfFileFound
        #check for align files
        if not util.checkDir(self.varsP.alignFolder, makeIfNotExist=False) :
            self.varsP.updatePipeReport("ERROR: bad alignFolder:%s\n\n" % self.varsP.alignFolder)
            return 1

        alignFiles = []
        #for sJob in self.jobList:
        #    sJob.CheckIfFileFound()
        #    alignFile = sJob.expectedResultFile
        #    if sJob.resultFound:
        #        alignFiles.append(alignFile)
        #    else:
        #        self.warning += 1
        #        self.messages += '  PW Warning Missing Expected File: %s\n' % alignFile
        #if alignFiles.__len__() == 0:
        #    self.error += 1
        #    self.messages += '  Error: PW  Missing All Align Files\n' 
        #else:

        #Above uses results in singleJob instances, below reads from disk. Either way should work
        for ifile in os.listdir(self.varsP.alignFolder) :
            if ifile.endswith(".align") :
                alignFiles.append( os.path.join(self.varsP.alignFolder, ifile) )
        if len(alignFiles) == 0 :
            self.varsP.updatePipeReport("ERROR: no align files in alignFolder %s\n\n" % self.varsP.alignFolder)
            return 1

        self.varsP.writeListToFile(alignFiles, self.varsP.alignTarget)
        #self.varsP.error += self.error
        #self.varsP.warning += self.warning
        #self.varsP.message += self.messages
        self.varsP.stageComplete = 'Pairwise'
        #self.pipeReport += self.makeRunReport()
        #self.pipeReport += self.makeParseReport()
        #self.varsP.updatePipeReport(self.pipeReport)
       


class sortBNX(mthread.jobWrapper) :

    def __init__(self, varsP) :
        """splitBNX.__init__: this class is for sorting the input bnx
        for subsequent splitting by the splitBNX class, and eventually
        easier processing with the Pairwise class. The constructor
        (this) will call varsP.runJobs and doAllPipeReport, then
        instantiate splitBNX, which will do all the splitting required
        for the Pairwise class.
        """
        self.varsP = varsP #fewer code modifications below
        if self.generateJobList() : #return 0 for success, 1 for skip
            return
        self.varsP.runJobs(self, "SortBNX")
        self.doAllPipeReport()
        if not self.allResultsFound() :
            self.varsP.updatePipeReport("ERROR: sortBNX failed. Check: "+self.varsP.bnxFile+"\n")
            raise RuntimeError
        splitBNX(self.varsP) #now split the sorted bnx

    def runJobs(self) :
        self.multiThreadRunJobs(1)

    def generateJobList(self) :

        if not self.varsP.executeCurrentStage:
            return 1 #tell self.__init__ not to continue processing
	    
        sorted_file = self.varsP.bnxFile.replace(".bnx", "_sorted")
        self.varsP.sorted_file = sorted_file
        self.varsP.updatePipeReport('Sorting %s into %s\n' % (self.varsP.bnxFile, sorted_file))
	    
        jobName="SortingBNX"
        expectedResultFile=sorted_file+".bnx"
        # We use assembly section here because the memory usage is higher than pairwise, while the jobs are quite short.
        #sortJobSet=mthread.jobWrapper(self.varsP,jobName,clusterArgs=self.varsP.getClusterArgs('assembly'))
        super(sortBNX, self).__init__(self.varsP, jobName, clusterArgs=self.varsP.getClusterArgs("assembly"))

        cargs=[self.varsP.RefAlignerBin, '-f', '-i', self.varsP.bnxFile, "-maxthreads", str(self.varsP.maxthreads),  "-merge", "-sort-idinc", "-bnx", "-o", sorted_file] + self.varsP.argsListed('bnx_sort')
	if self.varsP.bnxStatsFile!=None:
		cargs += ['-XmapStatWrite', self.varsP.bnxStatsFile]
        if self.varsP.stdoutlog :
            cargs.extend( ['-stdout', '-stderr'] )
        self.addJob(mthread.singleJob(cargs, jobName, expectedResultFile, jobName, clusterLogDir=self.varsP.clusterLogDir, expectedStdoutFile=sorted_file+".stdout"))

        return 0 #success


class splitBNX(mthread.jobWrapper) :

    def __init__(self, varsP) :
        """splitBNX.__init__: this class is for splitting the sorted bnx file into
        smaller chunks for easier processing with the Pairwise class. Like the
        sortBNX class, the constructor also calls varsP.runJobs and doAllPipeReport.
        """
        self.varsP = varsP #fewer code modifications below
        self.generateJobList()
        self.varsP.runJobs(self, "splitBNX")
        self.doAllPipeReport()
        if not self.allResultsFound() :
            self.varsP.updatePipeReport("ERROR: splitBNX failed. Check: "+self.varsP.sorted_file+"\n")
            raise RuntimeError


    def runJobs(self):
        self.multiThreadRunJobs(1)


    def generateJobList(self) :
        """splitBNX.generateJobList: before making the jobs, open the bnx file and
        calculate the number of molcules and total length in order to calculate
        how many pieces to split to.
        """

        sorted_file = self.varsP.sorted_file

        f=open(sorted_file+".bnx", "r")
        count=0
        length=0
        for line in f:
            if line[0] == "0":
                x=line.split()
                count+=1
                length+=float(x[2])
        f.close()
	    
        nAuto=int(math.ceil(count/80000.0))
        N=self.varsP.nPairwiseJobs
	    
        self.varsP.updatePipeReport("splitBNX: Sorted and filtered file has %d molecules, %f length total. Auto blocks=%d\n\n" %(count, length, nAuto))
	    
        if nAuto > self.varsP.nPairwiseJobs:
            self.varsP.updatePipeReport("Increasing number of blocks from %d to %d\n" % (self.varsP.nPairwiseJobs, nAuto))
            self.varsP.nPairwiseJobs=nAuto
            N=nAuto
            
        self.varsP.updatePipeReport('Splitting BNX\n')
        jobName="SplittingBNX"
        #splitJobSet=mthread.jobWrapper(self.varsP,jobName,clusterArgs=self.varsP.getClusterArgs('splitting'))
        super(splitBNX, self).__init__(self.varsP,jobName,clusterArgs=self.varsP.getClusterArgs('splitting'))
	    
        for partial in range(1,N + 1):
            output_file=self.varsP.bnxFile.replace(".bnx", "_%s_of_%s" %(partial, self.varsP.nPairwiseJobs))
            cargs=[self.varsP.RefAlignerBin, '-f', '-i', sorted_file+".bnx", "-maxthreads", "2", "-merge", "-subsetbin", str(partial), str(N),"-bnx", "-o",  output_file]
            if self.varsP.stdoutlog :
                cargs.extend( ['-stdout', '-stderr'] )
            #print('%d/%d' % (partial, N), cargs)
            expectedResultFile=output_file+".bnx"
            self.addJob(mthread.singleJob(cargs, jobName + str(partial), expectedResultFile, jobName + str(partial), clusterLogDir=self.varsP.clusterLogDir, expectedStdoutFile=output_file+".stdout"))
	    
