import os
#import time
#import subprocess
import sys
import shutil
import xml.etree.ElementTree as ET
#import pdb
import argparse
import copy
import time

import PairwiseModule as pm
import ImageProcessingModule as ipm
import AssemblyModule as am
import RefinementModule as refmod
import GroupedRefinementModule as grprefmod
import CharacterizeModule as cm
import SampleCharModule as scm
import SVModule as svm
import Multithreading as mthread
import molecule
import utilities as util

"""@package Pipeline 
Main routine for defining and running De Novo Assembly


"""


util.setVersion("$Id: Pipeline.py 2483 2014-02-12 22:54:24Z wandrews $")


class varsPipeline():
    """Pipeline Variables Class.
    
        This class parses the command line and input files.  It stores the 
        runtime parameters and determines the process flow for the other
        modules.  Along with the process flow, the global file system is
        created, and binary dependencies are checked.
    """

    def __init__(self):
        self.localRoot = None # Top folder for analysis results
        self.toolsFolder = None # Target of executable files
        self.nThreads = 0       # Meaningful for local execution, how many concurrent jobs to run.
        self.nPairwiseJobs = 0
        self.nExtensionIter = 0 # Number of extension-merge iterations after assembly
        self.extensionCount = 0 # Running count of extension merge iterations
        self.expID = 'expDev'   # Prefix for all resulting output files
        self.ref = None         # Reference genome as cmap file (None if no reference)
        self.bypassCount = 0    # Number of stages to bypass (command line: -B)
        self.currentStageCount = 0   # Current Stage number, to be compared with bypass count
        self.executeCurrentStage = True  # Result of bypass count / currentStageCount comparison
        self.curCharacterizeTargets = [] 
        ## @var curCharacterizeTargets
        # Latest merged cmaps for characterization
        self.curCharacterizeCmaps = [] # Latest Cmaps fo characterization
        self.numCharacterizeJobs = 1 # Load balance the characterize work into numCharacterizeJobs jobs
        self.imgFile = None
        self.bnxFileIn = None
        self.bnxFile = None
        self.bnxStatsFile = None
        self.subsampleMb = 0
        self.doNoise = False
        self.noiseOnly = False 
        self.time=False
        self.perf=False
        self.startTime = 0 #time.time() #pipeline start time
        self.cleanup = False 
        self.genomeSize = 100 
        self.onCluster = False
        self.clusterArgumentsFileIn = None
        self.clusterLogDir = None
        self.wipe = False
        self.idFile = ''
        self.error = 0
        self.warning = 0
        self.message = ''
        self.assembleParams = {}
        self.optArgumentsFile = None
        #list blocks which are _always_ required in optArguments.xml (the -a parameter)
        #this will be added to depending on command line parameters
        self.requiredArgGroups = [#'imgDetection', #required only for -I
                                  #'sampleChar', #required only for -n
                                  'bnx_sort', #required because triangle mode is always on
                                  'noise0',
                                  'pairwise',
                                  'assembly',
                                  #'cmapMerge', #this is never required
                                  'refineA',
                                  'refineB',
                                  #'refineNGS', #required only for -f
                                  #'refineFinal', #NOT required for -u, otherwise required
                                  'extension',
                                  'merge',
                                  'characterizeDefault',
                                  #'characterizeStrict', #this is never required (obsolete)
                                  #'mapLambda', #required only for -L
                                  #'lambdaFilter', #required only for -L
                                  ]

        self.shortArgs = ['-T','-j','-N','-M','-i','-I','-b','-l','-t','-B','-e','-r','-H','-P','-s','-n','-x','-c','-g','-C','-w','-a','-L','-p','-d','-f','-F','-u','-U','-D','-S','-V','-W'] #this is used in celery implementation only (keep up to date)--no -v for celery implementation
        self.argData = {}
        self.stageComplete = None
        #self.curContigPrefix = None
        self.inputContigPrefix = ''
        self.outputContigPrefix = ''
        self.inputContigFolder = None
        self.outputContigFolder = None
        self.curContigCount = 0
        self.latestMergedCmap = None
        self.prefixUsed = []
        self.logstdouterr = False #logging std out and std err -- make command line option -- implementation in progress
        self.logdir = ''
        self.lambdaRef = None
        self.ngsInDir = None
        self.skipFinalRefine = None

    
    def toExecuteCurrentStage(self, quiet=False):
        """Check if the current stage is to be excecuted, determined by the 
        bypass parameter (command line: -B).
        """
        self.currentStageCount += 1
        if self.currentStageCount > self.bypassCount:
            self.executeCurrentStage = True
            if not quiet : outstr = "Executing"
        else:
            self.executeCurrentStage = False
            if not quiet : outstr = "Skipping"
        if not quiet :
            outstr += " stage number %i" % self.currentStageCount #, ":", self.stageComplete #problem with printing stageComplete is that it's the previous stage and the next stage is not known
            #print outstr
            self.updatePipeReport(outstr+"\n\n")
        return self.executeCurrentStage
    

    #This method is migrated to a fn in ImageProcessingModule
    #def joinBnxFiles(self):
    

    def prerunChecks(self):
        """Check inputs and run time parameters before starting assembly
        
        Parse the command line, check the integrity of binary and input files, 
        check existance of required files, check contridictions in flow based 
        on inputs. Finally assign pipeline variables and construct output file
        structure.
        """
        self.parseCommandLine()
        self.checkFlow() #this is more or less just command line option consistency checking--do before input checking
        self.checkInputs()
        self.assignFileTargets()
        self.checkToolPaths()
        self.checkCluster()

        self.RefAlignerBinOrig = self.RefAlignerBin
        self.AssemblerBinOrig = self.AssemblerBin
        self.HMstaticBinOrig = self.HMstaticBin
        self.DMstaticBinOrig = self.DMstaticBin
        if self.onCluster:
                self.RefAlignerBin += "${BINARY_SUFFIX:=}"
                self.AssemblerBin += "${BINARY_SUFFIX:=}"
                self.HMstaticBin += "${BINARY_SUFFIX:=}"
                self.DMstaticBin += "${BINARY_SUFFIX:=}"
        self.parseArguments()
        if self.onCluster:
            self.parseArguments(readingClusterFile=True)
        if self.error:
            return
        self.setupFileStructure()
        self.setCoverageBasedParams()

    
    def parseCommandLine(self):
        parser = argparse.ArgumentParser(description='Pipeline for de novo assembly - BioNano Genomics')
        parser.add_argument('-T', help='Available Threads per Node ', default=1, type=int)
        parser.add_argument('-j', help='Threads per Job, -maxthreads (non-cluster only)',dest = 'maxthreads', default = 1,type=int)
        parser.add_argument('-N', help='Number of partial pairwise jobs (optional, default: -T)', default = -1,type=int)
        #parser.add_argument('-G', help='Retired option (pairwise triangle mode), always enabled.', action='store_true')
        parser.add_argument('-G', help='Compute and use global statistics on input molecules, requires RefAligner >= 2329.', default=1, action='store_false')
        parser.add_argument('-i', help='Number of extension and merge iterations (default=1)', dest='iter', default = '1',type=int)
        parser.add_argument('-I', help='File with listed experiment targets, use without -bnx',dest='img', default=None)
        parser.add_argument('-b', help='Input molecule (.bnx) file, use without -img',dest = 'bnx', default=None)
        parser.add_argument('-l', help='Location of output file root',dest='local', default="")
        parser.add_argument('-t', help='Location of executable files',dest='tools', default="")
        bypassHelp = 'Skip steps, using previous result. 0:None, 1:ImgDetect, 2:NoiseChar/Subsample, 3:Pairwise, 4:Assembly, 5:RefineA, 6:RefineB, 7:RefineNGS (-f only), 7+(i-1)*2:Ext(i), 8+(i-1)*2:Mrg(i)'
        parser.add_argument('-B', help=bypassHelp,dest='bypass', default = '0',type=int)
        parser.add_argument('-e', help='Experiment file prefix (optional, default = exp)',dest='exp', default = 'exp')
        parser.add_argument('-r', help='Reference File cmap or spots, to compare resulting contigs',dest='ref', default=None)
        parser.add_argument('-s', help='Make a subsampling of the input set in Mb. (optional)',dest='sub', default = 0)
        parser.add_argument('-n', help='Evaluate single molecule noise characterization',dest='noise', action='store_true')
        parser.add_argument('-x', help='Exit after noise characterization, skip de novo', action='store_true')
        parser.add_argument('-c', help='Remove intermediate contig files',dest='cleanup', action='store_true')
        parser.add_argument('-g', help='Organism genome size estimate in megabases, used for tuning assembly parameters',dest='size', default=0)
        parser.add_argument('-C', help='Run on cluster, read XML file for submission arguments',dest='cxml', default=None)
        parser.add_argument('-w', help='Wipe clean previous contig results', dest='wipe', action='store_true')
        parser.add_argument('-a', help='Read XML file for parameters (required)', dest='xml', default=None)
        parser.add_argument('-L', help='Lambda phage is spiked in, used for molecule scaling', dest='lambdaRef', default=None)
        parser.add_argument('-p', help='Log performance in pipelineReport 0=None, 1=time, 2=perf, 3=time&perf (default=1)', dest='perf', default=1,type=int)
        #parser.add_argument('-d', help='Make contig subdirectories for each contig-generating phase',dest='dirs', action='store_true')
        parser.add_argument('-d', help='Retired option (contig subdirectories), always enabled.',dest='dirs', action='store_true')
        parser.add_argument('-f', help='Directory which contains NGS contigs as cmaps for use in refineNGS.', dest='ngsarg', default=None)
        parser.add_argument('-F', help='When using -f, disable min contigs check for all stages prior to refineNGS.', dest='ngsskiparg', action='store_true')
        parser.add_argument('-u', help='Do not perform a final refinement.', dest='nofinalref', action='store_true')
        parser.add_argument('-U', help='Group contigs in refinement and extension stages', dest='groupcontigs', action='store_true', default=0)
        parser.add_argument('-D', help='Distribute contig characterization into parts. (optional, default=1)',dest='nCharJobs', default = 1, type=int)
        parser.add_argument('-v', help='Print version; exit if argument > 1 supplied.', nargs="?", dest='version', const=1, default=0, type=int)
        parser.add_argument('-S', help='Log stdout/stderr of all RefAligner calls in files with same names as output files and suffix stdout (default on--use this flag to turn off).', dest='stdoutlog', default=1, action='store_false')
        parser.add_argument('-V', help='Detect structural variations. Default: only after final stage (normally refineFinal); if argument 2, also after refineB; if argument 0, disable.', dest='runsv', default=1, type=int)
        parser.add_argument('-W', help='Reserved for celery implementation (do not use).',dest='workorder')
        
        result = parser.parse_args()

        #if result.version : #old behavior was to only print on -v 1; new is to always print and log, and exit on > 1
        # need to wait until setupFileStructure though to have the pipereport available
        if result.version > 1 :
            print( "Pipeline Version: %s" % util.getMaxVersion() )
            sys.exit(0)
        
        # assign Run Time Parameters
        self.localRoot = os.path.abspath(result.local)
        self.toolsFolder = result.tools
        self.nThreads = result.T
        self.nPairwiseJobs = (result.N if result.N != -1 else result.T)
        self.pairwiseTriangleMode = True #result.G #this is now always enabled
        self.GlobalStats = result.G
        self.groupContigs=result.groupcontigs
        
        self.maxthreads = result.maxthreads
        self.nExtensionIter = result.iter
        self.imgFile = result.img
        self.expID = result.exp
        self.ref = result.ref
        self.bypassCount = result.bypass
        self.bnxFileIn = result.bnx
        self.subsampleMb = float(result.sub)
        self.noiseOnly = result.x
        self.doNoise = (result.noise or self.noiseOnly) #if noiseOnly, assume doNoise also
        self.cleanup = result.cleanup
        self.genomeSize = float(result.size)
        self.onCluster = False
        if result.cxml:
            self.onCluster = True
            self.clusterArgumentsFileIn = result.cxml
        self.optArgumentsFileIn = result.xml
        self.wipe = result.wipe
        self.lambdaRef = result.lambdaRef
        self.time = False
        self.perf = False
        if result.perf == 1:
            self.time = True
        if result.perf == 2:
            self.perf = True
        if result.perf == 3:
            self.time = True
            self.perf = True
        if result.perf > 3 or result.perf < 0:
            self.error += 1
            self.message += '  ERROR:   -p must be between 1-3 input: %d\n' % result.perf
        self.workorder = result.workorder 
        self.contigSubDirectories = True #result.dirs #always True now
        self.ngsInDir = result.ngsarg
        self.ngsBypass = 0 #if not using, 0 is sufficient
        #if -F (ngsskiparg), must know how many stages to skip. The number is 6, ie, up to and including refineB
        if result.ngsskiparg :
            self.ngsBypass = 7
        self.skipFinalRefine = result.nofinalref
        self.numCharacterizeJobs = result.nCharJobs
        self.stdoutlog = result.stdoutlog
        if result.runsv < 0 :
            self.message += '  ERROR:   argument -V must be integer >= 0: %i\n' % result.runsv
            self.error += 1
        self.runSV = result.runsv

        
    def setupFileStructure(self):
        aboveLocalRoot = os.path.split(self.localRoot.rstrip("/"))[0] #if localRoot ends in "/", need the rstrip
        if not util.checkDir(aboveLocalRoot, makeIfNotExist=False) :
            self.message += '  ERROR:   Parent Directory Non-existant: %s\n' % aboveLocalRoot
            self.error += 1
            return
        if not util.checkDir(self.localRoot) : #this will make if not exists
            self.message += '  ERROR:   Could not create %s\n' % self.localRoot
            self.error += 1
            return
        if self.wipe and os.path.exists(self.contigFolder):
            shutil.rmtree(self.contigFolder)
        if not self.noiseOnly :
            if not util.checkDir(self.contigFolder) : #this will make if not exists
                self.message += '  ERROR:   Could not create %s\n' % self.contigFolder
                self.error += 1
                return
            if not util.checkFile(self.idFile) :
                f1 = open(self.idFile, 'w')
                f1.close()
                if not util.checkFile(self.idFile) :
                    self.message += '  ERROR:   Bad ID file %s\n' % self.idFile
                    self.error += 1

        #if contigSubDirectories, then each one has its own alignref which is made inside CharacterizeModule
        if not(os.path.exists(self.contigAlignTarget)) and self.ref and not self.contigSubDirectories :
            os.mkdir(self.contigAlignTarget)
        #also wipe the align folder
        if self.wipe and os.path.exists(self.alignFolder):
            shutil.rmtree(self.alignFolder)
        if not self.noiseOnly :
            if not util.checkDir(self.alignFolder) :
                self.message += '  ERROR:   Could not create %s\n' % self.alignFolder
                self.error += 1
                return

        if self.onCluster:
            if not(os.path.exists(self.clusterLogDir)):
                os.mkdir(self.clusterLogDir)
        if self.bnxFileIn:
            self.bnxFileIn = os.path.abspath(self.bnxFileIn)
            if not(self.bnxFileIn == self.bnxFile):
                shutil.copy(self.bnxFileIn, self.bnxFile)
        if self.optArgumentsFileIn:
            self.optArgumentsFileIn = os.path.abspath(self.optArgumentsFileIn)
            if not(self.optArgumentsFileIn == self.optArgumentsFile):
                shutil.copy(self.optArgumentsFileIn, self.optArgumentsFile)
        if self.clusterArgumentsFileIn:
            self.clusterArgumentsFileIn = os.path.abspath(self.clusterArgumentsFileIn)
            if not(self.clusterArgumentsFileIn == self.clusterArgumentsFile):
                shutil.copy(self.clusterArgumentsFileIn, self.clusterArgumentsFile)
        if self.logstdouterr :
            self.logdir = os.path.join(self.localRoot, 'logs')
            util.checkDir(self.logdir)
        
        util.InitStatus(os.path.join(self.localRoot, "status.xml"))
    
        pipeReport = ' '.join(sys.argv) + '\n\n'
        infoReport = ' '.join(sys.argv) + '\n\n'
        self.updatePipeReport(pipeReport, printalso=False)
        self.updateInfoReport(infoReport)
        
    def checkInputs(self):
        if (self.doNoise or self.noiseOnly) and not self.ref :
            self.message += '  ERROR: Cannot run sample characterization experiment (-n or -x) with no reference (-r)\n'
            self.error += 1
        #now require ref is a cmap or spots file
        if self.ref:
            if not util.checkFile(self.ref, ".cmap") and not util.checkFile(self.ref, ".spots") :
                self.message += '  ERROR:   Reference not found or invalid file type (must be .spots or .cmap): %s\n' % self.ref
                self.error += 1
                self.ref = None
            else :
                self.ref = os.path.abspath(self.ref)
        #require ref for runSV
        if not self.ref and self.runSV :
            self.message += '  WARNING: Disabling SV detection (-V, default ON) because no reference supplied, or invalid.\n'
            self.warning += 1
            self.runSV = False
        #also require it is a cmap, because spots doesn't work with -i input which is required for SVModule
        elif self.ref and self.ref.endswith(".spots") :
            self.message += '  WARNING: Disabling SV detection (-V, default ON) because reference supplied is spots file: use .cmap to enable SV detection.\n'
            self.warning += 1
            self.runSV = False
        #check bnx/image inputs
        if not self.bnxFileIn and not self.imgFile :
            self.message += '  ERROR:   Must provide either -I or -b as input\n'
            self.error += 1
        elif self.bnxFileIn and self.imgFile:
            self.message += '  ERROR:   Cant provide two experiment sources use -img or -bnx exclusive\n'
            self.error += 1
        if self.bnxFileIn:
            if not util.checkFile(self.bnxFileIn, ".bnx") :
                self.message += '  ERROR:   Bnx File: %s not found or invalid type.\n' % self.bnxFileIn
                self.error += 1
        if self.imgFile:
            if not util.checkFile(self.imgFile) : #just a text file, no type checking
                self.message += '  ERROR:   Img File: %s  not found\n' % self.imgFile
                self.error += 1
            if self.bypassCount :
                self.message += '  ERROR:   Img File supplied, but bypassed (%i) : cannot bypass image processing\n' % self.bypassCount
                self.error += 1
                
        if self.optArgumentsFileIn:
            if not os.path.exists(self.optArgumentsFileIn) :
                self.message += '  ERROR:   XML File not found %s\n' % self.optArgumentsFileIn
                self.error += 1
        else:
            self.message += '  ERROR:   -a XML File required\n' 
            self.error += 1
        
        if self.onCluster:
            if not os.path.exists(self.clusterArgumentsFileIn) :
                self.message += '  ERROR:   XML Cluster File not found %s\n' % self.clusterArgumentsFileIn
                self.error += 1
        
        if self.onCluster and self.imgFile:
            self.message +=  '  ERROR:   -C and -I, cant run img detection on cluster (I/O issue)\n' % self.imgFile
            self.error += 1
        
        if self.lambdaRef:
            if not os.path.exists(self.lambdaRef) :
                self.message +=  '  ERROR:   -L, Lambda Reference %s not found\n' % self.lambdaRef
                self.error += 1

        if self.ngsInDir :
            self.checkNgsDir() #for ngs dir, need various checks
            
        if self.numCharacterizeJobs > 1 and not(self.ref):
            self.message += '  WARNING:  Distributing characterize jobs (-D %d) without a reference (-r)\n' % self.numCharacterizeJobs
            self.message += '       Setting -D 1 and continuing\n'
            self.numCharacterizeJobs = 1
            self.warning += 1

        #check that -F (ngsBypass) isn't used without -f (ngsInDir), otherwise exitTestMinimumContigs is broken
        if self.ngsBypass and not self.ngsInDir :
            self.message +=  '  ERROR:   -F (skip minimum contigs check: %s) can only be used with -f (ngs contigs dir: %s) ' % (str(self.ngsBypass), self.ngsInDir)
            self.error += 1


    #setup ngs data members based on ngsInDir, which is a command line arg
    def checkNgsDir(self) :
        #check dir exists
        if not util.checkDir(self.ngsInDir, checkWritable=False, makeIfNotExist=False) :
            self.message += '  ERROR:   NGS dir does not exist %s\n' % self.ngsInDir
            self.error += 1
            return #abort if dir not found

        #check it has cmaps, set data member for it
        hascmap = False #guilty until proven innocent
        self.ngsContigPrefix = ""
        for ngsf in os.listdir(self.ngsInDir) :
            #If no "_contig", no valid prefix (this format is required by findContigs()), so no valid cmap
            if util.checkCmap( os.path.join(self.ngsInDir, ngsf) ) and util.hasSubStr(ngsf, "_contig") :
                hascmap = True #true after just one
                #the prefix is everything before "_contig". 
                newprefix = ngsf[:ngsf.find("_contig")]
                #check that prefixes are consistent -- require at least two (ie, confirm previous)
                if self.ngsContigPrefix and self.ngsContigPrefix == newprefix :
                    break #have confirmation, so done getting prefix
                else : #first time found
                    self.ngsContigPrefix = newprefix 
        if not hascmap :
            self.message += '  ERROR:   NGS dir does not contain any cmaps %s\n' % self.ngsInDir
            self.error += 1
        #print "self.ngsContigPrefix = ", self.ngsContigPrefix #debug

                
    def checkFlow(self):
        if self.nPairwiseJobs <= 0:
            self.self.message += '  ERROR:   Number of Pairwise Jobs (-n) must be >= 1'
            self.error += 1
        if self.nThreads <= 0:
            self.message += '  ERROR:   Number of Threads (-T) must be >= 1'
            self.error += 1
        if self.maxthreads > self.nThreads:
            self.message += '  ERROR:   Number of Threads (-T) must be >= Job Threads (-j)'
            self.error += 1
        if self.bypassCount > 4 and self.wipe:
            self.message += '  ERROR:   -b bypassing assembly and -w wiping previous contigs'
            self.error += 1
        if self.subsampleMb < 0:
            self.message += '  ERROR:   -s subsample must be positive number'
            self.error += 1

    def assignFileTargets(self):
        
        self.localRoot = os.path.abspath(self.localRoot)
        self.contigFolder = os.path.join(self.localRoot, 'contigs')
        self.alignFolder = os.path.join(self.localRoot, 'align')
        #self.hashFolder = os.path.join(self.localRoot, 'hash')
        if self.onCluster:
            self.clusterLogDir = os.path.join(self.localRoot,'ClusterLogs')

        self.toolsFolder = os.path.abspath(self.toolsFolder)
        self.RefAlignerBin = os.path.join(self.toolsFolder, 'RefAligner')
        self.AssemblerBin = os.path.join(self.toolsFolder, 'Assembler')
        self.HMstaticBin = os.path.join(self.toolsFolder, 'HM-static')
        self.DMstaticBin = os.path.join(self.toolsFolder, 'DM-static')
        self.bnxFile = os.path.join(self.localRoot, 'all.bnx')
        if self.GlobalStats : #-G option: bnxStatsFile defaults to None
            self.bnxStatsFile = os.path.join(self.localRoot, 'molecule_stats.txt')
        self.bnxTarget = os.path.join(self.localRoot, 'bnxOut')
        self.extraInfo1 = os.path.join(self.localRoot, 'extraDat1')
        self.extraInfo2 = os.path.join(self.localRoot, 'extraDat2')
        self.alignTarget = os.path.join(self.alignFolder, 'alignOut')
        self.contigMergeTarget = os.path.join(self.contigFolder, 'curContigs')
        self.idFile = os.path.join(self.contigFolder, 'ID')
        self.contigAlignTarget = os.path.join(self.contigFolder, 'alignref')
        
        self.pipeReportFile = os.path.join(self.localRoot, self.expID + '_pipelineReport.txt')
        self.infoReportFile = os.path.join(self.localRoot, self.expID + '_informaticsReport.txt')
        self.optArgumentsFile = os.path.join(self.localRoot, self.expID + '_optArguments.xml')
        self.clusterArgumentsFile = os.path.join(self.localRoot, self.expID + '_clusterArguments.xml')
        if os.path.exists(self.pipeReportFile):
            fileCt = 1
            while(True):
                testFileTag = '_%02d.txt' % fileCt
                testFileName = self.pipeReportFile.replace('.txt', testFileTag)
                if fileCt >= 99 or not os.path.exists(testFileName) :
                    self.pipeReportFile = testFileName
                    self.infoReportFile = self.infoReportFile.replace('.txt', testFileTag)
                    self.optArgumentsFile = self.optArgumentsFile.replace('.xml', '_%02d.xml' % fileCt)
                    self.clusterArgumentsFile = self.clusterArgumentsFile.replace('.xml', '_%02d.xml' % fileCt)
                    break
                fileCt += 1
                

    def checkToolPaths(self):
        if not util.checkExecutable(self.RefAlignerBin):
            self.message += '  ERROR: RefAligner not found or not executable, exiting\n'
            self.error += 1
        if self.bypassCount < 5 and not util.checkExecutable(self.AssemblerBin) :
            self.message += '  ERROR: Assembler not found or not executable, exiting\n'
            self.error += 1
        #if self.hashMethod == 1 and not util.checkExecutable(self.HMstaticBin) :
        #    self.message += '  ERROR: HM-static not found or not executable, exiting\n'
        #    self.error += 1
        if not self.bnxFileIn and not util.checkExecutable(self.DMstaticBin) :
            self.message += '  ERROR: DM-static not found or not executable, exiting.\n'
            self.error += 1
        

    def checkCluster(self):
        if self.onCluster:
            try:
                import drmaa
            except:
                self.message += '  ERROR:  option -C:  Error importing dmraa library\n'
                self.message += '  ERROR:   try: sudo apt-get install python-drmaa\n'
                self.error += 1
            try:
                sgeRoot = os.environ['SGE_ROOT']
            except:
                self.message += ' option -C:  Requires enviroment variable "SGE_ROOT" to be set\n'
                self.error += 1
    

    def getAssemblyParams(self,mbInput):
        """Sets the pvalue thresholds based on genome size input parameter.
        
        """
    
        infoReport = ''
        Tval = 10/(self.genomeSize*1e6) # Sets the pvalue threshold for matches based on genome size
        TEval = Tval/10
        T = str(Tval)
        TE = str(TEval)
        aveCov = mbInput / self.genomeSize
        #changing the minimum coverage is dangerous--below 5 maps, results shouldn't be trusted, so don't change these
        #minCov = str(min(20, max(2,int(aveCov/5))))#*
        #minAvCov = str(min(10, max(1,int(aveCov/10))))#*
        ''' #old format of argData was dict
        self.argData['pairwise'  ].update({'-T':T})
        self.argData['assembly'  ].update({'-T':T, '-MinCov':minCov, '-MinAvCov' : minAvCov})
        self.argData['refineA'   ].update({'-T':T})
        self.argData['refineB'   ].update({'-T':TE})
        self.argData['extension' ].update({'-T':TE, '-TE':TE})
        '''
        #new format is list--use small utility fn
        self.replaceParam('pairwise' , '-T', T)
        self.replaceParam('assembly' , '-T', T)
        self.replaceParam('refineA'  , '-T', T)
        self.replaceParam('refineB'  , '-T', TE)
        self.replaceParam('extension', '-T', TE)
        self.replaceParam('extension', '-TE', TE)

        infoReport += '  Average Coverage %3.2fx, of a %3.1f Mb Genome (user input)\n' % (aveCov, self.genomeSize)
        if aveCov < 30:
            infoReport += '  WARNING: Average Coverage %1.1fx, is below Min. Recommended (30x)\n' % aveCov
        print infoReport
        self.updateInfoReport(infoReport)


    def replaceParam(self, stage, param, newval) :
        if not param in self.argData[stage] : #if it's not there, add it
            self.argData[stage].extend( [param, newval] )
            return
        idx = self.argData[stage].index(param) #if it is there, replace it
        self.argData[stage][idx+1] = newval #replace the next element
        

    def parseArguments(self, readingClusterFile=False):
        """Read XML file for runtime arguments
        
        """
        if not(readingClusterFile):
            targetFile = self.optArgumentsFileIn
        else:
            targetFile = self.clusterArgumentsFileIn

        try:
            tree = ET.parse(targetFile)
        except Exception,e: #the actual type is 'xml.etree.ElementTree.ParseError', but lets just catch 'em all
            self.error += 1
            self.message += '  ERROR: Invalid xml file %s: %s\n' % (targetFile, e)
            return
        # try to get xml.etree.ElementTree version to check compatibility
        requiredxmlver = "1.3.0" #from /usr/lib/python2.7/xml/etree/ElementTree.py
        xmlver = "" #try to get this from ET (xml.etree.ElementTree)
        try:
            #print "XML PARSER VERSION =", ET.VERSION 
            xmlver = ET.VERSION
        except :
            self.warning += 1
            self.message += '  WARNING: version of xml.etree.ElementTree not found\n'
        if xmlver != requiredxmlver :
            self.warning += 1
            self.message += '  WARNING: version of xml.etree.ElementTree is not recommended version (%s)\n' % requiredxmlver
        
        #print '  Using %s for arguments' % targetFile
        allArguments = {}
        for argGroup in tree.getroot().getchildren() : #loop on modules (pairwise, assembly, etc)
            curArgs = [] #use list to preserve order
            for aflag in argGroup.getchildren() : #each arg for this module
		if sys.version_info < (2,7,0):
			elements=aflag.getiterator()
		else:
			elements=aflag.iter()
                for pair in elements : #iterate in order: each element in module
                    include = False
                    if pair.tag == "include" :
                        include = True
                    elif pair.get("attr") : #skip comment lines; no attr in include tag
                        curArgs.append( pair.get("attr") ) #ie, -T
                    for key in sorted(pair.attrib.keys()) : #you have to do this for 'valN' to be in order
                        if key.startswith("val") :
                            if include :
                                curArgs.extend( allArguments[pair.get(key)] )
                            else :
                                curArgs.append( pair.get(key) )
            #special code for -contigsplit:
            # for common sections, this will get added again--check if it's already there
            if "-contigsplit" in curArgs and not curArgs[curArgs.index("-contigsplit")+3].endswith("ID") :
                #RefAligner requires two floats following -contigsplit, then the count file--assume two floats are present
                curArgs.insert(curArgs.index("-contigsplit")+3, self.idFile)

            allArguments[argGroup.tag] = curArgs
        #end loop on xml
        if not(readingClusterFile):
            self.argData = allArguments
        else:
            self.clusterArgData = allArguments
        self.checkRequiredArguments()


    def checkRequiredArguments(self):
        """check that the xml file has all of the argument sets which will be 
        used by any pipeline module
        
        """
        argDataKeys = self.argData.keys() #all of the arg groups read in from the xml
        #eles of requiredArgGroups are always required
        for arg in self.requiredArgGroups :
            if not arg in argDataKeys :
                self.error += 1
                self.message += '  ERROR: no parameter definitions for required stage %s in %s\n' % (arg, self.optArgumentsFileIn)

        #check for required arg sets based on command line flags
        if self.imgFile and not "imgDetection" in argDataKeys :
            self.error += 1
            self.message += '  ERROR: no parameter definitions for imgDetection in %s, but image file %s supplied\n' % (self.optArgumentsFileIn, self.imgFile)

        if self.doNoise and not "sampleChar" in argDataKeys :
            self.error += 1
            self.message += '  ERROR: no parameter definitions for sampleChar in %s, but noise characterization (-n) specified\n' % (self.optArgumentsFileIn)        

        if self.lambdaRef and (not "mapLambda" in argDataKeys or not "lambdaFilter" in argDataKeys) :
            self.error += 1
            self.message += '  ERROR: no parameter definitions for mapLambda or lambdaFilter in %s, but lambda ref %s supplied\n' % (self.optArgumentsFileIn, self.lambdaRef)

        if self.ngsInDir and not "refineNGS" in argDataKeys :
            self.error += 1
            self.message += '  ERROR: no parameter definitions for refineNGS in %s, but ngs contig dir %s supplied\n' % (self.optArgumentsFileIn, self.ngsInDir)

        if not self.skipFinalRefine and not "refineFinal" in argDataKeys :
            self.error += 1
            self.message += '  ERROR: no parameter definitions for refineFinal in %s, but not skipping final refinement (-u)\n' % (self.optArgumentsFileIn) 

        if self.runSV and not "svdetect" in argDataKeys :
            self.error += 1
            self.message += '  ERROR: no parameter definitions for svdetect in %s, but detecting SVs (-V, default ON)\n' % (self.optArgumentsFileIn) 

        if self.groupContigs :
            groupstages = ["refineB0", "refineB1", "extension0", "extension1", "refineFinal0", "refineFinal1"]
            for stg in groupstages :
                if not stg in argDataKeys :
                    self.error += 1
                    self.message += '  ERROR: running grouped refine/extension (-U) but parameter definitions in %s missing %s\n' % (self.optArgumentsFileIn, stg)

    #end checkRequiredArguments

    
    def setCoverageBasedParams(self):
        """Set coverage threshold values based on input coverage depth
        
        (depricated by optArguments.xml)
        """
        if self.genomeSize:
            print('  Reading coverage depth from bnx...')
            dset = molecule.moleculeDataset(500, PitchNm = 700.)
            dset.readBnxFile(self.bnxFile, mbOnly=True)
            mbInput = dset.MegabasesDetected
            self.getAssemblyParams(mbInput)
    
    def subSampleBnx(self):
        """Subselect molecule from bnx file randomly for reduced coverage input
        
        """
        if self.subsampleMb:
            self.error = molecule.subsampleBnx(self, bypass = not(self.executeCurrentStage))
            
    def argsListed(self, moduleName):
        """Get arguments for stage moduleName from self.argData
        which were read from optArgumentsFileIn xml file in self.parseArguments
        
        """
        return self.argData[moduleName]


    def getClusterArgs(self, moduleName, category=None):
        """Get cluster arguments for stage moduleName from self.clusterArgData
        which were read from clusterArgs xml file in self.parseArguments
        
        """
        if not self.onCluster :
            return None
        #new behavior: if key is present, proceed properly
        # if not, print an error
        if category:
		mname=moduleName+category
	else:
		mname=moduleName
        if self.clusterArgData.has_key(mname) :
            return ' '.join(self.clusterArgData[mname])
        if category and moduleName!="default":
		return self.getClusterArgs("default", category)
        print '  Failed to read cluster Arguments for module %s section %s' % (moduleName, mname)
        return ''

    
    def prerunLog(self):
        """At start of DNPipeline, call this method to log information before any
        processing starts. So far, only Pipeline version and start time.
        """
        #record version here in pipereport only
        self.updatePipeReport( "  Pipeline Version: %s\n\n" % util.getMaxVersion() )
        #put time here because it looks better after the Prerun Tests in pipelineCL.py
        self.startTime = time.time() #time since Epoch
        self.updatePipeReport( "  Pipeline start time: %s\n\n" % time.ctime(self.startTime) )


    def FinalCleanup(self):
        """Remove intermediate cmap files for simpler resulting file structure
        
        Feature requested by software group (-c command line option)
        """
        if not(self.cleanup):
            return
        finalMergedPrefix = self.prefixUsed[-1]
        src = os.path.join(self.contigFolder, finalMergedPrefix + '.cmap')
        finalCmap = self.expID + '.cmap'
        dst = os.path.join(self.contigFolder, finalCmap)
        shutil.move(src, dst)
        finalPrefix = finalMergedPrefix.lower()
        allFiles = os.listdir(self.contigFolder)
        for curFile in allFiles:
            if curFile.startswith(finalPrefix):
                continue
            if curFile == finalCmap:
                continue
            pathname = os.path.join(self.contigFolder, curFile)
            if os.path.isdir(pathname):
                continue
            try:
                os.remove(pathname)
            except:
                pass
        
    def prepareContigIO(self, contigPrefix, updateDirs=True):
        ''' Set the input folder and prefix, as well as the output 
        
        '''
        self.inputContigPrefix = self.outputContigPrefix
        self.outputContigPrefix = contigPrefix
        if self.contigSubDirectories:
            if updateDirs:
                self.inputContigFolder = self.outputContigFolder
                self.outputContigFolder = os.path.join(self.contigFolder, contigPrefix)
                if not util.checkDir(self.outputContigFolder) : #will make if not exist, only returns False if already exists or can't make
                    print "ERROR in varsPipeline.prepareContigIO: bad dir:", self.outputContigFolder
        else:
            self.inputContigFolder = self.contigFolder
            self.outputContigFolder = self.contigFolder
        self.prefixUsed.append(self.outputContigPrefix) 


    def mergeIntoSingleCmap(self):
        """Merge resulting contigs into single cmap file
        
        """
        debug = False
        log = False
        if debug :
            print "\ncontigFolder:", self.outputContigFolder #debug
            print "outputContigPrefix:", self.outputContigPrefix #debug
            print "contigMergeTarget:", self.contigMergeTarget #debug
        
        # new variable : self.numCharacterizeJobs - distribute characterization job (by size)
        contigFiles, contigIDs = self.findContigs(self.outputContigFolder, self.outputContigPrefix, txtOutput=self.contigMergeTarget)
        self.curContigCount = len(contigFiles)
        if debug :
            print "curContigCount:", self.curContigCount, "\n"
        if len(contigFiles) == 0:
            self.latestMergedCmap = None
        if len(contigFiles) == 1:
            self.latestMergedCmap = os.path.join(self.outputContigFolder, contigFiles[0])

        mergeTargets = [self.contigMergeTarget]
        if self.numCharacterizeJobs > 1:
            mergeTargets += self.curCharacterizeTargets
        
        stageName = 'Cmap_Merge ' + self.outputContigPrefix #self.curContigPrefix
        mergeJobSet = mthread.jobWrapper(self,stageName,clusterArgs=self.getClusterArgs('cmapMerge'))
        self.curCharacterizeCmaps = []
        for i,mergeTarget in enumerate(mergeTargets):
            cargs = [self.RefAlignerBin]
            cargs += ['-if', mergeTarget]
            if i == 0:
                mergeFileName = util.uniquifyContigName(self.outputContigPrefix)
                self.prefixUsed.append(mergeFileName)
                outputTarget = os.path.join(self.outputContigFolder, mergeFileName)
                expectedResultFile = outputTarget + '.cmap'
                keyResult = expectedResultFile
                if self.numCharacterizeJobs == 1:
                    self.curCharacterizeCmaps.append(expectedResultFile)
            else:
                outputTarget = os.path.join(self.outputContigFolder, mergeFileName + '_partial_%d' % (i))
                expectedResultFile = outputTarget + '.cmap'
                self.curCharacterizeCmaps.append(outputTarget + '.cmap')
            cargs += ['-merge', '-o', outputTarget, '-f']
            if self.stdoutlog :
                cargs.extend( ['-stdout', '-stderr'] )
            jobName = 'mrg_%s_%d' % (self.outputContigPrefix,i)
            if log :
                outpre = os.path.join(self.logdir, "log_merge_"+self.stageComplete)
                sJob = mthread.singleJob(cargs, jobName, expectedResultFile, jobName, stdOutFile=outpre+"stdout.log", stdErrOutFile=outpre+"stderr.log", clusterLogDir=self.clusterLogDir)
            else :
                sJob = mthread.singleJob(cargs, jobName, expectedResultFile, jobName, clusterLogDir=self.clusterLogDir)            
            mergeJobSet.addJob(sJob)
        if self.executeCurrentStage:
            mergeJobSet.multiThreadRunJobs(1) # run locally
            mergeJobSet.doAllPipeReport()
        if os.path.exists(keyResult):
            self.latestMergedCmap = keyResult
        else:
            self.latestMergedCmap = None
            #self.curContigCount = 0
        
    def findContigs(self,contigFolder, searchString, txtOutput=None):
        """Using string match, find phase-specific contigs
        
        Output is list of load balanced cmap list
        Optional output is load balanced cmap files or single cmap files
        """
        
        fileList = os.listdir(contigFolder)
        searchString += '_contig'
        contigFiles = []
        contigIDs = []
        fileSizes = []
        for fileName in fileList:
            match = fileName.find(searchString)
            if match == -1:
                continue
            prefixEnd = match + len(searchString)
            targetFile = os.path.join(contigFolder, fileName)
            statRes = os.stat(targetFile)
            if statRes.st_size == 0:
                continue
            suffixStart = fileName.find('.cmap')
            cmapString = '.cmap'
            if fileName.endswith(cmapString):
                suffixStart = len(fileName) - len(cmapString)
                #cmapRefinedString = '_refined.cmap'
                #if fileName.endswith(cmapRefinedString):
                #    suffixStart = fileName.__len__() - cmapRefinedString.__len__()
            else:
                continue
            match = fileName.find('condensed')
            if match != -1:
                continue
            contigNumber = fileName[prefixEnd:suffixStart]
            if any([x.isalpha() for x in contigNumber]):
                continue
            contigFiles.append(targetFile)
            contigIDs.append(contigNumber)
            fileSizes.append(statRes.st_size)
        
        if not(txtOutput):
            return contigFiles, contigIDs

        f1 = open(txtOutput, 'wb')
        for contigFile in contigFiles:
            f1.write(contigFile + '\n')
        self.curCharacterizeTargets = [txtOutput]
        
        # Split up the Cmaps for Characterization
        nBins = min(self.numCharacterizeJobs, contigFiles.__len__())
        if self.numCharacterizeJobs > 1 and self.ref:
            self.curCharacterizeTargets = []
            binnedContigFiles = BinContigsBySize(contigFiles, fileSizes, nBins)
            for i,ContigGroup in enumerate(binnedContigFiles):
                partialTxtOutput = txtOutput + '_%d' % (i+1)
                f1 = open(partialTxtOutput, 'wb')
                for contigFile in ContigGroup:
                    f1.write(contigFile + '\n')
                f1.close()
                self.curCharacterizeTargets.append(partialTxtOutput)
        return contigFiles, contigIDs
        
    def updatePipeReport(self, content, printalso=True):
        if printalso :
            print content, #no extra newline
        f1 = open(self.pipeReportFile, 'a')
        f1.write(content) 
        f1.close()
    
    def updateInfoReport(self, content, printalso=False): #default printalso False, unlike PipeReport
        if printalso :
            print content, #no extra newline
        f1 = open(self.infoReportFile, 'a')
        f1.write(content) 
        f1.close()

    #if not ismerge, we want to exit. If it is, we don't, ie, for bacteria which merge to single contig.
    def exitTestMinimumContigs(self, minCount, ismerge=False):
        '''Set pipeline exit condition if contigs too few to continue
        
        '''
        #for ngsBypass, in addition to skipping the stages themselves (in toExecuteCurrentStage),
        # also skip this check (min contigs) because there need not be any results of assembly and refineA/B
        if self.ngsBypass and self.currentStageCount <= self.ngsBypass :
            return 0
        if self.curContigCount <= minCount:
            if not ismerge :
                exitString = '\n %s Contig count %d <= %s Min %d, exiting %s \n' % (10*'*', self.curContigCount, self.stageComplete, minCount, 10*'*')
                util.LogError("critical", "stage did not produce minimum number of contigs")
            else :
                exitString = '\n %s Contig count %d <= %s Min %d, skipping to final refine %s \n' % (10*'*', self.curContigCount, self.stageComplete, minCount, 10*'*')
            self.updateInfoReport(exitString)
            print exitString
            return 1
        else : 
            #print "exitTestMinimumContigs:", self.stageComplete, ":", self.curContigCount #debug
            pass
        return 0


    def runJobs(self, module, name) :
        '''varsPipeline.runJobs: wrapper to module.runJobs for extension to celery job submission.
        Note: all modules _must_ have the runJobs method implemented.
        For normal (SGE or local) processing, runJobs will just use the jobWrapper class in
        Multithreading.py.
        '''
        if not self.workorder : #default (SGE, IrysView, local)
            module.runJobs()
        else : #celery ONLY
            from opensgi.cloud.api.tasks import runJobGroup
            runJobGroup(self.workorder, name, module) # Celery Specific


    # targetFile is a list of bnxfiles which are in outputList
    # this is used here for SampleCharModule, and in PairwiseModule
    # this was previously a standalone fn called writeIntermediate
    def writeListToFile(self, outputList, targetFile):
        f1 = open(targetFile, 'w')
        for val in outputList: 
            writeVal = os.path.abspath(val)
            f1.write(writeVal + '\n')
        f1.close()   

#end class varsPipeline



## Execution of De Novo assembly workflow
#
# Flow is determined by varsPipeline class

class DNPipeline():
    """ Execution of De Novo assembly workflow
    
    Flow is determined by varsPipeline class
    Phase execution follow the following convention:
        - Instantiate Phase Class
        - Populate Phase Jobs - hardcoded + varsPipeline
        - Run Phase Jobs
        - Log Phase Data: computation data and informatics data
    """

    def __init__(self):
        pass
    

    def constructData(self, varsP):
        """Build bnx files from raw image data
        
        Optional: Single molecule noise characterization
        Cluster capability may be unstable.
        """
        # Perform Image Processing:
        varsP.toExecuteCurrentStage(quiet=True)
        if not varsP.bnxFileIn :
            #new return of this is 1 for failure, False or None for success
            if ipm.performImageAnalysis(varsP, bypass=not(varsP.executeCurrentStage)) :
                #print '  No experiments found, check existance of image target locations'
                #move printing/logging into performImageAnalysis
                return 1
            #writeIntermediate(bnxFiles, varsP.bnxTarget) #moved into joinBnxFiles in performImageAnalysis
        
        # Sample Characterization
        
        if varsP.toExecuteCurrentStage(quiet=True) and varsP.doNoise:
            stageSC = scm.SampleChar(varsP)
            varsP.runJobs(stageSC, "SampleChar")
            stageSC.checkResults() 

        #move this to part of performImageAnalysis since it's only done there anyway
        #if not varsP.bnxFileIn :
        #    varsP.joinBnxFiles()
        
        if varsP.noiseOnly:
            varsP.updatePipeReport('Finished noise evaluation -noiseonly, exiting\n') #print and log sucessful completion message
            return 1 #like failure in that it causes pipeline to exit
        
        return 0

            
    def run(self, varsP):
        """Main pipeline method
        
        Executes pipeline operations prescribed by varsPipeline class
        Method is mirrored in tasks.py, the celery/django specific module
        """ 
        os.putenv('O64_OMP_SET_AFFINITY', 'false')  
        
        varsP.prerunLog() #general information in log

        if self.constructData(varsP): #returns 0 on successful completion, 1 on failure or exit after sample char
            return
        
        varsP.subSampleBnx()
            
        # Perform Pairwise Comparison:
        #if varsP.hashNumBnxPartial > 1:
        #    stageHashBnxSplit = pm.RAHashBnxSplit(varsP)
        util.LogStatus("progress", "stage_start", "Pairwise")
        
        stagePW = pm.Pairwise(varsP)
        if varsP.toExecuteCurrentStage():
            #if varsP.hashNumBnxPartial > 1:
            #    stageHashBnxSplit.runJobs()
            #    stageHashBnxSplit.checkResults()
            #stagePW.runJobs()               # Single Node or Grid Engine
            varsP.runJobs(stagePW, "Pairwise")
        if stagePW.checkResults() : # Update report here -- returns 1 if all align files are missing
            return
            
        util.LogStatus("progress", "stage_complete", "Pairwise")
        
        util.LogStatus("progress", "stage_start", "Assembly")
        # Denovo Assembly
        stageASSMBL = am.Assemble(varsP)
        if varsP.toExecuteCurrentStage():
            #stageASSMBL.runJobs()
            varsP.runJobs(stageASSMBL, "Assembly")
        stageASSMBL.checkResults()
        if varsP.exitTestMinimumContigs(0):
            return
        self.runCharacterize(varsP)
        util.LogStatus("progress", "stage_complete", "Assembly")
        
        # Contig Refine A
        util.LogStatus("progress", "stage_start", "RefineA")
        if varsP.groupContigs:
		stageRFNA = grprefmod.Refine("refineA", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNA.runJobs()
                    varsP.runJobs(stageRFNA, "refineA")
                    stageRFNA.checkResults()
		if varsP.exitTestMinimumContigs(0):
                    return
	else:
		stageRFNA = refmod.Refine("refineA", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNA.runJobs()
                    varsP.runJobs(stageRFNA, "refineA")
                    stageRFNA.checkResults()
		if varsP.exitTestMinimumContigs(0):
                    return
        util.LogStatus("progress", "stage_complete", "RefineA")
        
        util.LogStatus("progress", "stage_start", "RefineB")
        # Contig Refine B
        if varsP.groupContigs:
		stageRFNB = grprefmod.Refine("refineB0", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNB.runJobs()
                    varsP.runJobs(stageRFNB, "refineB0")
                    stageRFNB.checkResults()

		stageRFNB = grprefmod.Refine("refineB1", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNB.runJobs()
                    varsP.runJobs(stageRFNB, "refineB1")
                    stageRFNB.checkResults()
		if varsP.exitTestMinimumContigs(0):
                    return 
		self.runCharacterize(varsP)
	else :
		stageRFNB = refmod.Refine("refineB", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNB.runJobs()
                    varsP.runJobs(stageRFNB, "refineB")
                    stageRFNB.checkResults()
		if varsP.exitTestMinimumContigs(0):
                    return 
		self.runCharacterize(varsP)
        util.LogStatus("progress", "stage_complete", "RefineB")

        if varsP.runSV > 1 : #runSV: for now, anything > 1 is refineB
	    util.LogStatus("progress", "stage_start", "SVrefineB")
            stageSV = svm.SVdetect(varsP) 
            #stageSV.runJobs()
            varsP.runJobs(stageSV, "SVrefineB")
            stageSV.checkResults()
	    util.LogStatus("progress", "stage_complete", "SVrefineB")

        # NGS Contig Refine
        #because toExecuteCurrentStage is inside if ngsInDir, the current counting of stages is preserved when not using this
        if varsP.ngsInDir :
            stageRFNngs = refmod.Refine("refineNGS", varsP) #new class requires first arg string
            if varsP.toExecuteCurrentStage():
                #stageRFNngs.runJobs()
                varsP.runJobs(stageRFNngs, "refineNGS")
            stageRFNngs.checkResults()
            #not sure about exitTestMinimumContigs -- may want to continue if no ngs but have denovo
            self.runCharacterize(varsP) #see how your refined contigs compare to unrefined vis-a-vis reference

        util.LogStatus("progress", "stage_start", "Merge0")
	varsP.toExecuteCurrentStage()
	stageMRG = refmod.Merge(varsP)
	for j in range(26):
		stageMRG.generateJobList()
		if varsP.executeCurrentStage:
			#stageMRG.runJobs()
			varsP.runJobs(stageMRG, "merge_0")
		if stageMRG.mergeComplete():
			break
	self.runCharacterize(varsP)
        if varsP.exitTestMinimumContigs(1, ismerge=True):
            return 0
        util.LogStatus("progress", "stage_complete", "Merge0")

        # Iterative Extension and Merging
        for i in range(varsP.nExtensionIter):
	    util.LogStatus("progress", "stage_start", "extension%d" % (i+1))
	    if varsP.groupContigs:
		stageEXT = grprefmod.Refine("extension0", varsP)
		if varsP.toExecuteCurrentStage():
                    #stageEXT.runJobs()
                    varsP.runJobs(stageEXT, "extension0_%i"%i)
                    stageEXT.checkResults()
			
		stageEXT = grprefmod.Refine("extension1", varsP)
		if varsP.toExecuteCurrentStage():
                    #stageEXT.runJobs()
                    varsP.runJobs(stageEXT, "extension1_%i"%i)
                    stageEXT.checkResults()
		self.runCharacterize(varsP)
		if varsP.exitTestMinimumContigs(1, ismerge=True):
                    break

	    else :
		stageEXT = refmod.Extension(varsP)
		if varsP.toExecuteCurrentStage():
                    #stageEXT.runJobs()
                    varsP.runJobs(stageEXT, "extension_%i"%i)
		stageEXT.checkResults()
		self.runCharacterize(varsP)
		if varsP.exitTestMinimumContigs(1, ismerge=True): #no point in merging single contig, ismerge just changes printout
                    break #do final refinement anyway
	    util.LogStatus("progress", "stage_complete", "extension%d" % (i+1))
			
	    util.LogStatus("progress", "stage_start", "Merge%d" % (i+1))
            varsP.toExecuteCurrentStage()
            stageMRG = refmod.Merge(varsP)
            for j in range(26):
                stageMRG.generateJobList()
                if varsP.executeCurrentStage:
                    #stageMRG.runJobs()
                    varsP.runJobs(stageMRG, "merge_%i"%i)
                if stageMRG.mergeComplete():
                    break
            self.runCharacterize(varsP)
	    util.LogStatus("progress", "stage_complete", "Merge%d" % (i+1))
            if varsP.exitTestMinimumContigs(1, ismerge=True):
                break #do final refinement anyway
        
        # done with extend - merge; do final refinement
        #  if no extend-merge, previous stage was refineB, so no further refinement is necessary
	util.LogStatus("progress", "stage_start", "refineFinal")
        if not varsP.skipFinalRefine and varsP.nExtensionIter > 0 :
	    if varsP.groupContigs:	
		stageRFNfinal = grprefmod.Refine("refineFinal0", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNfinal.runJobs()
                    varsP.runJobs(stageRFNfinal, "refineFinal0")
                    stageRFNfinal.checkResults()
		
		stageRFNfinal = grprefmod.Refine("refineFinal1", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNfinal.runJobs()
                    varsP.runJobs(stageRFNfinal, "refineFinal1")
                    stageRFNfinal.checkResults()
		if varsP.exitTestMinimumContigs(0):
                    return 
		self.runCharacterize(varsP)
	    else :
		stageRFNfinal = refmod.Refine("refineFinal", varsP) #new class requires first arg string
		if varsP.toExecuteCurrentStage():
                    #stageRFNfinal.runJobs()
                    varsP.runJobs(stageRFNfinal, "refineFinal")
                    stageRFNfinal.checkResults()
		if varsP.exitTestMinimumContigs(0):
                    return 
		self.runCharacterize(varsP)
	util.LogStatus("progress", "stage_complete", "refineFinal")

        #runSV: only if previous stage was not SVdetect
        if varsP.runSV and (varsP.ngsInDir or varsP.nExtensionIter or not varsP.skipFinalRefine) : 
            util.LogStatus("progress", "stage_start", "SV_analysis")
            stageSV = svm.SVdetect(varsP)
            #stageSV.runJobs() 
            varsP.runJobs(stageSV, "SVfinal") 
            stageSV.checkResults()
            util.LogStatus("progress", "stage_complete", "SV_analysis")

	util.LogStatus("progress", "stage_start", "AsyncWait")
	mthread.wait_all_threads() #ensure start_new_thread jobs (Characterize) finished
	util.LogStatus("progress", "stage_complete", "AsyncWait")

        varsP.FinalCleanup()

        elp = time.time() - varsP.startTime #elapsed time in seconds
        varsP.updatePipeReport( "  Pipeline end time: %s\n  Elapsed time: %.0fs; %.2fh; %.2fd\n\n" % (time.ctime(), elp, elp/3600., elp/3600./24) )

        if util.SummarizeErrors(varsP=varsP)==0:
		varsP.updatePipeReport("Pipeline has successfully completed\n") #print and log sucessful completion message
		util.LogStatus("progress", "pipeline", "success")
	else:
		varsP.updatePipeReport("Pipeline has completed with errors\n") #print and log sucessful completion message
		util.LogStatus("progress", "pipeline", "failure")
    #end run(self, varsP)

    
    def runCharacterize(self, varsP):
        if varsP.executeCurrentStage:
            stageCHAR = cm.Characterize(copy.deepcopy(varsP))
            mthread.start_new_thread(self.doRunCharacterize, (stageCHAR, varsP))
	return
	    
    
    def doRunCharacterize(self, stageCHAR, varsP):
        """Compare resulting contigs to reference; get similarity statistics
        
        Results published to informaticsReport file
        """
        #stageCHAR.runJobs()
        varsP.runJobs(stageCHAR, "Characterize") #here, celery won't know what stage this is, but that's ok
        stageCHAR.checkResults()


    
def writeToFile(fileName, content):
    f1 = open(fileName, 'w')
    f1.write(content) 
    f1.close()
    
#def appendToFile(fileName, content):
#    f1 = open(fileName, 'a')
#    f1.write(content) 
#    f1.close()


#This has been moved to a method of varsPipeline and renamed writeListToFile
#def writeIntermediate(outputList, targetFile):


def BinContigsBySize(ContigFiles, FileSizes, nBins):
    sortDict = {}
    for i,ContigFile in enumerate(ContigFiles):
        FileSize = FileSizes[i]
        if sortDict.has_key(FileSize):
            sortDict[FileSize].append(ContigFile)
        else:
            sortDict[FileSize] = [ContigFile]
    SortedFileSizes = sorted(sortDict.keys(), reverse = True)
    
    binnedContigFiles = []
    ct = 0
    for FileSize in SortedFileSizes:
        CurContigFiles = sortDict[FileSize]
        for CurContigFile in CurContigFiles:
            if ct/nBins == 0:
                binnedContigFiles.append([CurContigFile])
            else:
                binnedContigFiles[ct%nBins].append(CurContigFile)
            ct += 1
    return binnedContigFiles
    
    
