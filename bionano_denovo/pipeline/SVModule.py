import os
import shutil
from collections import defaultdict

import Multithreading as mthread
import mapClasses as mc

"""@package SVModule 
   Run structural variation (SV) detection on a given set of cmaps.
   Copy cmaps to new dir (for convenience of having input and output in same dir).
   Run SV detection using RefAligner to produce smaps.
   Report on results in informaticsReport. Merge smaps into bed files.

"""


import utilities as util
util.setVersion("$Id: SVModule.py 2484 2014-02-12 23:00:30Z wandrews $")



class SVdetect(mthread.jobWrapper):
    """SVdetect: main class for SVModule
    This should be called immediately after the stage for which you want SVs detected.
    """
    def __init__(self, varsP): #targetStage, 
        """SVdetect.__init__: instantiating this class will also copy the cmaps to run detect on and generate its jobs.
        """
        #the stage to run on is obtained from self.varsP.outputContigPrefix
        #self.targetStage = targetStage #not necessary
        self.varsP = varsP
        self.moduleName = "svdetect" #used as arg to varsP.argsListed, varsP.getClusterArgs -- this shouldn't change
        self.verbose = 1

        #configs for smapToBed
        self.smapTypeIdx = 10 #this is the column of the 'type' string in the smap--used to be last, now 10 due to indices at end
        self.smapExcludeTypes = ["complex", "end", "inversion"]
        self.svcolors = {}
        self.svcolors["insertion"] = "0,128,0" #green
        self.svcolors["deletion" ] = "255,0,0" #red
        self.svcolors["default"  ] = "0,0,128" #blue, presumably--use a default to avoid crashes if names change/are old

        #relies on outputContigPrefix: this is why you need to call this immediately after the stage for which you want SVs detected.
        newContigPrefix = self.varsP.outputContigPrefix + "_sv" 
        self.stageName = self.moduleName+"_"+newContigPrefix

        #setup for prepareContigIO:
        #specifically, the line: "self.inputContigPrefix = self.outputContigPrefix"
        #should just reset self.outputContigPrefix--store it here; see runJobs
        self.oldOutputContigPrefix = self.varsP.outputContigPrefix

        #these are the cmaps that need to be copied to the target dir, ie, varsP.outputContigFolder
        self.sourceContigFolder = self.varsP.outputContigFolder 

        self.varsP.prepareContigIO(newContigPrefix)

        super(SVdetect, self).__init__(self.varsP, self.stageName, clusterArgs=self.varsP.getClusterArgs(self.moduleName))

        #set dir for merged smap/bed files; will create at smap merge stage--see self.mergeSmaps()
        self.mergesmapdir = os.path.join(self.varsP.outputContigFolder, "merged_smaps")

        self.copyTargetCmaps()
        self.generateJobList()
    #end __init__


    #using data members made in constructor (ie, i/o dirs),
    # find the cmaps which should be copied, and copy them
    # note: part of this is very similar to utilities.getListOfCmapsFromDir, but it needs more functionality
    #also, make list of contigs to run on to save reading from disk again in generateJobList
    def copyTargetCmaps(self) :
        """SVdetect.copyTargetCmaps: copy the cmaps which are compared to the reference for 
        SV detection to a new folder.
        """
        if self.verbose :
            outstr =  "SVdetect: Copying cmaps from: %s\n" % self.sourceContigFolder
            outstr += "SVdetect: To output folder: %s" % self.varsP.outputContigFolder #this should be where the contigs will go
            self.varsP.updatePipeReport(outstr+"\n\n")

        self.targetCmapList = []
        for ifile in os.listdir(self.sourceContigFolder) :
            #define which files to skip:
            #any file which doesn't end in ".cmap"
            if not ifile.endswith(".cmap") :
                continue
            #any map for which the prefix is in all caps--these are the combined
            pref = ifile[:ifile.find(".")]
            if pref.upper() == pref :
                continue
            #any file (cmap) which ends with "_q" or "_r"
            if ifile.endswith("_q.cmap") or ifile.endswith("_r.cmap") :
                continue

            #this should be a cmap to be processed--copy
            shutil.copy(os.path.join(self.sourceContigFolder,ifile), self.varsP.outputContigFolder)
            #store in list for further processing
            self.targetCmapList.append( os.path.join(self.varsP.outputContigFolder, ifile) )
    #end copyTargetCmaps


    #use addJob (inherited from Multithreading.jobWrapper) for each contig in self.targetCmapList
    def generateJobList(self) :
        """SVdetect.generateJobList: create RefAligner jobs for each contig to detect SVs on.
        """
        #remember, -sv is pairwise, which is only implemented for pairwise, so even the reference _must_ be a -i argument
        #validity of reference should have been checked in varsP.checkInputs() at pipeline startup
        baseargs =  [self.varsP.RefAlignerBin, '-i', self.varsP.ref]
        baseargs += ['-maxthreads', str(self.varsP.maxthreads)]
        endargs = self.varsP.argsListed(self.moduleName)
        self.smapList = [] #store all smap paths for later processing in checkResults

        #example: RefAligner -i HG_b19_merged_bspq_condensed29.cmap -i exp_mrg5B_contig1.cmap -o exp_mrg5B_contig1 -f -sv 1 -sf 0.2 -maxthreads 64

        for idx,cmappath in enumerate(self.targetCmapList) :
            cbase, cname = os.path.split(cmappath)
            cname = cname[:cname.find(".")] #strip suffix
            jobName = "SV_detect__"+cname
            outputfile = os.path.join(cbase,cname)
            jobargs = ["-i", cmappath, "-o", outputfile ]
            stdoutf = None
            if self.varsP.stdoutlog :
                jobargs.extend( ['-stdout', '-stderr'] )
                stdoutf = outputfile+".stdout"
            if idx != 0 : #for all but the first job
                jobargs.extend( ['-output-veto-filter', '_r.cmap$'] ) #do not output the _r.cmap
            expectedResultFile = outputfile+".smap" #smap is SV map (could also just check xmap)
            self.smapList.append(expectedResultFile)
            #expectedOutputString = cname #afaik, this is only used for cluster args -- also used in report: set same as jobName
            s1Job = mthread.singleJob(baseargs+jobargs+endargs, 
                                      jobName, 
                                      expectedResultFile, 
                                      jobName, #expectedOutputString,
                                      maxThreads=self.varsP.maxthreads,
                                      clusterLogDir=self.varsP.clusterLogDir,
                                      expectedStdoutFile=stdoutf
                                      )
            self.addJob(s1Job)

        self.logArguments()
    #end generateJobList


    #for big bear compatibility, this is only allowed to call multiThreadRunJobs
    def runJobs(self):
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)


    def checkResults(self):
        """SVdetect: record results of sv calls in pipelineReport and informaticsReport
        Also merge smaps and create merged bed file.
        """        
        self.doAllPipeReport() #see Multithreading.jobWrapper -- this is the pipelineReport
        self.checkSmapResults() #add SV info to informatics report
        if not self.mergeSmaps() : #returns None for success, 1 for fail
            self.smapToBed() #don't do this if merge fails
        #fix the varsP data members needed for prepareContigIO for next stage
        self.varsP.outputContigPrefix = self.oldOutputContigPrefix


    #open each smap, analyze results for SVs
    #info to get: same as in run_sv:
    #dict of sv type to number of occurrances
    #sv rate: number of svs over number of maps
    #detailedreport will also print the length of the maps, the total aligned length, and the number of SVs
    # _for each map_. This can be large for human.
    def checkSmapResults(self, detailedreport=True) :
        """SVdetect: loop on all maps produced by all sv calls, collect stats
        """
        mapunit = 1e6 #print map lens in Mb
        #instead of hardcoding the types, don't make any assumptions about them, and use a default dict
        #self.svtypes = ["insert", "delete", "translocation", "rearrangement", "end"] #currently, 5 types
        svtypeslen = "13" #for printing
        #svtype_dict = dict.fromkeys(self.svtypes, 0) #first arg is list which becomes keys, second is default
        svtype_dict = defaultdict(int)

        #this is a warning only
        # if happens, proceed using len(smapList) as number of contigs so as to not penalize the sv rate based on failed jobs
        if len(self.smapList) != len(self.jobList) :
            self.varsP.updatePipeReport("Warning in SVModule: number of jobs not equal to number of smaps\n\n")

        #check presence of each smap prior to loop below because if it doesn't exist, there's not much point
        # and it's a bit misleading to have a 0 entry in the summary when the job failed becuase 0 looks
        # like no alignment
        # do this inside loop below
        self.useSmapList = [] #will contain only those smaps found on disk

        outstr = "SV detect: %s\n" % self.stageName
        outstr += "map filename : len (Mb) : align len (Mb) (ratio) : N SV\n" #header for below
        maxpathlen = 0
        nsvtot = 0
        for smappath in self.smapList :
            svdict = self.checksmap(smappath) #new fn returns dict of svtype : number
            if svdict == None : #empty dict is no SV, None is no file
                continue
            self.useSmapList.append(smappath)
            util.addDictToDict(svtype_dict, svdict)
            nsv = (sum(svdict.values()) if svdict else 0)
            nsvtot += nsv
            if detailedreport :
                smapfile = os.path.split(smappath)[1]
                if len(smapfile) > maxpathlen :
                    maxpathlen = len(smapfile)
                cmaplen = mc.cmap(smappath.replace(".smap",".cmap"), lengthonly=True).length/mapunit
                alignlen = self.checkxmap(smappath.replace(".smap",".xmap"), mapunit) #just for kicks
                #print in Mb (mapunit) so use 3 decimal places (6.3)
                arat = (alignlen/cmaplen if cmaplen > 0 else 0)
                outstr += ("%-"+str(maxpathlen)+"s : %6.3f : %6.3f (%5.2f) : %i\n") % (smapfile, cmaplen, alignlen, arat, nsv)
        #end loop on smaps

        #nsvtot   = sum(nsv_dict.values()) #redundant with above--could do a dummy check, but not necessary
        #outstr = "N sv dict:" + str(nsv_dict) + "\n" #this is in the table below
        ncontigs = len(self.smapList)
        rate     = (nsvtot/float(ncontigs) if ncontigs > 0 else 0)
        outstr += ("%"+svtypeslen+"s :   N : N/tot\n") % "SV type"
        for key in sorted(svtype_dict.keys()) : #for uniformity of output
            rat = (svtype_dict[key]/float(nsvtot) if nsvtot else 0)
            outstr += ("%"+svtypeslen+"s : %3i : %.4f\n") % (key, svtype_dict[key], rat)
        outstr += "N contigs: %6i\n" % ncontigs
        outstr += "N sv tot : %6i\n" % nsvtot
        outstr += "SV rate  : %.4f\n" % rate
        #outstr += "%5i  %4i  %6.4f" % (ncontigs, nsvtot, rate) #above 3 on one line--redundant

        self.varsP.updateInfoReport(outstr + '\n')
    #end checkSmapResults


    #check if svs found: 
    #return dict of sv type to number of SVs, where type is read from the smap
    # empty dict if no SVs; return None if no file found to avoid further errors
    def checksmap(self, usefile) :
        """SVdetect.checksmap: open smap produced by sv call, get stats on SVs detected
        """
        if not util.checkFile(usefile, ".smap") :
            self.varsP.updatePipeReport("Warning in checksmap: File missing or not smap: "+usefile+"\n")
            return None #None means no file found; empty dict means no SVs

        svdict = defaultdict(int) #int will default to 0, conveniently
        for line in open(usefile) :
            if line[0] == "#" :
                continue
            thissvtype = line.split()[self.smapTypeIdx] #see __init__
            #no longer any assumption about types string, just accept all into defaultdict
            #if not thissvtype in self.svtypes :
            #    self.varsP.updatePipeReport("Warning in checksmap: Invalid sv type (%s) in file %s, skipping\n\n" % (thissvtype, usefile))
            #    continue
            svdict[thissvtype] += 1
        
        return svdict
    #end checksmap

    #check if any alignment, if so, return total length
    #treat mapunit as denominator, ie, 1e6 for Mb
    def checkxmap(self, usefile, mapunit=1. ) :
        """SVdetect.checkxmap: open xmap produced by sv call, get total aligned length
        """
        if not util.checkFile(usefile, ".xmap") : #os.path.isfile(usefile) :
            outstr = "Warning in checkxmap: File missing: " + usefile
            self.varsP.updatePipeReport(outstr+"\n")
            return 0

        return mc.xmap(usefile).getSumMappedQryLen(unitscale=mapunit)
    #end checkxmap

    #this opens all the smaps again even though they were all opened already in checkSmapResults
    def mergeSmaps(self) :
        """SVdetect.mergeSmaps: create one file with all smaps merged
        """
        
        if not util.checkDir(self.mergesmapdir) : #set in self.__init__; makes this dir
            self.varsP.updatePipeReport("ERROR in SVModule.mergeSmaps: unable to create merged smap dir %s\n" % self.mergesmapdir)
            return

        if not self.useSmapList : #if empty, no smaps, nothing to merge
            self.varsP.updatePipeReport("SVdetect: no smaps found, skipping merge\n")
            return 1 #return 1 for fail

        self.varsP.updatePipeReport("SVdetect: merging smaps to %s\n" % self.mergesmapdir)
        # now, find and merge all smaps in usedir
        nmaps = 0
        nsv = 0
        firstmap = True #copy header of first map
        mergedoutput = "" #this is the smap
        for smappath in self.useSmapList : #contains only smaps on disk; see checkSmapResults
            nmaps += 1
            # open smap, read it
            #usepath = os.path.join(smapdir, sfile) #not necessary; already have full path
            usefile = open(smappath)
            for line in usefile :
                if line[0] != "#" or firstmap :
                    mergedoutput += line
                if line[0] != "#" :
                    nsv += 1
            # end for line in smap
            usefile.close()
            if firstmap : #the first map has been processed
                firstmap = False
        # end for smaps in usedir
        self.varsP.updatePipeReport("SVdetect: merging smaps: found %i Maps with %s SVs\n" % (nmaps, nsv))
        outfile = os.path.split(smappath)[1]
        outfile = outfile[:outfile.rfind("_")]
        outfile = os.path.join(self.mergesmapdir, outfile+"_merged.smap")
        self.varsP.updatePipeReport("SVdetect: Writing merged smap: %s\n" % outfile)
        outf = open(outfile, "w+")
        outf.write(mergedoutput)
        outf.close()
        self.mergedsmap = outfile #for use in smapToBed
    #end mergeSmaps

    # How to convert an smap to a bed:
    #  First column is chromosome number--this is same as ref contig id. 
    #  Second and third are start and stop position--also same as ref start and ref stop. 
    #  Third is type.
    #  Next four are always ["-1", "+", "0", "0"] for score, top/bottom, and highlighted start/stop
    #  Last is rgb color.
    # excludetypes is the types of svs to exclude--see main
    # output is put in same path as smappath, with .smap replaced by .bed
    # also produce query bed file--only difference is contig id is query contig id, 
    #  and positions are query positions
    #  and filename has _query
    def smapToBed(self, makequerybed=True) :
        smappath = self.mergedsmap #convenience
        outpath = smappath.replace(".smap", ".bed")
        #if os.path.exists(outpath): #forget this--just overwrite it
        #    self.varsP.updatePipeReport("ERROR in SVdetect: Output bed file already exists: %s\n" % outpath)
        #    return
        if makequerybed :
            qoutpath = smappath.replace(".smap", "_query.bed")
            #assert not os.path.exists(qoutpath), "Output bed file already exists:"+outpath #forget existence check
        ws = "\t" #Irysview requires tab separated

        self.varsP.updatePipeReport("SVdetect: Converting smap %s to bed\n" % smappath)
        f1 = open(smappath)
        bedstr = "" #this will be the bed file
        qbedstr = "" #query bed (stays empty if not makequerybed)
        for line in f1 :
            if line[0] == "#" :
                continue
            tokens = line.split()
            svtype = tokens[self.smapTypeIdx] #see __init__
            if svtype in self.smapExcludeTypes :
                continue
            # 0 : SmapEntryID #1 : QryContigID
            ref = tokens[2] #RefcontigID1
            # 3 : RefcontigID2 #4-5 : QryStartPos - QryEndPos
            refstart = int(float(tokens[6])) #RefStartPos
            refstop  = int(float(tokens[7])) #RefEndPos
            # prevent extra space -- no numerals for %
            bedstr += ("%-s"+ws+"%i"+ws+"%i"+ws+"%s"+ws+"-1"+ws+"+"+ws+"0"+ws+"0"+ws+"%s\n") % (ref, refstart, refstop, svtype, self.getSVcolor(svtype))
            if makequerybed :
                qry = tokens[1]
                qrystart = int(float(tokens[4])) #QryStartPos
                qrystop  = int(float(tokens[5])) #QryEndPos
                qbedstr += ("%-s"+ws+"%i"+ws+"%i"+ws+"%s"+ws+"-1"+ws+"+"+ws+"0"+ws+"0"+ws+"%s\n") % (qry, qrystart, qrystop, svtype, self.getSVcolor(svtype))
        f1.close()

        # write output file
        self.varsP.updatePipeReport("SVdetect: writing bed file %s\n" % outpath)
        outfile = open(outpath, "w+")
        outfile.write(bedstr)
        outfile.close

        if makequerybed :
            self.varsP.updatePipeReport("SVdetect: writing bed file %s\n" % qoutpath)
            outfile = open(qoutpath, "w+")
            outfile.write(qbedstr)
            outfile.close
        self.varsP.updatePipeReport("\n")
    #end smapToBed

    def getSVcolor(self, svtype) :        
        return self.svcolors[svtype] if self.svcolors.has_key(svtype) else self.svcolors["default"]

#end class SVdetect(mthread.jobWrapper):
