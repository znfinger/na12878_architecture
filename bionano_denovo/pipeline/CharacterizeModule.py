
import os, math
import pdb

import mapClasses
import MapClassesRev
import RefinementModule as rm
import Multithreading as mthread
import utilities as util

"""
@package CharacterizeModule 
Get general stats and mapping stats (if reference) for contigs

"""


util.setVersion("$Id: CharacterizeModule.py 2481 2014-02-12 22:52:01Z wandrews $")


class Characterize(mthread.jobWrapper):
    """Define jobs for characterization of contigs
    
    """
    def __init__(self, varsP):
        self.varsP = varsP
        mthread.jobWrapper.__init__(self, varsP, 'Characterize ' + self.varsP.stageComplete,clusterArgs=varsP.getClusterArgs('characterizeStrict'))
        self.xmapTarget = None
        self.curCharacterizeFileRoots = []
        varsP.contigAlignTarget = os.path.join(varsP.outputContigFolder, 'alignref')
        if not(os.path.exists(varsP.contigAlignTarget)):
            os.mkdir(varsP.contigAlignTarget)
        self.generateJobList()
        
    def generateJobList(self,strictness=-1):
        if not self.varsP.ref : #no jobs if no ref
            return
        jobargs = [self.varsP.RefAlignerBin, '-ref', self.varsP.ref]
        strictstr = "" 
        if strictness == 0 and self.varsP.argData.has_key('characterizeStrict') : #0
            strictstr = "Strict"
            opta = self.varsP.argsListed('characterizeStrict')
        elif strictness == -1 and self.varsP.argData.has_key('characterizeDefault') : # don't use nominal default
            strictstr = "Default"
            opta = self.varsP.argsListed('characterizeDefault')
        elif strictness == 1 and self.varsP.argData.has_key('characterizeDefault') : #extend (on default) -- make this default
            strictstr = "Default Extend"
            opta = self.varsP.argsListed('characterizeDefault') + ['-extend', '1']
        
        for i, curCharacterizeCmap in enumerate(self.varsP.curCharacterizeCmaps):
            if self.varsP.numCharacterizeJobs == 1:
                jobName = 'Char_%s' % self.varsP.stageComplete
            else:
                jobName = 'Char_%s_%d' % (self.varsP.stageComplete, i+1)
            outFileName = os.path.split(curCharacterizeCmap)[-1].replace(".cmap", "")
            outfile = os.path.join(self.varsP.contigAlignTarget,outFileName)
            self.curCharacterizeFileRoots.append(outfile)
            #jobargs.extend(opta)
            expectedResultFile = outfile+".xmap"
            self.xmapTarget = expectedResultFile
            currentArgs = jobargs + ["-i", curCharacterizeCmap, "-o", outfile]
            stdoutf = None
            if self.varsP.stdoutlog :
                currentArgs.extend( ['-stdout', '-stderr'] )
                stdoutf = outfile+".stdout"
            currentArgs += ['-maxthreads', str(self.varsP.nThreads)]
            currentArgs += opta
            s1Job = mthread.singleJob(currentArgs, jobName, expectedResultFile, jobName.replace(' ',''),maxThreads=self.varsP.nThreads,clusterLogDir=self.varsP.clusterLogDir, expectedStdoutFile=stdoutf)
            self.addJob(s1Job)
            if i==0:
                self.logArguments()
        
    def runJobs(self):
        if not self.varsP.ref :
            return
        self.multiThreadRunJobs(self.varsP.nThreads, sleepTime = 0.2)
    
    def checkResults(self):
        if not self.varsP.ref : #still want contig stats
            infoReport = "Skipping Characterize because no reference (-r)\n"
            self.varsP.updatePipeReport(infoReport, printalso=False) #put this in pipereport just as an fyi
            infoReport += 'Stage Complete: %s\n' % self.varsP.stageComplete
            infoReport += MapClassesRev.ContigCharacterizationNoRef(self.varsP)
            self.varsP.updateInfoReport(infoReport + '\n')
            return
        #for sJob in self.jobList:
        #    sJob.CheckIfFileFound()
        #self.pipeReport += self.makeRunReport()
        #self.pipeReport += self.makeParseReport()
        #self.varsP.updatePipeReport(self.pipeReport)
        self.doAllPipeReport()
        infoReport = 'Stage Complete: %s\n' % self.varsP.stageComplete
        infoReport += MapClassesRev.TopLevelCharacterization(self.varsP,self.curCharacterizeFileRoots)
        self.varsP.updateInfoReport(infoReport + '\n')



#code below is old CharacterizeModule code


#deprecated
#after calling alignContigRef with strictness > 0, call this to make dict of contig id : aligned len
def makeStrictAlignDict(contigpaths, aligndir):
    """Depricated: not in use 
    """
    maxcontigs = -1
    #nmultxmap = 0 #; nstrict = 0 #test
    retdict = {}
    for idx, cpath in enumerate(contigpaths) :
        xmappath = aligndir+os.path.split(cpath)[-1].replace(".cmap", ".xmap") #need cmap file name
        #make sure xmappath exists; if not, bail silently
        if not os.path.exists( xmappath ) :
            continue
        xmapobj = mapClasses.xmap(xmappath)
        if len(xmapobj.xmapLookup) == 0 :
            continue
        #lenq = getMappedQryLen(xmapobj) #currently not used
        lenr = getMappedRefLen(xmapobj)

        #contigQry is equivalent to the contigID--this is the key
        #print "adding", xmapobj.xmapLookup[1].contigQry, ":", lenq #debug
        #value is just len of aligned portion--ref for compare with totalignlen
        retdict[xmapobj.xmapLookup[1].contigQry] = lenr 

        #if len(xmapobj.xmapLookup) > 1  : #test--turn off
            #print "strict multi-xmap:", xmapobj.xmapLookup[1].contigQry #test--turn off
            #nmultxmap += 1 #else 0)

        if maxcontigs != -1 and idx >= maxcontigs :
            break

    return retdict


#deprecated
#Put a dictionary for strings for cmap entries for the merged reference
def refIndexToChrStr(index) :
    refchrdict = {} 
    if refchrdict.has_key( index ) :
        return refchrdict[index]
    else :
        return "NA"


#get the mapped length and the alignment parameters from the align to ref call
#mapped, params = getMappedStats(aligndir, cpath)
#return the alignParams object so that it can be used in characterizeContigs
def getMappedErrStats(aligndir, cpath):
    """Depricated: not in use 
    """
    errfile = aligndir+os.path.split(cpath)[-1].replace(".cmap", ".err") #path checking exists in alignparams constructor
    return alignParams(errfile)


    
#if not listcontigs, do not print list of all contigs, ie, summary only
def characterizeContigs(varsP, xmappath=None, listcontigs=False) :
    """Depricated: not in use 
    """
    #print header of table
    unitscale = 1e-6
    dorefalign = bool(xmappath) #i'm never actually calling refaligner here--this is just using xmappath
    dorefidchr = False
    dorefcid = False
    printrange = False
    printsegrms = False
    dochrstr = False
    iscluster = True
    haveref = bool(varsP.ref)

    refcmap = mapClasses.multiCmap() #just in case... 
    aligndir = varsP.contigAlignTarget

    try :
        refcmap = mapClasses.multiCmap(varsP.ref)
        reflen = refcmap.totalLength #note: total length of _all_ contigs
        #in summary table, this is a denominator--make sure it's non-zero, don't bail (still get contig summary)
        if reflen <= 0 :
            #print "Warning in CharacterizeModule.characterizeContigs: bad reflen", reflen, "defaulting to 1" #not necessary
            reflen = 1.
    except:
        reflen = 1.

    outstr = "Contig Characterization:\n"

    if listcontigs and dorefalign :
        outstr += "cID  len"
        outstr += "  Cov"
        if dorefidchr or dorefcid :
            outstr += "  rID" #ref index for either of these
        if dorefidchr :
            outstr += "  rpos"
        outstr += "  alignlen  alignlen/len"
        if printrange :
            outstr += "  Qry1  Qry2  Ref1  Ref2"
        outstr += ("  segRMS" if printsegrms else "")
        outstr += "  Conf  Conf/lenkb"
        outstr += "  FP  FN  sf  sd  bpp" #"  res" #--ditch res (not bpp)
        if dochrstr :
            outstr += "  Chr"
        outstr += "\n"

    totcontiglen = 0; totalignlen = 0; nmapcontigs = 0; defalignlen = 0; totalignqlen = 0
    contiglens = [] #lens of all contigs in bases
    uniqueseg = {} #the argument to util.uniqueRange--stores all the map lengths which go into totalignlen--now each of these is a value, and the keys are the reference contig id or chromosome depending on dorefidchr
    avgfplist = []; avgfnlist = [] #average FP/FN rates
    if dorefidchr :
        chrsum = refcmap.makeChrSummaryDict() #see mapClasses.multiCmap
    for citr, cpath in enumerate([varsP.latestMergedCmap]) : #always use contigpaths
        mapi = mapClasses.multiCmap(cpath) 
        totcontiglen += mapi.totalLength
        contiglens += mapi.getAllMapLengths() #getAllMapLengths is list of all map lengths

        #store a list of the contig ids in this multiCmap, then remove them if they're in the xmap
        # if they're not, print at the end
        mapids = mapi.getAllMapIds() #this is once per cmap, not once per characterizeModule call--becuase it's keys, it's already a copy, no need to copy explicitly
        ncontigs = len(mapids) #this is ncontigs in this file, ie, in mapi (see below)

        xmapobj = mapClasses.xmap() #empty map to fix xmapobj scope
        if dorefalign : #get xmap object
            #xmappath = aligndir+os.path.split(cpath)[-1].replace(".cmap", ".xmap") #need cmap file name
            #xmappath = self.xmapTarget
            #if xmappath exists isn't a file, nothing will be loaded
            if os.path.isfile( xmappath ) : #was if not isfile : continue
                xmapobj = mapClasses.xmap(xmappath)

        for xitr, xmapentry in enumerate(xmapobj.xmapLookup.values()) :
            #print the contig id from the xmap
            #this is sorted by ref position; could sort the list this loop is over by the contigQry data member,
            # _but_, I think I like reference-oriented better because you can see gap spanning

            #get map length from multicmap.getMapLength--returns 0 for any exception
            contiglen = mapi.getMapLength(xmapentry.contigQry)
            if contiglen <= 0 : #this strikes me as clumsy...but I don't want to return non-zero from multiCmap.getMapLength
                contiglen = 1.
            contigcov = mapi.getMapAvgCoverage(xmapentry.contigQry)

            if listcontigs :
                outstr += "%5i" % xmapentry.contigQry
                outstr += "  %9.1f  %2i" % (contiglen, contigcov)

            #don't print lenr for each contig--just total them
            lenr = xmapentry.getMappedRefLen()
            lenq = xmapentry.getMappedQryLen()
            refid = xmapentry.contigRef #int
            if dorefidchr : #this is the encoding of ref contig id to chromosome and start position
                chrpos = mapClasses.intToChrPos(refid, verbose=False) #this returns a tuple (shouldn't fail, but verbose false)
                refidstr = "  %2s  %6i" % chrpos
                chrs = chrpos[0] #just for readability
                if chrsum.has_key(chrs) : #the method that fills chrsum (makeChrSummaryDict) also uses intToChrPos
                    chrsum[chrs][0] += lenr #values are list, first ele is total aligned length, second is ref length (fixed)
            elif dorefcid :
                refidstr = "  %3i" % refid #refid is int
            else : #nothing for neither, but still need empty string
                refidstr = ""

            conf = xmapentry.Confidence #confidence from the xmap, and ratio of it to length in kb
            if listcontigs :
                alignpars = getMappedErrStats(aligndir, cpath) #an empty err file is produced for case of no align
                avgfplist.append( alignpars.fp ) 
                avgfnlist.append( alignpars.fn ) 
                outstr += "%s  %9.1f  %.3f" % (refidstr, lenq, lenq/contiglen) #length for refidstr set above
                if printrange :
                    outstr += "  %5.0f  %5.0f  %5.0f  %5.0f" % (xmapentry.QryStart/pn, xmapentry.QryStop/pn, xmapentry.RefStart/pn, xmapentry.RefStop/pn)
                outstr += ("  %5.0f" % 0 if printsegrms else "") #don't print anything
                outstr += "  %3.0f  %5.3f" % (conf, conf*1000./lenq) #1000 is for kb
                outstr += "  " + alignpars.getParamString()

            totalignlen  += lenr
            totalignqlen += lenq

            #uniqueseg is now a dict to take into account which chromosome the query contig is on
            #note need refid bc need to separate different contigs on the _same_ chromosome
            if not uniqueseg.has_key(refid) : #if first contig on chromosome, need to init new list
                uniqueseg[refid] = []
            uniqueseg[refid].append( [xmapentry.RefStart, xmapentry.RefStop] )

            #process mapids--remove contig id (contigQry) from mapids if they're in the xmap so non-aligning contigs can be printed
            if xmapentry.contigQry in mapids :
                mapids.remove(xmapentry.contigQry)

            #note: the feature of multiple alignments (strict vs default) is no longer implemented
            defalignlen  += lenr #currently, just default and strict

            if listcontigs and dochrstr :
                outstr += "  " + refIndexToChrStr( xmapentry.contigRef )
                outstr += "\n"
            
        #end loop on xmap entries

        #now that all xmap entries are processed, all contigs with an alignment are removed from mapids,
        # so we can get n contigs align using this and ncontigs
        nmapcontigs += ncontigs - len(mapids) #sum multiple cmaps

        #and print the data for the contigs which don't align--just id, length, and coverage
        #these lines are kind of redundant, but I guess that's ok
        if listcontigs :
            for ids in mapids :
                outstr += "%5i" % ids
                #get map length from multicmap.getMapLength--returns 0 for any exception
                contiglen = mapi.getMapLength(ids) #it's ok if it's 0 bc it's never a denominator here
                contigcov = mapi.getMapAvgCoverage(ids)
                outstr += "  %9.1f  %2i\n" % (contiglen, contigcov)

    #end loop on contigs

    ncontigs = len(contiglens) #contigpaths is just files--contiglens is all contigs
    avgcontiglen = (float(totcontiglen)/ncontigs if ncontigs > 0 else 0)

    #print averages
    if listcontigs and not iscluster : #only do avg if not merged, otherwise just one noise parameter
        avgfp    = sum(avgfplist)/len(avgfplist)
        avgfn    = sum(avgfnlist)/len(avgfnlist)
        outstr += "AVG    %9.1f           %9.1f                     %5.3f  %5.3f\n" % (avgcontiglen, totalignqlen/nmapcontigs, avgfp, avgfn)

    if unitscale > 1e-6 : #if not megabases
        fstr = "%9.0f"
    else : #megabases
        fstr = "%8.3f" 

    outstr += "N contigs: %i\n" % ncontigs
    outstr += ("Total Contig Len: "+fstr+"\n") % (totcontiglen*unitscale)
    outstr += ("Avg. Contig Len : "+fstr+"\n") % (avgcontiglen*unitscale)
    outstr += ("Contig n50      : "+fstr+"\n") % (util.getn50(contiglens)*unitscale)

    if haveref :
        outstr += ("Total Ref Len   : "+fstr+"\n") % (reflen*unitscale)
        outstr += "Total Contig Len / Ref Len  : %.3f \n" % (totcontiglen/reflen)
    if dorefalign :
        #print the chromosome summary before the strict/default/total align stats
        if dorefidchr :
            outstr += "Chromosome Summary:\n"
            outstr += "Chromosome  align len  ref len  (ratio):\n"
            for chrs, align in chrsum.iteritems() :
                outstr += "%3s  %9.0f  %9.0f  (%5.3f)\n" % (chrs, align[0], align[1], align[0]/align[1])

        ratio = (float(nmapcontigs)/ncontigs if ncontigs > 0 else 0)
        outstr += "N contigs total align       : %3i (%.2f)\n" % (nmapcontigs, ratio)
        outstr += "Total Aligned Len           : "+str(totalignlen*unitscale)+"\n"
        outstr += "Total Aligned Len / Ref Len : %.3f \n" % (totalignlen/reflen)
        uniquelen = 0
        for segs in uniqueseg.values() : # need to sum on dict entries
            util.uniqueRange(segs) #this modifies list in place
            uniquelen += util.totalLengthFromRanges( segs )
        outstr += "Unique Aligned Len          : "+str(uniquelen*unitscale)+"\n"
        outstr += "Unique Aligned Len / Ref Len: %.3f \n" % (uniquelen/reflen)

    return outstr

#end characterizecontigs
