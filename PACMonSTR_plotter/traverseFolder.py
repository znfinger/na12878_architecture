#!/usr/bin/env python -o

import os, sys

#if the folder to be plotted is /path/to/fastas/
# with files /path/to/fastas/read.fa and /path/to/fastas/ref.fa
# then simply give /path/to/fastas/
pathToSPT = os.path.abspath(os.path.expanduser(sys.argv[1]))
pairedFastaPaths = map(lambda x: os.path.abspath(os.path.expanduser(x)),sys.argv[2:])

cwd = os.getcwd()
for folder in pairedFastaPaths:
    os.chdir(folder)
    #cmd = 'submitjob 2 -c 8 -m 16 -P acc_bashia02e -a mothra -n 1 python %s reads.fa refs.fa ' % pathToSPT
    cmd = 'python %s reads.fa refs.fa 1 1' % pathToSPT
    os.system(cmd)
    os.chdir(cwd)
