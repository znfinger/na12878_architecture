#!/usr/bin/env python -o

import os, sys
import argparse

try: #tests to see whether pysam is loaded
    import pysam
except ImportError:
    print "pysam module not yet loaded. Run \"module load pysam\", then try again."

parser = argparse.ArgumentParser(description='Provided a BAM alignment, an indexed reference genome in fasta format and a bed file, it generates a file heirarchy in the present directory wherein the root is named after the source .bed file, and each folder is named after bed entries. Those folders contain a pair of fasta files named read.fa and ref.fa, contained paired read and ref sequences. The outputted structure can be immediately used by our simplePlotter to generate rich dotplots.')

parser.add_argument( "-i", "--indexedReference", help="the location of an indexed reference genome, as produced by samtools faidx.")
parser.add_argument( "-r", "--reads", help="The location of the samtools sorted and indexed bam file to be used.")
parser.add_argument( "-b" , "--bedFile", help="The location of the bed file containing loci of interest.")
parser.add_argument( "-f" , "--folderOut", default ="", help="A name for the root folder. Default is the bed file's name without extensions.")

args = parser.parse_args()

#indexed_hg19 = '/sc/orga/work/pendlm02/hg19/ucsc.hg19.fasta'
#targetIdx = pysam.Fastafile(indexed_hg19)
#bedIn = open(sys.argv[1])
#bamIn = pysam.Samfile(sys.argv[2],'rb')

try:
    os.path.exists(args.indexedReference + '.fai')
except AttributeError:
    print "No reference index can be found. Be sure to create it in the same directory as the .fasta file itself, and to name it identically as the fasta with the addition of the extension '.fai'."
    exit()

targetIdx = pysam.Fastafile(args.indexedReference)
# targetIdx.fetch(region= flank5str).upper() is the command to use

bedIn = open(args.bedFile )
try:
    os.path.exists(args.reads + '.bai')
except AttributeError:
    print "No index can be found for the selected bam file. Be sure to create it in the same directory as the .bam file itself, and to name it identically as the bam with the addition of the extension '.bai'."
    exit()
bamIn = pysam.Samfile(args.reads,'rb')

cwd = os.getcwd()

if args.folderOut == "":
    bedFolderName = sys.argv[1].replace('.bed','').strip()
else:
    bedFolderName = args.folderOut

try:
    os.mkdir( bedFolderName)
except OSError:
    pass

for line in bedIn:
    ll = line.strip().split('\t')
    print '\t','\t'.join(ll[:3])
    try:
        os.mkdir('%s/%s_%s_%s' % (bedFolderName,ll[0],ll[1],ll[2]))
    except OSError:
        pass
    readsOut = open('%s/%s/%s_%s_%s/reads.fa' % (cwd, bedFolderName, ll[0],ll[1],ll[2]), 'a')
    refsOut = open('%s/%s/%s_%s_%s/refs.fa' % (cwd, bedFolderName, ll[0],ll[1],ll[2]), 'a')

    for alignedRead in bamIn.fetch(ll[0],int(ll[1]),int(ll[2])):
        if alignedRead.aend < int(ll[2]):
            continue
        read = alignedRead.query
        readsOut.write('>%s\n%s\n' % (alignedRead.qname, read))
        refsOut.write('>%s_%s_%s\n%s\n' % ( ll[0],alignedRead.pos,alignedRead.aend, targetIdx.fetch(region = "%s:%s-%s" % (ll[0],alignedRead.pos,alignedRead.aend)).upper()))

                      
"""
pysam.fetch Attributes:
['__class__', '__delattr__', '__doc__', '__format__', '__getattribute__', '__hash__', '__init__', '__new__', '__pyx_vtable__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '_convert_python_tag', 'aend', 'alen', 'aligned_pairs', 'bin', 'blocks', 'cigar', 'cigarstring', 'compare', 'fancy_str', 'flag', 'inferred_length', 'is_duplicate', 'is_paired', 'is_proper_pair', 'is_qcfail', 'is_read1', 'is_read2', 'is_reverse', 'is_secondary', 'is_unmapped', 'isize', 'mapq', 'mate_is_reverse', 'mate_is_unmapped', 'mpos', 'mrnm', 'opt', 'overlap', 'pnext', 'pos', 'positions', 'qend', 'qlen', 'qname', 'qqual', 'qstart', 'qual', 'query', 'rlen', 'rname', 'rnext', 'seq', 'setTag', 'tags', 'tid', 'tlen']
"""
